<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('home_gallery')){
    function home_gallery($key='rajiv'){
				
         $gallery_arr = array(
		'rajiv'=>array(
			array('image'=>base_url('assets/front/images/clinic.jpg'),'caption'=>'0'),
			array('image'=>base_url('assets/front/images/clinic1.jpg'),'caption'=>'1'),
			array('image'=>base_url('assets/front/images/clinic2.jpg'),'caption'=>'2'),
			array('image'=>base_url('assets/front/images/clinic3.jpg'),'caption'=>'3'),
			array('image'=>base_url('assets/front/images/clinic4.jpg'),'caption'=>'4'),
			array('image'=>base_url('assets/front/images/clinic5.jpg'),'caption'=>'5'),
			array('image'=>base_url('assets/front/images/clinic6.jpg'),'caption'=>'6'),
			array('image'=>base_url('assets/front/images/clinic7.jpg'),'caption'=>'7'),
			array('image'=>base_url('assets/front/images/clinic8.jpg'),'caption'=>'8'),
		),
		'ajay'=>array(
			array('image'=>base_url('assets/front/images/gallery-ajay-1.jpg'),'caption'=>'0'),
			array('image'=>base_url('assets/front/images/gallery-ajay-2.jpg'),'caption'=>'1'),
			array('image'=>base_url('assets/front/images/gallery-ajay-3.jpg'),'caption'=>'2'),
			array('image'=>base_url('assets/front/images/gallery-ajay-4.jpg'),'caption'=>'3'),
			array('image'=>base_url('assets/front/images/gallery-ajay-5.jpg'),'caption'=>'4'),
			array('image'=>base_url('assets/front/images/gallery-ajay-6.jpg'),'caption'=>'5'),
			array('image'=>base_url('assets/front/images/gallery-ajay-7.jpg'),'caption'=>'6'),
			array('image'=>base_url('assets/front/images/gallery-ajay-8.jpg'),'caption'=>'7'),
			array('image'=>base_url('assets/front/images/gallery-ajay-9.jpg'),'caption'=>'8'),
			array('image'=>base_url('assets/front/images/gallery-ajay-10.jpg'),'caption'=>'0'),
			array('image'=>base_url('assets/front/images/gallery-ajay-11.jpg'),'caption'=>'1'),
			array('image'=>base_url('assets/front/images/gallery-ajay-12.jpg'),'caption'=>'2'),
			array('image'=>base_url('assets/front/images/gallery-ajay-13.jpg'),'caption'=>'3'),
			array('image'=>base_url('assets/front/images/gallery-ajay-14.jpg'),'caption'=>'4'),
			array('image'=>base_url('assets/front/images/gallery-ajay-15.jpg'),'caption'=>'5'),
			array('image'=>base_url('assets/front/images/gallery-ajay-16.jpg'),'caption'=>'6'),
			array('image'=>base_url('assets/front/images/gallery-ajay-17.jpg'),'caption'=>'7'),
			array('image'=>base_url('assets/front/images/gallery-ajay-18.jpg'),'caption'=>'8'),
			array('image'=>base_url('assets/front/images/gallery-ajay-19.jpg'),'caption'=>'0'),
			array('image'=>base_url('assets/front/images/gallery-ajay-20.jpg'),'caption'=>'1'),
			array('image'=>base_url('assets/front/images/gallery-ajay-21.jpg'),'caption'=>'2'),
			array('image'=>base_url('assets/front/images/gallery-ajay-22.jpg'),'caption'=>'3'),
			array('image'=>base_url('assets/front/images/gallery-ajay-23.jpg'),'caption'=>'4'),
			array('image'=>base_url('assets/front/images/gallery-ajay-24.jpg'),'caption'=>'5'),
			array('image'=>base_url('assets/front/images/gallery-ajay-25.jpg'),'caption'=>'6'),
			array('image'=>base_url('assets/front/images/gallery-ajay-26.jpg'),'caption'=>'7')
			
		),
		);
		return (array_key_exists($key,$gallery_arr))?$gallery_arr[$key]:false;
    }   
}
