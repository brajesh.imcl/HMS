<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('top_nav')){
    function top_nav($key='rajiv'){
				
        $nav_arr = array('rajiv'=>array(array('url'=>base_url('home/profile/'.$key),'label'=>'Home'),array('url'=>'javascript:void(0);','label'=>'About the Clinic'),array('url'=>'javascript:void(0);','label'=>'Test & Treatments'),array('url'=>'javascript:void(0);','label'=>'GI Symptoms'),array('url'=>base_url('home/gallery/'.$key),'label'=>'Gallery'),array('url'=>base_url('home/contact_us/'.$key),'label'=>'Contact')),
		'ajay'=>array(array('url'=>base_url('home/profile/'.$key),'label'=>'Home'),array('url'=>'javascript:void(0);','label'=>'About the Clinic'),array('url'=>'javascript:void(0);','label'=>'Test & Treatments'),array('url'=>'javascript:void(0);','label'=>'GI Symptoms'),array('url'=>base_url('home/gallery/'.$key),'label'=>'Gallery'),array('url'=>base_url('home/contact_us/'.$key),'label'=>'Contact'))
		);
		return (array_key_exists($key,$nav_arr))?$nav_arr[$key]:false;
    }   
}
if ( ! function_exists('home_logo')){
    function home_logo($key='rajiv'){
        $logo_arr = array(
		'rajiv'=>array('logo'=>base_url('assets/front/images/logo.png'),'alt'=>'','title'=>'','url'=>''),
		'ajay'=>array('logo'=>base_url('assets/front/images/neonatology_logo.png'),'alt'=>'','title'=>'','url'=>'')
		);
		return (array_key_exists($key,$logo_arr))?$logo_arr[$key]:false;
    }   
}
if ( ! function_exists('clinic_details')){
    function clinic_details($key='rajiv'){
        $clinic_arr = array(
		'rajiv'=>array('email'=>'contact@dr.rajivsinha.com','phone'=>'0641-2301818',),
		'ajay'=>array('email'=>'contact@dr.ajaykumar.com','phone'=>'0641-2303030',),
		);
		return (array_key_exists($key,$clinic_arr))?$clinic_arr[$key]:false;
    }   
}

if ( ! function_exists('home_slider')){
    function home_slider($key='rajiv'){
       $slider_arr = array(
		'rajiv'=>array(
			array('image'=>base_url('assets/front/images/slider_img2_new.png'),'caption'=>'Slider-1'),
			array('image'=>base_url('assets/front/images/slider_img3.png'),'caption'=>'Slider-2'),
			array('image'=>base_url('assets/front/images/slider1.png'),'caption'=>'Slider-3'),
			array('image'=>base_url('assets/front/images/slider2.jpg'),'caption'=>'Slider-4'),
			array('image'=>base_url('assets/front/images/slider1.png'),'caption'=>'Slider-5'),
		),
		'ajay'=>array(
			array('image'=>base_url('assets/front/images/imageedit_1_6869133196.jpg'),'caption'=>'Slider-1'),
			array('image'=>base_url('assets/front/images/neonatology.jpg'),'caption'=>'Slider-2'),
			array('image'=>base_url('assets/front/images/neonatology1.jpg'),'caption'=>'Slider-3'),
			//array('image'=>base_url('assets/front/images/neonatology2.jpg'),'caption'=>'Slider-4'),
			array('image'=>base_url('assets/front/images/Pediatrict2.jpg'),'caption'=>'Slider-5'),
			array('image'=>base_url('assets/front/images/Pediatrict3.jpg'),'caption'=>'Slider-5'),
			array('image'=>base_url('assets/front/images/Pediatrict5.jpg'),'caption'=>'Slider-5'),
			
		),
		);
		return (array_key_exists($key,$slider_arr))?$slider_arr[$key]:false;
    }   
}
if ( ! function_exists('doctor_details')){
    function doctor_details($key='rajiv'){
       $doctor_arr = array(
		'rajiv'=>array(
			'image'=>base_url('assets/front/images/rajiv_sinha.jpg'),'details'=>'Dr Rajiv Sinha MBBS, M.D. (Medicine Patna), Medical Officer (JLNM College & Hospital Bhagalpur). Dr Rajiv Sinha is a Renowned Gastroenterologist and liver diseases specialist in JLNM Hospital. After finishing his Graduation from the ANMMCH, Gaya and PMCH,Patna. Dr Rajiv Sinha joined All India Institute of Medical Sciences, New Delhi, and worked there till 2011. In 2003, he commenced his post graduation in department of internal medicine under the guidance of esteemed faculty at country\'s premier institute, and over next 3 years, he learnt the art of medicine. In 2006, he worked as Causality Medical officer, where he dealt with medical emergencies proficiently. In 2007, he was selected for prestigious superspecialization course, in the department of Gastroenterology and Hepatology, at AIIMS. After finishing his superspecialization he Worked as Research Officer in the Department of Gastroenterology and Hepatology, and did the research work in Non alcoholic Fatty Liver Disease and Ulcerative colitis, and published papers in national and international Journals, and delivered Lectures in various National conferences. During his tenure as Research officer, he also got trained in the Endoscopic ultrosonography, which is a advanced tool to diagnose various abdominal diseases. Dr Rajiv Sinha Joined Department of Gastroenterology at Sir Ganga Ram Hospital in 2012, and is presently working as consultant, with a particular interest in liver diseases, pancreatic and biliary diseases, inflammatory bowel disease, and advanced endoscopic procedures Facilities','name'=>'Dr Rajiv Sinha'),
		'ajay'=>array(
			'image'=>base_url('assets/front/images/dr-ajay.jpg'),'details'=>'Dr Ajay Kumar MBBS, M.D. (Medicine Patna), Medical Officer (JLNM College & Hospital Bhagalpur). Dr Ajay Kumar is a Renowned Gastroenterologist and liver diseases specialist in JLNM Hospital. After finishing his Graduation from the ANMMCH, Gaya and PMCH,Patna. Dr Ajay Kumar joined All India Institute of Medical Sciences, New Delhi, and worked there till 2011. In 2003, he commenced his post graduation in department of internal medicine under the guidance of esteemed faculty at country\'s premier institute, and over next 3 years, he learnt the art of medicine. In 2006, he worked as Causality Medical officer, where he dealt with medical emergencies proficiently. In 2007, he was selected for prestigious superspecialization course, in the department of Gastroenterology and Hepatology, at AIIMS. After finishing his superspecialization he Worked as Research Officer in the Department of Gastroenterology and Hepatology, and did the research work in Non alcoholic Fatty Liver Disease and Ulcerative colitis, and published papers in national and international Journals, and delivered Lectures in various National conferences. During his tenure as Research officer, he also got trained in the Endoscopic ultrosonography, which is a advanced tool to diagnose various abdominal diseases. Dr Ajay Kumar Joined Department of Gastroenterology at Sir Ganga Ram Hospital in 2012, and is presently working as consultant, with a particular interest in liver diseases, pancreatic and biliary diseases, inflammatory bowel disease, and advanced endoscopic procedures Facilities','name'=>'Dr Ajay Kumar'),
		);
		return (array_key_exists($key,$doctor_arr))?$doctor_arr[$key]:false;
    }   
}
if ( ! function_exists('clinic_services')){
    function clinic_services($key = 'rajiv'){
		$service_arr = array('rajiv'=>array(array('title'=>'Endoscopy','image'=>base_url('assets/front/images/crl_img_1.png'),'desc'=>'This is a test using a thin flexible tube called an endoscope, to examine the inner lining of the oesophagus (gullet), stomach and upper small bowel(duodenum).'),array('title'=>'Acid Reflux','image'=>base_url('assets/front/images/crl_img_3.png'),'desc'=>'It is the movement of stomach contents back up from the stomach into the oesophagus (gullet). '),array('title'=>'Colonoscopy','image'=>base_url('assets/front/images/crl_img_2.png'),'desc'=>'Colonoscopy is a test in which the doctor passes a thin flexible tube through the back passage to have a look at the lining of the large bowel (colon).'),array('title'=>'Irritable Bowel Syndrome','image'=>base_url('assets/front/images/crl_img_4.png'),'desc'=>'IBS tends to be characterized by either diarrhoea or constipation, or an alternation between the two complaints.')),
		'ajay'=>array(array('title'=>'General Pediatrics','image'=>base_url('assets/front/images/General_pediatrics.png'),'desc'=>'Pediatrics (also spelled paediatrics or pædiatrics) is the branch of medicine that involves the medical care of infants, children, and adolescents.'),array('title'=>'Vaccination Services','image'=>base_url('assets/front/images/Vaccination_Services.png'),'desc'=>'Infants and young children need vaccines to protect them from infections that can cause diseases like chickenpox, measles, and whooping cough.. '),array('title'=>'Asthma Treatment','image'=>base_url('assets/front/images/Asthma_Treatment.png'),'desc'=>'Asthma is now treated in a step-wise approach. Intermittent asthma is treated with a rescue inhaler which is only used for symptoms.'),array('title'=>'Social Pediatrics','image'=>base_url('assets/front/images/Social_Pediatrics.png'),'desc'=>'Social pediatrics is a whole-family and whole-community approach to child medical problems and prevention.'))
		);
       return (array_key_exists($key,$service_arr))?$service_arr[$key]:false;
    }   
}
if ( ! function_exists('clinic_location_map')){
    function clinic_location_map($key = 'rajiv'){
       $location_map_arr = array('rajiv'=>' <iframe width="100%" height="400" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;coord=25.2527435,86.9815721&amp;q=Dr.%20Rajiv%20Sinha+(Gastroclinic)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>',
	   'ajay'=>' <iframe width="100%" height="400" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;coord=25.2527435,86.9815721&amp;q=Dr.%20Rajiv%20Sinha+(Gastroclinic)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>');
	    return (array_key_exists($key,$location_map_arr))?$location_map_arr[$key]:false;
    }   
}
if ( ! function_exists('rhs_news')){
    function rhs_news($key='rajiv'){
       $slider_arr = array(
		'rajiv'=>array(
			array('image'=>base_url('assets/front/images/clinic.jpg'),'caption'=>'0'),
			array('image'=>base_url('assets/front/images/clinic1.jpg'),'caption'=>'1'),
			array('image'=>base_url('assets/front/images/clinic2.jpg'),'caption'=>'2'),
			array('image'=>base_url('assets/front/images/clinic3.jpg'),'caption'=>'3'),
			array('image'=>base_url('assets/front/images/clinic4.jpg'),'caption'=>'4'),
			array('image'=>base_url('assets/front/images/clinic5.jpg'),'caption'=>'5'),
			array('image'=>base_url('assets/front/images/clinic6.jpg'),'caption'=>'6'),
			array('image'=>base_url('assets/front/images/clinic7.jpg'),'caption'=>'7'),
			array('image'=>base_url('assets/front/images/clinic8.jpg'),'caption'=>'8'),
		),
		'ajay'=>array(
			array('image'=>base_url('assets/front/images/slider_img2_new.png'),'caption'=>'Slider-1'),
			array('image'=>base_url('assets/front/images/slider_img3.png'),'caption'=>'Slider-2'),
			array('image'=>base_url('assets/front/images/slider1.png'),'caption'=>'Slider-3'),
			array('image'=>base_url('assets/front/images/slider2.jpg'),'caption'=>'Slider-4'),
			array('image'=>base_url('assets/front/images/slider1.png'),'caption'=>'Slider-5'),
		),
		);
		return (array_key_exists($key,$slider_arr))?$slider_arr[$key]:false;
    }	
}