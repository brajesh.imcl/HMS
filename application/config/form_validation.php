<?php
$config = array(
   'user_login' =>array(    
		array(
			'field' => 'username',
			'rules' => 'required|max_length[50]',
			'errors'=>array('required'=>'Please enter username','max_length'=>'Max length should be 50 char!')
		),
		array(
			'field' => 'password',
			'rules' => 'required|max_length[40]',
			'errors'=>array('required'=>'Please enter password','max_length'=>'Max length should be 40 char!')
		)
	),
	
	'create_clinic' =>array(    
		array(
			'field' => 'username',
			'rules' => 'required|max_length[50]',
			'errors'=>array('required'=>'Please enter username','max_length'=>'Max length should be 50 char!')
		),
	)
);