<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consultant_model extends CI_Model{
	
	private $table_name = 'consultant';
	
	
	function __construct(){
		parent:: __construct();
	}
	
	public function getConsultantRowBy($args = array()){
		$where_args = array();
		$sqlQuery = $this->db->select('*')->from($this->table_name)->where($args)->get();
		#echo $this->db->last_query();	
		$resultData = $sqlQuery->row_array();
		return $resultData;
		
	}
	
	public function getConsultant($args = array()){
		
		$sqlQuery = $this->db->select('*')->from($this->table_name)->where($args)->get();
		$resultData		=	$sqlQuery->result_array();
		#echo $this->db->last_query();
		return $resultData;	
		
	}
	public function saveConsultant($args = array()){	
		
		return $this->db->insert($this->table_name,$args);	
		
	}
	
	public function updateConsultant($postData = array(), $varWhere = array()){
		
		return $this->db->update($this->table_name, $postData, $varWhere);	
	}
	
	public function deleteConsultant($varWhere =	array()){
		
		return $this->db->delete($this->table_name, $varWhere);
	}

}