<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient_model extends CI_Model {
	
	private $patients = 'patients';
	private $patient_transaction = 'patient_transaction';
	private $treatments = 'treatments';
	private $clinic_user_login = 'clinic_user_login';
	private $temp_appointment = 'temp_appointment';
	
	function __construct(){
		parent:: __construct();
	}
	
	function getExcelData($args = array()){
		$sqlQuery = $this->db->select('patients.full_name,patients.gender,patients.address,patients.mobile,patients.location,patients.age,patients.weight,patients.dob,patients.email,patient_transaction.fees,patient_transaction.discount,patient_transaction.remarks,patient_transaction.paid_amt,patient_transaction.payment_date,patient_transaction.refered_by,patient_transaction.created,treatments.name,treatments.fees')
		->from($this->patients)	
		->join($this->patient_transaction,'patients.id = patient_transaction.patient_id')	
		->join($this->treatments,'patient_transaction.clinic_id = treatments.id')		
		->where($args)		
		->get();
		
		$this->load->library('excel');
		$sql = $this->db->select('*')
		->get('temp_appointment');	
		$this->excel->filename = 'Patient_History';
		$this->excel->make_from_db($sqlQuery);
	}
	public function getPatientDataById($args = array()){
		$where_args = array('patients.id'=>$args["id"],"patients.clinic_id"=>$args["clinic_id"]);
		$this->db->select('patients.*, trnsc.refered_by,trnsc.id as transId,trnsc.fees,trnsc.paid_amt,trnsc.remarks FROM `patients` LEFT JOIN (
		select id, refered_by,fees, patient_id, paid_amt, remarks from `patient_transaction` where id IN (select max(id) from patient_transaction group by patient_id)
	) trnsc ON `patients`.`id` = `trnsc`.`patient_id`')
			->where($where_args)			
		    ->limit(1);
		$sqlQuery =  $this->db->get();
		#echo $this->db->last_query();
		if ( $sqlQuery->num_rows() > 0 ){
			$resultData = $sqlQuery->row_array();
			return $resultData;
		}
	}
	
	public function getPatientBy($args = array()){
		
		$this->db->select('*')->from($this->patients)->where($args);
		$sqlQuery =  $this->db->get();		
		$resultData = $sqlQuery->row_array();
		return $resultData;
		
	}
	
	public function getAllPrescriptionFiles($args = array()){
		
		$this->db->select('*')->from('prescription_files')->where($args)->order_by('created_date desc');
		$sqlQuery =  $this->db->get();		
		$resultData = $sqlQuery->result_array();
		return $resultData;
		
	}
	public function getTreatments($args = array()){
		$where_args = array("clinic_id"=>$args["clinic_id"],'status'=>1);
		$this->db->select('*')
			->from($this->treatments)
			->where($where_args)->order_by('id asc');			 
			 
		$sqlQuery =  $this->db->get();
		#echo $this->db->last_query();
		if ( $sqlQuery->num_rows() > 0 ){
			$resultData = $sqlQuery->result_array();
			return $resultData;
		}
	}
	
	public function getTreatmentsByIds($args = array()){
		
		$this->db->select('ts.*, tr.name')
			->from('patient_transaction ts')->join('treatments tr', 'ts.treatment_id = tr.id')
			->where($args)->order_by('id asc');			 
		
		$sqlQuery =  $this->db->get();	
		#echo $this->db->last_query();		
		$resultData = $sqlQuery->result_array();
		return $resultData;
		
	}
	
	public function getPatientTransaction($args = array()){		
		$this->db->select('*')
			->from($this->patient_transaction)
			->where($args);			 
			 
		$sqlQuery =  $this->db->get();
		#echo $this->db->last_query();
		if ( $sqlQuery->num_rows() > 0 ){
			$resultData = $sqlQuery->result_array();
			return $resultData;
		}
	}
	public function getClinicInfo($args = array()){		
		$this->db->select('*')
			->from($this->clinic_user_login)
			->where($args);			 
			 
		$sqlQuery =  $this->db->get();
		#echo $this->db->last_query();
		if ( $sqlQuery->num_rows() > 0 ){
			$resultData = $sqlQuery->result_array();
			return $resultData;
		}
	}
	
	
	
	public function getPatients($args = array()){
		
		$where_clause = ' 1=1 ';
		$where_clause = " clinic_id = ".(int)$args["clinic_id"];
		if($args["p_id"]){
			$where_clause = " patients.id =".(int)$args["p_id"]."";
		}else if($args["date_range"] && $args["date_range"] !=""){
			$date_arr = explode(",",$args["date_range"]);
			$start_date =  date('Y-m-d', strtotime($date_arr[0]));
			$end_date =  date('Y-m-d', strtotime($date_arr[1]));
			$where_clause = ' CAST(patients.created AS DATE) BETWEEN "'. $start_date. '" and "'. $end_date.'"';
		}else{
			//$end_date = $start_date = date('Y-m-d', strtotime('today'));
			$where_clause = ' CAST(patients.created AS DATE)';
		}
		
		$refered_by_condition = " 1=1 ";
		if($args["refered_by"]){
			$refered_by_condition = " refered_by like '%".$args["refered_by"]."%'";
		}
		$patient_name = " 1=1 ";
		if($args["patient_name"]){
			$patient_name = " full_name like '%".$args["patient_name"]."%'";
		}
		
		$return_data = array('total'=>0,'data'=>'');
		///==== get total records 
			$this->db->select('patients.*, trnsc.refered_by FROM `patients` LEFT JOIN (
		select refered_by, patient_id from `patient_transaction` where id IN (select max(id) from patient_transaction group by patient_id)
	) trnsc ON `patients`.`id` = `trnsc`.`patient_id`')	
				->where(array("$this->patients.clinic_id"=>$args["clinic_id"]))			
				->where($where_clause)	
				->where($refered_by_condition)
				->where($patient_name);
		
			$sqlQuery = $this->db->get();
			$return_data["total"] = $sqlQuery->num_rows();
			#echo $this->db->last_query();
		//==== total records ends here
		
		$this->db->select('patients.*, trnsc.refered_by FROM `patients` LEFT JOIN (
		select refered_by, patient_id from `patient_transaction` where id IN (select max(id) from patient_transaction group by patient_id)
	) trnsc ON `patients`.`id` = `trnsc`.`patient_id` ')	
				->where(array("$this->patients.clinic_id"=>$args["clinic_id"]))						
				->where($where_clause)					
				->where($refered_by_condition)
				->where($patient_name)
				->order_by('patients.id desc')
				->limit($args["limit"],$args["offset"]);
				//->limit($args["limit_end"],$args["limit_st"]);
			

		
		$sqlQuery = $this->db->get();
		#echo $this->db->last_query();
		if ( $sqlQuery->num_rows() > 0 ){			
			 $resultData =$sqlQuery->result_array();
			 $return_data["data"] = $resultData;
			
		}
		return $return_data;
		
		
	}
	public function getReferedList($args = array()){
	 $this->db->select('refered_by')
				->from($this->patient_transaction)				
				->where($args)
				->group_by('refered_by')
				->order_by('refered_by', 'asc');
				//->limit($args["limit"]);
		
		$sqlQuery = $this->db->get();
		#echo $this->db->last_query();
		if ( $sqlQuery->num_rows() > 0 ){			
			 $resultData =$sqlQuery->result_array();				
			return $resultData;
		}
		
	
	}
	public function updatePatient($postData = array(),$varWhere = array()){
		#pr($postData);die;
		$data = array(				
				'full_name'=>$postData['full_name'],
				'age'=> $postData['age'],
				'yrs_mon'=> $postData['yrs_mon'],
				'weight'=> $postData['weight'],
				'address'=> $postData['address'],
				'mobile'=> $postData['mobile'],
				'gender'=> $postData['sex']
				);	
		$this->db->update($this->patient_transaction, 
										array('refered_by'=>$postData['refer_by'], 
										'treatment_id'=>$postData['treatment_id'],
										'fees'=>$postData['fees'],
										'discount'=>$postData['discount'],
										'remarks'=>$postData['remarks'],
										'paid_amt'=>(float)$postData["fees"]-((float)$postData["due_amount"]+(float)$postData["discount"])		
										),										
										array('id'=>$postData['transId'])
								);
		
		return $this->db->update($this->patients, $data, $varWhere);	
		
	}
	public function savePatientTransaction($data = array()){
		$this->db->insert($this->patient_transaction,$data);
		return $this->db->insert_id(); 
	}
	public function savePatient($postData = array()){
		
		$data = array(
				'clinic_id'=>$postData['clinic_id'],
				'full_name'=>$postData['full_name'],
				'age'=> $postData['age'],
				'weight'=> $postData['weight'],
				'address'=> $postData['address'],
				'mobile'=> $postData['mobile'],
				'gender'=> $postData['gender'],				
				'created'=> date("Y-m-d")							
				);	
			#echo '**fdgf**<pre>'; print_r($data);
		
		$this->db->insert($this->patients,$data);
		$patient_id =  $this->db->insert_id(); 
		
		$patient_fees = array(	
				'patient_id'=>$patient_id,
				'clinic_id'=>$postData['clinic_id'],
				'treatment_id'=>$postData['treatment_id'],			
				'fees'=> $postData['fees'],
				'discount'=> $postData['discount'],
				'remarks'=>$postData["remarks"],
				'paid_amt'=>$postData['fees']-$postData['due_amount'],
				'payment_date'=>date("Y-m-d"),
				'refered_by'=> $postData['refered_by'],
				'created'=>date("Y-m-d")						
				);	
		
		$this->db->insert($this->patient_transaction,$patient_fees);
		
		if($postData["_temp_id"] >0){
			$this->db->update($this->temp_appointment, array('status'=>2), array('id'=>(int)$postData["_temp_id"]));	
		}
		
		
		return $patient_id; 
		
	}
	
	public function savePrescriptionFile($args = array()){
		return $this->db->insert('prescription_files', $args);
		
	}
	
	public function exportPatients($args = array()){
		$where_clause = ' 1=1 ';
		$where_clause = " clinic_id = ".(int)$args["clinic_id"];
		if($args["p_id"]){
			$where_clause = " patients.id =".(int)$args["p_id"]."";
		}else if($args["date_range"] && $args["date_range"] !=""){
			$date_arr = explode(",",$args["date_range"]);
			$start_date =  date('Y-m-d', strtotime($date_arr[0]));
			$end_date =  date('Y-m-d', strtotime($date_arr[1]));
			$where_clause = ' CAST(patients.created AS DATE) BETWEEN "'. $start_date. '" and "'. $end_date.'"';
		}else{			
			$where_clause = ' CAST(patients.created AS DATE)';
		}
		
		$refered_by_condition = " 1=1 ";
		if($args["refered_by"]){
			$refered_by_condition = " refered_by like '%".$args["refered_by"]."%'";
		}	
		
		$sql	=	$this->db->select('concat("PID-", patients.id) as patient_id, `patients`.`full_name`, `patients`.`dob`, `patients`.`gender`, `patients`.`address`, `patients`.`mobile`, `patients`.`location`, `patients`.`age`, `patients`.`email`, `trnsc`.`refered_by`, `patients`.`created` FROM `patients` LEFT JOIN (select refered_by, patient_id from `patient_transaction` where id IN (select max(id) from patient_transaction group by patient_id)) trnsc ON `patients`.`id` = `trnsc`.`patient_id`')	
				->where(array("$this->patients.clinic_id"=>$args["clinic_id"]))			
				->where($where_clause)	
				->where($refered_by_condition)->get();
				
		#echo $this->db->last_query();die;		
		$this->load->library('excel');		
		$this->excel->filename = 'patient_records';		
		$this->excel->make_from_db($sql);		
	}

}