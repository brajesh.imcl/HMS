<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment_model extends CI_Model{
	
	public $table_name = 'temp_appointment';
	
	function __construct(){
		parent:: __construct();
	}
	
	public function getAppointments($varWhere = array()){
		
		$sqlQuery = $this->db->select('*')->from($this->table_name)->where($varWhere)->order_by('id','asc')->get();
		$resultData = $sqlQuery->result_array();
		#echo $this->db->last_query();
		return $resultData;
	}
	
	public function getDoctersBy($varWhere = array()){
		$sqlQuery = $this->db->select(array('id','name'))->from('consultant')->where($varWhere)->order_by('name','asc')->get();
		$resultData = $sqlQuery->result_array();
		return $resultData;
		
	}
	
	public function bookAppointment($args = array()){
		
		return $this->db->insert($this->table_name, $args);
	}

}