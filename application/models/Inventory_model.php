<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory_model extends CI_Model{
	
	private $table_category = 'category';
	private $table_brand = 'brand';
	private $table_product = "product";
	private $db_inventory = "inventory";
	private $db_inventory_order = "inventory_order";
	private $db_inventory_order_product = "inventory_order_product";
	
	function __construct(){
		parent:: __construct();
		$this->db_inventory = $this->load->database('inventory', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.  
	}
	
	
	public function getCategoryBy($args = array()){
		$where_args = array();
		$sqlQuery = $this->db_inventory->select('*')->from($this->table_category)->where($args)->get();
		#echo $this->db->last_query();	
		$resultData = $sqlQuery->row_array();
		return $resultData;
		
	}
	public function getCategories($args = array('limit'=>'0,10')){		
		$sqlQuery = $this->db_inventory->select('*')->from($this->table_category)		
		->where($args)		
		->limit($args['limit'])->get();
		$resultData		=	$sqlQuery->result_array();
		#echo $this->db->last_query();
		return $resultData;	
		
	}
	
	public function saveCategory($args = array()){	
		
		//return $this->db->insert($this->table_expenses,$args);	
		return $this->db_inventory->insert($this->table_category,$args);	
		
		
	}
	
	public function updateCategory($postData = array(), $varWhere = array()){
		
		return $this->db_inventory->update($this->table_category, $postData, $varWhere);	
	}
	public function getBrandBy($args = array()){
		$where_args = array();
		$sqlQuery = $this->db_inventory->select('*')->from($this->table_brand)->where($args)->get();
		#echo $this->db->last_query();	
		$resultData = $sqlQuery->row_array();
		return $resultData;
		
	}
	public function getBrands($args = array('limit'=>'0,10')){		
		$sqlQuery = $this->db_inventory->select('*')->from($this->table_brand)		
		->where($args)		
		->limit($args['limit'])->get();
		$resultData		=	$sqlQuery->result_array();
		#echo $this->db->last_query();
		return $resultData;	
		
	}
	public function saveBrand($args = array()){	
		return $this->db_inventory->insert($this->table_brand,$args);		
	}
	public function updateBrand($postData = array(), $varWhere = array()){
		
		return $this->db_inventory->update($this->table_brand, $postData, $varWhere);	
	}
	public function getProductBy($args = array()){
		$where_args = array();
		$sqlQuery = $this->db_inventory->select('*')->from($this->table_product)->where($args)->get();
		#echo $this->db->last_query();	
		$resultData = $sqlQuery->row_array();
		#echo '<pre>proeuct::';print_r($resultData);
		return $resultData;
		
	}
	public function getProducts($args = array('limit'=>'0,10')){		
		$sqlQuery = $this->db_inventory->select('*')->from($this->table_product)		
		->where($args)		
		->limit($args['limit'])->get();
		$resultData		=	$sqlQuery->result_array();
		#echo $this->db->last_query();
		return $resultData;	
		
	}
	public function saveProduct($args = array()){	
		return $this->db_inventory->insert($this->table_product,$args);		
	}
	public function updateProduct($postData = array(), $varWhere = array()){
		
		return $this->db_inventory->update($this->table_product, $postData, $varWhere);	
	}
	//==================================order goes here
	public function getOrdersBy($args = array()){
		$where_args = array();
		$sqlQuery = $this->db_inventory->select('*')->from($this->table_product)->where($args)->get();
		#echo $this->db->last_query();	
		$resultData = $sqlQuery->row_array();
		return $resultData;
		
	}
	public function getOrders($args = array('limit'=>'0,10'),$filterWhere = array()){
		$where_clause = ' 1=1 ';		
		if($filterWhere["date_range"] && $filterWhere["date_range"] !=""){
			$date_arr = explode(",",$filterWhere["date_range"]);
			$start_date =  date('Y-m-d', strtotime($date_arr[0]));
			$end_date =  date('Y-m-d', strtotime($date_arr[1]));
			$where_clause = ' CAST(inventory_order_product.created AS DATE) BETWEEN "'. $start_date. '" and "'. $end_date.'"';
		}else{
			$end_date = $start_date = date('Y-m-d', strtotime('today'));
			$where_clause = ' CAST(inventory_order_product.created AS DATE) BETWEEN "'. $start_date. '" and "'. $end_date.'"';
		}		
		
		$goods_name = " 1=1 ";
		if(isset($filterWhere["product_name"]) && $filterWhere["product_name"]!="") {
			$goods_name = " product.product_name like '%".$filterWhere["product_name"]."%'";
		}
		
			
		$sqlQuery = $this->db_inventory->select('product.id,product.product_name,inventory_order_product.quantity,inventory_order_product.price,inventory_order_product.tax,inventory_order_product.created,inventory_order.inventory_order_name,inventory_order.inventory_order_address,inventory_order.payment_status,inventory_order.staff_id')
		->from($this->table_product)	
		->join($this->db_inventory_order_product,'product.id = inventory_order_product.product_id','left')	
		->join($this->db_inventory_order,'inventory_order_product.inventory_order_id = inventory_order.id','left')	
		->where($where_clause)
		->where($goods_name)
		->where($args)		
		->limit($args['limit'])->get();
		$resultData		=	$sqlQuery->result_array();
		#echo $this->db_inventory->last_query();
		
		/*$this->db->select('*')
			->from($this->patients)
			->where($where_args)
			 ->join($this->patient_transaction, 'patients.id = patient_transaction.patient_id')
			 ->limit(1);*/
		
		return $resultData;	
		
	}
	public function saveOrder($args = array()){	
		#echo '<pre>'; print_r($args);
		//=== buyer details goes here
		$order_arr["clinic_id"] = $args["clinic_id"];
		$order_arr["staff_id"] = $args["staff_id"];
		$order_arr["inventory_order_quantity"] = $args["inventory_order_quantity"];
		$order_arr["inventory_order_total"] = $args["inventory_order_total"];
		$order_arr["inventory_order_date"] = date("Y-m-d");
		$order_arr["inventory_order_name"] = $args["inventory_order_name"];
		$order_arr["inventory_order_address"] = $args["inventory_order_address"];
		$order_arr["payment_status"] = $args["payment_status"];		
		$this->db_inventory->insert($this->db_inventory_order,$order_arr);		
		$order_id = $this->db_inventory->insert_id(); 
		//=== buyer and product transaction goes here 
		$order_product_arr["clinic_id"] = $args["clinic_id"];
		$order_product_arr["inventory_order_id"] = $order_id;
		$order_product_arr["product_id"] = $args["product_id"];
		$order_product_arr["quantity"] = $args["inventory_order_quantity"];
		$order_product_arr["price"] = $args["product_base_price"];
		$order_product_arr["tax"] = $args["inventory_order_name"];
		#echo '<pre>order_product_arr:::'; print_r($order_product_arr);
		
		$this->db_inventory->insert($this->db_inventory_order_product,$order_product_arr);		
		$order_product_id = $this->db_inventory->insert_id(); 
		//=== update product quantity 	
		$product_arr = $this->getProductBy(array('clinic_id'=>$args["clinic_id"],'id'=>$args["product_id"]));			
		$product_id = $this->updateProduct(array('product_quantity'=>($product_arr["product_quantity"]-$args["inventory_order_quantity"])),array('clinic_id'=>$args["clinic_id"],'id'=>$args["product_id"]));
		
		
		return array('order_id'=>$order_id,'order_product_id'=>$order_product_id,'product_id'=>$product_id);
	}
	public function updateOrder($postData = array(), $varWhere = array()){
		
		return $this->db_inventory->update($this->table_product, $postData, $varWhere);	
	}
	//==================================order ends here

}