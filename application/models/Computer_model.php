<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Computer_model extends CI_Model{
	
	public $table_name = 'computer';
	public $table_clinic = 'clinic_user_login';
	
	function __construct(){
		parent:: __construct();
	}
	
	public function AuthenticateUser($args = array()){
		
		$sqlQuery = $this->db->select('*')->from($this->table_name)->where($args)->get();
		$resultData = $sqlQuery->row_array();
		#echo $this->db->last_query();
		return $resultData;
	}
	
	public function getClinicRowBy($args = array()){
		$where_args = array();
		$sqlQuery = $this->db->select('*')->from($this->table_clinic)->where($args)->get();
		#echo $this->db->last_query();	
		$resultData = $sqlQuery->row_array();
		return $resultData;
		
	}
	
	public function getClinics($args = array('limit'=>'0,10')){
		
		$sqlQuery = $this->db->select('*')->from($this->table_clinic)->where($args)->limit($args['limit'])->get();
		$resultData		=	$sqlQuery->result_array();
		#echo $this->db->last_query(); 
		return $resultData;	
		
	}
	public function saveClinic($args = array()){
		
		return $this->db->insert($this->table_clinic,$args);	
		
	}
	
	public function updateClinic($postData = array(), $varWhere = array()){
		
		return $this->db->update($this->table_clinic, $postData, $varWhere);	
	}

}