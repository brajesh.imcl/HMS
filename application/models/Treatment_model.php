<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Treatment_model extends CI_Model{
	
	private $table_treatment = 'treatments';
	
	
	function __construct(){
		parent:: __construct();
	}
	
	
	public function getTreatmentRowBy($args = array()){
		$where_args = array();
		$sqlQuery = $this->db->select('*')->from($this->table_treatment)->where($args)->get();
		#echo $this->db->last_query();	
		$resultData = $sqlQuery->row_array();
		return $resultData;
		
	}
	public function getTreatments($args = array('limit'=>'0,10')){
		
		$sqlQuery = $this->db->select('*')->from($this->table_treatment)->where($args)->limit($args['limit'])->get();
		$resultData		=	$sqlQuery->result_array();
		#echo $this->db->last_query();
		return $resultData;	
		
	}
	public function saveTreatment($args = array()){	
		
		return $this->db->insert($this->table_treatment,$args);	
		
	}
	
	public function updateTreatment($postData = array(), $varWhere = array()){
		
		return $this->db->update($this->table_treatment, $postData, $varWhere);	
	}
	
	public function deleteTreatment($varWhere =	array()){
		return $this->db->update($this->table_treatment, array('status'=>2), $varWhere);	
		//return $this->db->delete($this->table_treatment, $varWhere);
	}

}