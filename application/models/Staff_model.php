<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff_model extends CI_Model{
	
	private $table_staff = 'staff_details';
	
	
	function __construct(){
		parent:: __construct();
	}
	
	public function getStaffRowBy($args = array()){
		$where_args = array();
		$sqlQuery = $this->db->select('*')->from($this->table_staff)->where($args)->get();
		#echo $this->db->last_query();	
		$resultData = $sqlQuery->row_array();
		return $resultData;
		
	}
	
	public function getStaffs($args = array('limit'=>'0,10')){
		
		$sqlQuery = $this->db->select('*')->from($this->table_staff)->where($args)->limit($args['limit'])->get();
		$resultData		=	$sqlQuery->result_array();
		#echo $this->db->last_query(); 
		return $resultData;	
		
	}
	public function saveStaff($args = array()){	
		
		return $this->db->insert($this->table_staff,$args);	
		
	}
	
	public function updateStaff($postData = array(), $varWhere = array()){
		
		return $this->db->update($this->table_staff, $postData, $varWhere);	
	}
	
	public function deletestaff($varWhere =	array()){
		
		return $this->db->delete($this->table_staff, $varWhere);
	}

}