<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model{
	
	public $table_name = 'clinic_user_login';
	
	function __construct(){
		parent:: __construct();
	}
	
	public function AuthenticateUser($args = array()){
		
		$sqlQuery = $this->db->select('*')->from($this->table_name)->where($args)->get();
		$resultData = $sqlQuery->row_array();
		#echo $this->db->last_query();
		return $resultData;
	}

}