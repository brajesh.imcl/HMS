<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expense_model extends CI_Model{
	
	private $table_expenses = 'daily_expenses';
	
	
	function __construct(){
		parent:: __construct();
	}
	
	
	public function getExpenseRowBy($args = array()){
		$where_args = array();
		$sqlQuery = $this->db->select('*')->from($this->table_expenses)->where($args)->get();
		#echo $this->db->last_query();	
		$resultData = $sqlQuery->row_array();
		return $resultData;
		
	}
	public function getExpenses($args = array('limit'=>'0,10'),$filterWhere = array()){
		
		$where_clause = ' 1=1 ';
		/*$where_clause = " clinic_id = ".(int)$args["clinic_id"];*/
		if($filterWhere["date_range"] && $filterWhere["date_range"] !=""){
			$date_arr = explode(",",$filterWhere["date_range"]);
			$start_date =  date('Y-m-d', strtotime($date_arr[0]));
			$end_date =  date('Y-m-d', strtotime($date_arr[1]));
			$where_clause = ' CAST(created AS DATE) BETWEEN "'. $start_date. '" and "'. $end_date.'"';
		}else{
			$end_date = $start_date = date('Y-m-d', strtotime('today'));
			$where_clause = ' CAST(created AS DATE) BETWEEN "'. $start_date. '" and "'. $end_date.'"';
		}		
		
		$goods_name = " 1=1 ";
		if(isset($filterWhere["goods_name"]) && $filterWhere["goods_name"]!="") {
			$goods_name = " goods_name like '%".$filterWhere["goods_name"]."%'";
		}
		
		
		$sqlQuery = $this->db->select('*')->from($this->table_expenses)
		->where($where_clause)
		->where($goods_name)
		->where($args)		
		->limit($args['limit'])->get();
		$resultData		=	$sqlQuery->result_array();
		#echo $this->db->last_query();
		return $resultData;	
		
	}
	public function saveExpense($args = array()){	
		
		return $this->db->insert($this->table_expenses,$args);	
		
	}
	
	public function updateExpense($postData = array(), $varWhere = array()){
		
		return $this->db->update($this->table_expenses, $postData, $varWhere);	
	}
	
	public function deleteTreatment($varWhere =	array()){
		return $this->db->update($this->table_expenses, array('status'=>2), $varWhere);	
		//return $this->db->delete($this->table_treatment, $varWhere);
	}

}