
<div class="container text-center my-4 testimonial">
   <h2 class="mb-4">Testimonial</h2>

<div id="carouselTestimonial" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselTestimonial" data-slide-to="0" class="active"></li>
    <li data-target="#carouselTestimonial" data-slide-to="1"></li>
    <li data-target="#carouselTestimonial" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active  mb-4">        
         <p>"Nulla vitae elit libero, a pharetra augue mollis interdum.„</p>
          <small>...Someone famous</small>
       </div> 
    <div class="carousel-item mb-4">        
         <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit.„</p>
          <small>...Someone famous</small>
    </div>
    <div class="carousel-item mb-4">        
         <p>"Praesent commodo cursus magna, vel scelerisque nisl consectetur.„</p>
          <small>...Someone famous</small>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselTestimonial" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselTestimonial" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

</div>