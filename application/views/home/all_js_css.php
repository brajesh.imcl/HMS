<link rel="stylesheet" href="<?php echo base_url('assets/common/css/bootstrap.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/front/css/stylee.css?'.time());?>">
<link rel="stylesheet" href="<?php echo base_url('assets/front/css/text-switcher.css?'.time());?>">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<script src="<?php echo base_url('assets/common/js/jquery.min.js');?>"></script>
<script src="<?php echo base_url('assets/common/js/popper.min.js');?>"></script>
<script src="<?php echo base_url('assets/common/js/bootstrap.min.js');?>" ></script> 
 <script src="<?php echo base_url('assets/front/js/text-switcher.js');?>" ></script>
<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,hi', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
