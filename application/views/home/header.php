<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <?php $this->load->view('home/meta');?>  
	<title>e-HMS</title>
    <!-- Bootstrap CSS -->
     <?php $this->load->view('home/all_js_css');?>  
     
     <link rel="stylesheet" href="<?php echo base_url('assets/front/css/simplelightbox.css?'.time());?>">
  </head>
  <body>
   
<section class="bg-gradient-primary">

  <div class="container bg-primary ">
     <a href="javascript:void(0);" class="text-white"> <i class="far fa-envelope"></i> <?php echo $clinic_details["email"];?></a>
     <a href="" class="float-right  text-white"><i class="fas fa-phone-volume"></i> <?php echo $clinic_details["phone"];?>  </a>
     <!--<a href="" class="float-right  text-white">hospital site template </a>-->
  </div>
</section>
<section class="bg-light">
  <div class="container">
   <nav class="navbar navbar-expand-lg navbar-light bg-light px-0">
  <a class="navbar-brand" href="<?php echo $home_logo["url"];?>" title="<?php echo $home_logo["alt"];?>"><img src="<?php echo $home_logo["logo"];?>" class="img-fluid" alt="<?php echo $home_logo["alt"];?>"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ">
    <?php foreach($top_nav as $key => $row){?>
      <li class="nav-item <?php if($key==0){?>active<?php }?>">
        <a class="nav-link" href="<?php echo $row["url"];?>"><?php echo $row["label"];?> <?php if($key==0){?><span class="sr-only">(current)</span><?php }?></a>
      </li>
      <?php }?>   
         
      
    </ul>
    <ul class="navbar-nav flex-row ml-md-auto">
      <li class="nav-item">
          <a href="javascript:void(0);" class="float-right appointment rounded" data-toggle="modal" data-target="#exampleModalCenter">Book Appointment </a>
      </li>
    </ul>
  </div>
  
</nav>
</div>
</section>