
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Appointment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form id="bookappointment" method="post" onsubmit="return validatappForm();">
          <div class="form-group">
            <label>Name </label>
            <input type="text" class="form-control" id="name" name="name"  placeholder="Enter Name" required>
          </div>
           
           <div class="form-group">
            <label>Phone Number</label>
            <input type="text" class="form-control" id="mobile" name="mobile"  placeholder="Enter Mobile No" required>
          </div>
         <div class="form-row">
           <div class="form-group col-md-6">
              <label>Location</label>
              <input type="text" class="form-control" id="address" name="address" placeholder="Your Location" required>
           </div>
           <div class="form-group col-md-6">
               <label>Date/Time</label>
               <input type="datetime-local" id="created" name="created" class="form-control" required>
            </div>
             
         </div>
           <!--div class="form-group">
                <label for="">Comments:</label>
               <textarea class="form-control" id="" rows="3"></textarea>
          </div>-->
        <input type="hidden" name="clinic_id" id="clinic_id" value="<?php echo ($doctor == 'Dr. Rajiv')?1:3?>">
       

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Book</button>
      </div>
	  </form>
    </div>
  </div>
</div>
<script>
function validatappForm(){

	$.ajax({
			'url':'http://localhost/hms/appfeed/appointment/book',
			'data' : $('#bookappointment').serialize(),
			'type': 'POST',
			'dataType' : 'json',
			'success' : function(responseData){				
				if(responseData.status){
					$('#exampleModalCenter').modal('hide');
					$('#bookappointment')[0].reset();
				}
				show_message_box(responseData.message);				
			}		
	});
	
return false;	
}
</script>