   <div class="col-12 col-lg-3">
      <div class="row">
        <div class=" col-lg-12 col-12">
          <div class=" text-white side-card mt-4 rounded p-0" >
            <h5 class="p-2 bg-warning rounded text-center">In The News</h5>
            <div class="side-body px-1"> 
             
              <div class="text-switcher" data-js="text-switcher">
                <div class="tc__line"><span>&bull; <?php echo $doctor; ?></span> <span class="tc__bottom">&bull; and advanced endoscopic</span></div>
                <div class="tc__line"><span> &bull;pancreatic and biliary diseases, inflammatory </span> <span class="tc__bottom">&bull;pancreatic and</span></div>
                <div class="tc__line"><span>&bull; bowel disease, and advanced endoscopic </span> <span class="tc__bottom">&bull; bowel disease</span></div>
                <div class="tc__line"><span>&bull; pancreatic and biliary diseases, inflammatory </span> <span class="tc__bottom">&bull; biliary diseases,</span></div>
                <div class="tc__line"><span>&bull; bowel disease, and advanced endoscopic </span> <span class="tc__bottom">&bull; disease, and advanced</span></div>
              </div>
            </div>
          </div>
        </div>
       
        <div class=" col-lg-12 col-6">
          <div class="side-card rounded mt-4"> <a href="#"  class="text-white fs-13"> Email US</a> </div>
        </div>
        <div class=" col-lg-12 col-6">
          <div class="side-card rounded mt-4  text-white "> <a href="javascript:void(0);"  class="text-white  fs-13" data-toggle="modal" data-target="#exampleModalCenter"> Book Appointment</a> </div>
        </div>
       
      </div>
    </div>