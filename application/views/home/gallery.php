<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('home/header');
?>
<?php //$this->load->view("home/home-top");?>

<section class="bg-gradient bg-sub">
  <div class="container">
     <h1 class="text-center">Gallery</h1>
  </div>
</section>

<div class="container mt-4 gallery-img">
  
  <div class="row">   
   <?php foreach($gallery as $key => $row){?>
      <div class=" col-12 col-sm-6 pb-4"><img class="thumbnail img-fluid" src="<?php echo $row["image"];?>"></div>
	<?php }?>
      

   
    <hr>
    
    <hr>
  </div>
</div>
<div tabindex="-1" class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Gallery</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
  <div class="modal-body">
    
  </div>
  
   </div>
  </div>
</div>




<?php $this->load->view('home/footer');?>
<script src="<?php echo base_url('assets/front/js/custom.js');?>" ></script>  
