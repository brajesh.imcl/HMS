<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('home/header');
?>
<section class="bg-gradient bg-sub">
  <div class="container">
     <h1 class="text-center">Contact Us</h1>
  </div>
</section>

<div class="container mt-4">
    <div class="row">
      <div class="col-12 col-lg-9">
        <h4>Contact Us</h4>
         <form>
          <div class="form-group">
            <label>First Name </label>
            <input type="text" class="form-control" id=""  placeholder="Enter First Name">
          </div>
           <div class="form-group">
            <label>Last Name :</label>
            <input type="text" class="form-control" id=""  placeholder="Enter Last Name">
          </div>
           <div class="form-group">
            <label>Email Id</label>
            <input type="text" class="form-control" id=""  placeholder="Enter Email Id">
          </div>
         <div class="form-row">
           <div class="form-group col-md-4">
              <label>Phone Number </label>
              <input type="email" class="form-control" id="" placeholder="Phone Number">
           </div>
           <div class="form-group col-md-4">
               <label>APPOINTMENT</label>
                <select id="inputState" class="form-control">
                   <option selected>Choose...</option>
                    <option>...</option>
                  </select>
            </div>
             <div class="form-group col-md-4">
                <label>SCHEDULE</label>
                <select id="inputState" class="form-control">
                   <option selected>Choose...</option>
                    <option>...</option>
                  </select>
            </div>
         </div>
           <div class="form-group">
                <label for="">Comments:</label>
               <textarea class="form-control" id="" rows="3"></textarea>
          </div>
         <button type="submit" class="btn btn-primary btn-lg btn-block">Send</button>
       </form>

      </div>
  <?php $this->load->view("home/rhs");?>

  </div>


</div>



 

<?php $this->load->view('home/footer');?>
