<div class="container mt-4">
<div class="card-deck ">
<?php foreach($clinic_services as $key => $row){?>
    <div class="card border-success">
       <img class="card-img-top img-fluid img-thumbnail" src="<?php echo $row["image"];?>"  alt="<?php echo $row["title"];?>" />
        <div class="card-footer p-0">
           <h5 class="card-header text-center"><?php echo $row["title"];?></h5>
           <p class="p-2"> <?php echo $row["desc"];?></p>
         </div>
    </div>
<?php }?>
  
</div>
</div>