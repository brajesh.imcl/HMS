<div id="message_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>        
      </div>
      <div class="modal-body">
        <p></p>
      </div>      
    </div>
  </div>
</div>
<script>

function show_message_box(msg){
	$('#message_modal p').html(msg);
	$('#message_modal').modal();	
}

<?php if($this->session->flashdata('message')){?>
	show_message_box('<?php echo $this->session->flashdata('message');?>');
<?php }?>
</script>