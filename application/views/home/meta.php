<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no  user-scalable=0">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="object" property="og:type">
<meta content="e-HMS" property="og:site_name">
<meta content="e-HMS" property="og:title">
<meta content="e-HMS Management" property="og:description">
<meta content="<?php echo base_url('assets/front/images/Pediatrict2.jpg');?>" property="og:image">
<meta content="64" property="og:image:width">
<meta content="64" property="og:image:height">
<meta content="<?php echo base_url('home/index');?>" property="og:url">
<meta content="summary" property="twitter:card">
<meta content="e-HMS" property="twitter:title">
<meta content="e-HMS Management" property="twitter:description">
<meta content="<?php echo base_url('assets/front/images/Pediatrict2.jpg');?>" property="twitter:image">