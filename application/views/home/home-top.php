<section class="bg-gradient">
  <div class="container slider">
 
   <div id="carouselExampleControls" class="carousel slide pt-3" data-ride="carousel">
     <div class="carousel-inner">
     
	<?php foreach($home_slider as $key => $row){?>     
     <div class="carousel-item <?php if($key==0){?>active<?php }?>">
        <img class="d-block w-100 img-fluid" src="<?php echo $row["image"];?>" alt="<?php echo $row["caption"];?>" >
      </div>
      <?php }?>      
    </div>
     <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
       <span class="carousel-control-prev-icon" aria-hidden="true"></span>
       <span class="sr-only">Previous</span>
     </a>
     <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
       <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
     </a>
  </div>

  
</div>
</section>