<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('home/header');
?>
<?php $this->load->view("home/home-top");?>

<div class="container mt-4">
  <div class="row">
    <div class="col-12 col-lg-9 dr-portfolio">
      <h2>Know Your Doctor</h2>
      <img src="<?php echo $doctor_details["image"];?>" alt="<?php echo $doctor_details["name"];?>" width="286" height="232" class="img-fluid float-left p-3">
      <p class="text-justify"><?php echo $doctor_details["details"];?></p>
    </div>
  <?php $this->load->view("home/rhs");?>
    
  </div>
</div>
<?php $this->load->view("home/home-tiles");?>
<?php $this->load->view('home/testimonial');?>
<div class="container mt-4">
   <h2>Location</h2>
     <?php echo $clinic_location_map;?>
</div>

<?php $this->load->view('home/footer');?>
<script>
  $(document).ready(function() {
  //Set the carousel options
  $('#carouselTestimonial').carousel({
    pause: true,
    interval: 4000,
  });
});
  
</script>

