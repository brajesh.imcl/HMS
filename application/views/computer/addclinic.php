<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Computer Management System</title>
	<?php $this->load->view('computer/all_js_css');?>
  </head>
  <body class="form-bg">
  <?php $this->load->view('computer/profile_nav');?>
<div class="container-fluid">
  <div class="row">
     <?php $this->load->view('computer/navigation');?>
      <div class="col-md-10  col-12  col-sm-12 mt-3">
        <div class="bg-white p-3">
        <h3 class="mb-4"><?php echo (isset($clinicinfo))?'Edit':'Add';?> Clinic <a href="<?php echo base_url('computer/dashboard/');?>" class="float-right" title="Back"><i class="fas fa-arrow-left"></i></a></h3>

        <form class="needs-validation" novalidate method="post" action="" autocomplete="off" enctype="multipart/form-data">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label >Full Name <span>*</span></label>
      <input type="text" class="form-control" id="clinic_name" name="clinic_name" value="<?php echo set_value('clinic_name')? set_value('clinic_name') : $clinicinfo['clinic_name'];?>" autocomplete="off" required>
      <span id="full_name_error" class="error"></span>
    </div>
    <div class="form-group col-md-6">
      <label>Mobile<span>*</span></label>
      <input type="number" class="form-control" id="clinic_phone" name="clinic_phone"  value="<?php echo set_value('clinic_phone')? set_value('clinic_phone') : $clinicinfo['clinic_phone'];?>" required>
      <span id="age_error" class="error"></span>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Email</label>
      <input type="email" class="form-control" id="clinic_email" name="clinic_email" value="<?php echo set_value('clinic_email')? set_value('clinic_email') : $clinicinfo['clinic_email'];?>">
      <span id="weight_error" class="error"></span>
    </div>
    <div class="form-group col-md-6">
      <label>Address <span>*</span></label>
      <input type="text" class="form-control" id="clinic_addr" name="clinic_addr" value="<?php echo set_value('clinic_addr')? set_value('clinic_addr') : $clinicinfo['clinic_addr'];?>" required>
      <span id="address_error" class="error"></span>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>UserName <span>*</span></label>
      <input type="text" class="form-control" id="user_id" name="user_id" <?php if($clinicinfo){?> disabled <?php }?> value="<?php echo set_value('user_id')? set_value('user_id') : $clinicinfo['user_id'];?>" required>
      <font color="red"><?php echo form_error('user_id'); ?></font>
    </div>
    <div class="form-group col-md-6">
      <label>Password <span>*</span></label>
      <input type="password" class="form-control" id="password" name="password" value="<?php echo set_value('password')? set_value('password') : $clinicinfo['password'];?>" required>
	  <?php if($clinicinfo){?>
		<input type="hidden" class="form-control" id="oldpassword" name="oldpassword" value="<?php echo $clinicinfo['password'];?>">
	  <?php }?>
    </div>
  </div>
  <div class="form-row">    
	<div class="form-group col-md-6">
      <label>Select Access <span>*</span></label>
	  <select id="access" name="access[]" class="form-control" multiple="multiple" required>        
        <?php 
		$accessArr = array();
		if($clinicinfo){
			$accessArr = json_decode($clinicinfo['access']);	
		}
		foreach($controls as $row){ $value = str_replace('.php','',$row);?>
		<option value="<?php echo $value;?>" <?php echo (in_array($value, $accessArr))?'selected':'';?>><?php echo str_replace('.php',' Management',$row);?></option>
        <?php }?>
      </select>	  
    </div>  
	<div class="form-group col-md-6">
      <label>Clinic Logo</label>
      <input type="file" class="form-control" id="clinic_photo" name="clinic_photo">
	  <?php if(isset($uploadData['error']))echo '<font color="red">'.$uploadData['error'].'</font>';?>
    </div>	
  </div>    
  <div class="offset-md-5 col-md-3">  
  <button type="submit" class="btn btn-primary"><?php echo (isset($clinicinfo))?'Update':'Add';?> Clinic</button>
  <button type="reset" class="btn btn-warning">Reset</button>
</div>
</form>

</div>
      </div>
  </div>
</div>
<?php $this->load->view('computer/footer');?> 
<script>
// valid fields Validation Form
(function(){
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

    </script>
  </body>
</html>