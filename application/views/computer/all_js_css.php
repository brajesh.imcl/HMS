<link rel="stylesheet" href="<?php echo base_url('assets/common/css/bootstrap.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/backend/css/stylee.css?'.time());?>">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<script src="<?php echo base_url('assets/common/js/jquery-3.3.1.slim.min.js');?>"></script>
<script src="<?php echo base_url('assets/common/js/popper.min.js');?>"></script>
<script src="<?php echo base_url('assets/common/js/bootstrap.min.js');?>" ></script>  