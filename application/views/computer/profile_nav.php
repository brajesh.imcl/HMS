<nav class="navbar navbar-expand-lg navbar-light bg-light">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidebar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="<?php echo base_url('management/dashboard');?>"><img src="<?php echo base_url('assets/common/images/logo.png');?>"></a>
  <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown mx-3">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         <i class="fas fa-user-md menu-icon pr-0"></i> <?php echo $clinicInfo['clinic_name'];?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#"><i class="fas fa-cog menu-icon"></i> Setting</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url('computer/dashboard/logout');?>"><i class="fas fa-power-off menu-icon"></i>Logout</a>
          
        </div>
      </li>      
    </ul>
</nav>