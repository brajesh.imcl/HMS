<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Computer Management System</title>
	<?php $this->load->view('computer/all_js_css');?>
  </head>
  <body class="form-bg">
   <?php $this->load->view('computer/profile_nav');?>
<div class="container-fluid">
  <div class="row">
     <?php $this->load->view('computer/navigation');?>
      <div class="col-md-10 col-12  col-sm-12 mt-3">
      <div class="bg-white p-3">
<h3 class="mb-4">Clinic Listing <a href="<?php echo base_url('computer/dashboard/addclinic');?>" class="float-right" title="Add Staff"><i class="fas fa-plus-square"></i></a></h3>
<div class="table-responsive">
  <table class="table table-fluid table-bordered table-hover">
   <thead  class="thead-light">
    <tr>
      <th scope="col">SN. No</th>
      <th scope="col">Full Name</th>
      <th scope="col">Mobile</th>
      <th scope="col">Email</th>           
      <th scope="col">Satatus</th>
      <th scope="col">Action</th>
    </tr>
   </thead>
  <tbody>
  <?php 
  	if(count($allClinics)>0){ 
  	foreach($allClinics as $key => $row){?>
    <tr>
      <th scope="row"><?php echo ++$key;?></th>
      <td><?php echo $row["clinic_name"];?></td>
      <td><?php echo $row["clinic_phone"];?></td>
      <td><?php echo $row["clinic_email"];?></td>      
      <td><?php echo ($row["status"])?'Active':'Inactive';?></td>
      <td>
	  <a href="<?php echo base_url('computer/dashboard/edit/'.$row['id']);?>"><i class="fa fa-edit"></i></a> 
	  <a href="<?php echo base_url('computer/dashboard/delete/'.$row['id']);?>" onclick="return confirm('Are you sure want to delete this?');" class="float-right"><i class="fa fa-trash"></i></a></td>
    </tr>
    <?php }}else{?>
    <tr>
      <th scope="row" colspan="8">No Records Found !!</th>
     </tr>
    <?php }?>
    
  </tbody>
</table>
</div>
</div>

      </div>
  </div>
</div>
<?php $this->load->view('computer/footer');?>   
 </body>
</html>