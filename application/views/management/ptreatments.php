<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?> 
<div class="container-fluid">
  <div class="row">
    <?php $this->load->view('management/navigation');?>
    <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
      <div class="bg-white p-3">
        <h3 class="mb-4">Patient's Treatment <a href="<?php echo base_url('management/patient/patients/');?>" class="float-right" title="Back"><i class="fas fa-arrow-left"></i></a></h3>
        <form class="needs-validation" novalidate method="post" action="" autocomplete="off" id="frmPatient">
          <div class="form-row">
            <div class="form-group col-md-4">
              <label >Refer By</label>
              <input type="text" class="form-control" id="refer_by" name="refer_by" value="" />
            </div>
            <div class="form-group col-md-4">
              <label>Treatment Type <span class="star">*</span></label>
              <select id="treatment" name="treatment" class="form-control" >
                <option value="" selected>Select</option>
                <?php 
					$treatment_array = array();
					foreach((array)$treatment as $key => $treatment_row){
						$treatment_array[$treatment_row["id"]] = $treatment_row["fees"];
				?>
                <option value="<?php echo $treatment_row["id"];?>"><?php echo $treatment_row["name"];?></option>
                <?php }?>
                
              </select>
            </div>
            <div class="form-group col-md-4">
              <label>Fees <span class="star">*</span></label>
              <input type="number" step="0.01" class="form-control" id="fees" name="fees" required value="0.00"/>
              <span id="fees_error" class="error"></span> </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label>Discount</label>
              <input type="text" class="form-control" id="discount" name="discount" value="" />
            </div>
            <div class="form-group col-md-4">
              <label>Remarks</label>
              <textarea id="remarks" name="remarks" class="form-control" rows="1" ></textarea>
            </div>
            <div class="form-group col-md-4">
              <label>Due Amount</label>
              <input type="text" class="form-control" id="due_amount" name="due_amount" value="" />
            </div>
          </div>


          <div class="colpse col-md-12 px-0">
 
          <div class="form-row">
            <div class="form-group col-md-6">
              <label >Full Name <span class="star">*</span></label>
              <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Full Name" autocomplete="off" required value="<?php echo $full_name;?>" readonly>
              <span id="full_name_error" class="error"></span> </div>
            <div class="form-group col-md-6">
              <label>Age: (in years) <span>*</span></label>
              <input type="number" class="form-control" id="age" name="age"  placeholder="Age" required value="<?php echo $age;?>" readonly>
              <span id="age_error" class="error"></span> </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Weight (in kgs)<span>*</span></label>
              <input type="number" class="form-control" id="weight" name="weight" placeholder="Weight" required value="<?php echo $weight;?>" readonly>
              <span id="weight_error" class="error"></span> </div>
            <div class="form-group col-md-6">
              <label>Address <span>*</span></label>
              <input type="text" class="form-control" id="address" name="address" placeholder="Address" required value="<?php echo $address;?>"readonly>
              <span id="address_error" class="error"></span> </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Mobile <span>*</span></label>
              <input type="text" class="form-control" id="mobile" name="mobile" maxlength="12" required  value="<?php echo $mobile;?>" readonly>
              <span id="mobile_error" class="error"></span> </div>
            <div class="form-group col-md-6">
              <label>Sex <span>*</span></label>
              <select id="sex" name="sex" class="form-control" readonly>
                <option <?php if(!isset($sex)){?>selected<?php }?>></option>
                <option value="Female" <?php if($sex=='Female'){?>selected<?php }?>>Female</option>
                <option value="Male" <?php if($sex=='Male'){?>selected<?php }?>>Male</option>
              </select>
              <span id="sex_error" class="error"></span> </div>
          </div>
        </div>
          <div class="form-row">
          	<div class="form-group col-md-4 expand_cls">
              <a href="javascript:void(0);" id="expand_pinfo" class="btn btn-outline-info"><i class="fas fa-plus"></i></a>
            </div>
            <div class="form-group col-md-4 colpse_cls">
              <a href="javascript:void(0);" id="colpse_pinfo" class="btn btn-outline-info"><i class="fas fa-minus"></i></a>
            </div>
          </div>
          
          <div class="offset-md-5 col-md-3">
            <input type="hidden" name="_addpatient" value="addpatient">
            <button type="submit" class="btn btn-primary">Add</button>
           <a href="<?php echo base_url('management/patient/patients');?>" class="btn btn-warning">Cancel</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<style>
	.colpse{display:none;}
	.colpse_cls{display:none;}
</style>
<script>
var treament_obj = <?php echo json_encode($treatment_array);?>;
function callAjax(url,callbackFunctions){
	if(isIE()&&isIE()<10){
		if(window.XDomainRequest){
			var xdr=new XDomainRequest();
			xdr.open("GET",url,true);
			xdr.onprogress=function(){};
			xdr.ontimeout=function(){};
			xdr.onerror=function(){};
			xdr.onload=function(){
				var responseData=xdr.responseText;
				responseData=$.parseJSON(responseData);callbackFunctions(responseData);
			}
	setTimeout(function(){xdr.send();},0);}
}else{
	$.ajax({
		url:url,
		dataType:"json",type:'GET',
		success:function(responseData){
			callbackFunctions(responseData);
		},
		error:function(){
			setTimeout(function(){callAjax(url,callbackFunctions);},5000);
		}
		});
	}
}
function clear_form(){
	document.getElementById("frmPatient").reset();
}
       // valid fields Validation Form
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
	
	
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
		var discount = parseFloat($('#discount').val());
		var fees = parseFloat($('#fees').val());
		var due_amount = parseFloat($('#due_amount').val());
			
		var full_name = $("#full_name").val();
		var age = $("#age").val();
		var weight = $("#weight").val();
		var address = $("#address").val();
		var mobile = $("#mobile").val();
		var sex = $("#sex").val();
		
		
			
        var boolValidate = true;
		if (form.checkValidity() === false) {			
          event.preventDefault();
          event.stopPropagation();
		  boolValidate = false;
        }
		form.classList.add('was-validated');
		//alert(boolValidate+'*****');
		if(full_name==""){
			$("#full_name_error").html("Please enter Full Name.");		
			boolValidate = false;
		}else{ 			
			$("#full_name_error").html("");		
		}
		if(age==""){			
			$("#age_error").html("Please enter Age.");		
			boolValidate = false;
		}else{ 			
			$("#age_error").html("");		
		}
		if(weight==""){ 
			$("#weight_error").html("Please enter Weight.");		
			boolValidate = false;
		}else{ 			
			$("#weight_error").html("");		
		}
		if(address==""){
			$("#address_error").html("Please enter Address.");		
			boolValidate = false;
		}else{ 			
			$("#address_error").html("");		
		}
		if(mobile==""){
			$("#mobile_error").html("Please enter Mobile.");		
			boolValidate = false;
		}else{ 			
			$("#mobile_error").html("");		
		}
		if(sex==""){
			$("#sex_error").html("Please select Sex.");		
			boolValidate = false;
		}else{ 			
			$("#sex_error").html("");		
		}		
		
		if(isNaN(fees) || fees==""){
			$("#fees_error").html("Please enter Fees.")	;		
			boolValidate = false;
		}else if((fees < discount)){
			$("#fees_error").html("Fees should not be less than Discount.")	;		
			boolValidate = false;
		}else if((fees < due_amount)){ 	
			$("#fees_error").html("Fees should not be less than Due Amount.")			
			  boolValidate = false;
		}else{
			$("#fees_error").html("");	
			 
		}  
		
		
		if(boolValidate){
			return true;
		}else{
			event.preventDefault();
          	event.stopPropagation();
			return false;
		}
        
      }, false);
    });
  }, false);
  $("#treatment").change(function(){
	  var treatment_val = $(this).val();
	if(treatment_val !=""){
		$('#fees').prop('readonly',false);
		$('#fees').val(treament_obj[treatment_val]);
		//$('#fees').prop('readonly',true);
	}
  });
  $("#expand_pinfo").click(function(){
	$(".expand_cls").hide();
  	$(".colpse").show();
	$(".colpse_cls").show();
  });
  $("#colpse_pinfo").click(function(){
	$(".colpse_cls").hide();
  	$(".colpse").hide();
	$(".expand_cls").show();
  });
  
})();


    </script>
    
<?php $this->load->view('management/footer');?>    