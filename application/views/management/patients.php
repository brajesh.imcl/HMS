<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('assets/backend/css/daterangepicker.css');?>">
<script src="<?php echo base_url('assets/backend/js/moment.min.js');?>"></script>
<script src="<?php echo base_url('assets/backend/js/daterangepicker.js');?>" ></script>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('assets/common/css/jquery-ui.css');?>">
<script src="<?php echo base_url('assets/common/js/jquery-ui.js');?>"></script>

<div class="container-fluid">
  <div class="row">
    <?php $this->load->view('management/navigation');?>
    <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
      <div class="bg-white p-3">
        <h3 class="mb-4">All Patients <a href="<?php echo base_url('management/patient/addpatient');?>" class="float-right" title="Add Staff"><i class="fas fa-plus-square"></i></a></h3>
        <div class="filter">
          
   <form class="form-inline needs-validation" novalidate action="" method="get">
     <label  class="mb-2 mr-sm-2">Date:</label>
     <input type="text" class="form-control mb-2 mr-sm-2" id="date_range" autocomplete="off" >
     <input type="hidden" name="dt" id="dt" value="" />       

    <label for="pwd2" class="mb-2 mr-sm-2">Patient's ID:</label>
    <input type="text" class="form-control mb-2 mr-sm-2" id="p_id" name="p_id" placeholder="Patient's ID"  value="<?php echo $p_id;?>"  required>
     <label for="pwd2" class="mb-2 mr-sm-2">Ref By:</label>
      <input type="text" class="form-control mb-2 mr-sm-2" id="tags" name="tags" placeholder="Refered By" value="<?php echo $refered_by;?>" autocomplete="off"/>
       
  <button type="submit" name="_btnFilter" value="Filter" class="btn btn-primary  mb-2">Filter</button>
   <a href="<?php echo base_url('management/patient/patients');?>" class="btn btn-outline-warning mx-1 mb-2">Clear</a> 
   <a href="<?php echo base_url('management/patient/exportpatient?'.$this->input->server('QUERY_STRING'));?>" class="btn btn-outline-success mb-2">Export Excel</a>
  </form>
        </div>
        <div class="table-responsive">
          <table class="table table-fluid table-bordered table-hover table-patients">
            <thead  class="thead-light">
              <tr  class="text-center">
                <th scope="col" class="s_no">S. No</th>
                <th scope="col" class="p_id">Patient's ID</th>
                <th scope="col" class="p_n">Patient's  Name</th>
                <th scope="col" class="mo">Mobile</th>
                <th scope="col" class="r_b">Refered By</th>
                <th scope="col" class="add">Address</th>
				<th scope="col" class="add">Created</th>
                <th scope="col" class="act">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
    if($patients_data && (count((array)$patients_data)>0)){
      
    foreach((array)$patients_data as $patient_key => $patient_row){?>
              <tr>
                <td><?php echo $patient_key+1;?></td>
                <td>PID-<?php echo $patient_row['id'];?></td>
                <td><?php echo ($patient_row["full_name"]!="")?$patient_row["full_name"]:"";?></td>
                <td><?php echo $patient_row["mobile"];?></td>
                <td><?php echo $patient_row["refered_by"];?></td>
                <td><?php echo $patient_row["address"];?></td>
                <td><?php echo date('j-m-Y',strtotime($patient_row["created"]));?></td>
                <td class="mt-2"><a href="<?php echo base_url('management/patient/edit/'.$patient_row['id']);?>" title="Edit"><i class="fa fa-edit"></i></a> <a href="<?php echo base_url('management/patient/treatments/'.$patient_row['id']);?>" title="Add Treatments"><i class="fas fa-plus-square"></i></a> <a href="<?php echo base_url('management/patient/history/'.$patient_row['id']);?>"title="History"><i class="fab fa-hire-a-helper"></i></a> <a href="<?php echo base_url('management/patient/reportcard/'.$patient_row['id']);?>" title="Print"><img src="<?php echo base_url('assets/backend/images/printer-icon.png');?>"></a>
				<a href="<?php echo base_url('management/patient/prescriptionfiles/'.$patient_row['id']);?>" title="Upload Prescription"><i class="fa fa-upload" aria-hidden="true"></i></a>
				</td>
              </tr>
              <?php }}else{?>
              <tr>
                <th scope="row" colspan="7">Sorry !! No Records Found.</th>
              </tr>
              <?php }?>
            </tbody>
          </table>
        </div>
        <?php echo $links; ?>
      </div>
    </div>
  </div>
</div>
<script>
var options = {
  startDate: <?php if(isset($start_date) && $start_date !=""){ echo "'".$start_date."'";?><?php }else{?>moment().startOf('hour')<?php }?>,
    endDate: <?php if(isset($end_date) && $end_date !=""){ echo "'".$end_date."'";?><?php }else{?>moment().startOf('hour').add(0, 'hour')<?php }?>
};

     $('#date_range').daterangepicker(options, function(start, end, label) {
     console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')'); 
     $("#dt").val(start.format('YYYY-MM-DD')+','+end.format('YYYY-MM-DD'));
    })

$( function() {
    var availableTags = <?php echo $refered_list;?> 
    $( "#tags" ).autocomplete({
      source: availableTags
    });
  } );
  </script>
<?php $this->load->view('management/footer');?>
