<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<div class="container-fluid">
  <div class="row">
     <?php $this->load->view('management/navigation');?>
      <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
      <div class="bg-white p-3">
<h3 class="mb-4">All Consultants <a href="<?php echo base_url('management/consultant/add');?>" class="float-right" title="Add Staff"><i class="fas fa-plus-square"></i></a></h3>
<div class="table-responsive">
  <table class="table table-fluid table-bordered table-hover">
   <thead  class="thead-light">
    <tr>
      <th scope="col">SN. No</th>
      <th scope="col">Full Name</th>
      <th scope="col">Mobile</th>
      <th scope="col">Qualification</th>
      <th scope="col">Specialist</th>
      <th scope="col">Morning Hrs.</th>      
      <th scope="col">Evening Hrs.</th>      
      <th scope="col">Status</th>
      <th scope="col">Action</th>
    </tr>
   </thead>
  <tbody>
  <?php 
  	if(count($allconsultant)>0){ 
  	foreach($allconsultant as $key => $row){?>
    <tr>
      <th scope="row"><?php echo ++$key;?></th>
      <td><?php echo $row["name"];?></td>
      <td><?php echo $row["mobile_no"];?></td>
      <td><?php echo $row["degree"];?></td>
      <td><?php echo $row["specialist"];?></td>
      <td><?php echo $row["seating_morning"];?></td>
      <td><?php echo $row["seating_evening"];?></td>
      <td><?php echo ($row["status"])?'Active':'Inactive';?></td>
      <td>
	  <a href="<?php echo base_url('management/consultant/edit/'.$row['id']);?>"><i class="fa fa-edit"></i></a> 
	  <a href="<?php echo base_url('management/consultant/delete/'.$row['id']);?>" onclick="return confirm('Are you sure want to delete this?');" class="float-right"><i class="fa fa-trash"></i></a></td>
    </tr>
    <?php }}else{?>
    <tr>
      <th scope="row" colspan="9">No Records Found !!</th>
     </tr>
    <?php }?>
    
  </tbody>
</table>
</div>
</div>

      </div>
  </div>
</div>
<?php $this->load->view('management/footer');?>