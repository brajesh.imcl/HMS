<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<div class="container-fluid">
  <div class="row">
     <?php $this->load->view('management/navigation');?>
      <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
      <div class="bg-white p-3">
<h3 class="mb-4">All Staff <a href="<?php echo base_url('management/staff/add');?>" class="float-right" title="Add Staff"><i class="fas fa-plus-square"></i></a></h3>
<div class="table-responsive">
  <table class="table table-fluid table-bordered table-hover">
   <thead  class="thead-light">
    <tr>
      <th scope="col">SN. No</th>
      <th scope="col">Full Name</th>
      <th scope="col">Mobile</th>
      <th scope="col">Qualification</th>
      <th scope="col">Role</th>
      <th scope="col">Salary</th>      
      <th scope="col">Status</th>
      <th scope="col">Action</th>
    </tr>
   </thead>
  <tbody>
  <?php 
  	if(count($allStaff)>0){ 
  	foreach($allStaff as $key => $row){?>
    <tr>
      <th scope="row"><?php echo ++$key;?></th>
      <td><?php echo $row["staff_name"];?></td>
      <td><?php echo $row["staff_phone"];?></td>
      <td><?php echo $row["staff_degree"];?></td>
      <td><?php echo $row["role"];?></td>
      <td>&#8377; <?php echo $row["salary"];?></td>
      <td><?php echo ($row["status"])?'Active':'Inactive';?></td>
      <td>
	  <a href="<?php echo base_url('management/staff/edit/'.$row['id']);?>"><i class="fa fa-edit"></i></a> 
	  <a href="<?php echo base_url('management/staff/delete/'.$row['id']);?>" onclick="return confirm('Are you sure want to delete this?');" class="float-right"><i class="fa fa-trash"></i></a></td>
    </tr>
    <?php }}else{?>
    <tr>
      <th scope="row" colspan="8">No Records Found !!</th>
     </tr>
    <?php }?>
    
  </tbody>
</table>
</div>
</div>

      </div>
  </div>
</div>
<?php $this->load->view('management/footer');?>