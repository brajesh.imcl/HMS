<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Prescription</title>
  <?php $this->load->view('management/all_js_css');?>

<style>
.br{border-right: 2px solid red;}
 .form-prescription {height: 82.5%} 
}
.footer-prescription .copy span{color:red}
 .form-border.pi{width: 65%;}
 .form-border.rb{width: 74%;}
 .form-border.dt{width: 64%;}
 .form-border.wt{width: 50%;}
 .form-border.ag{width: 53%;}

@media print {
.
 .form-prescription {height: 82.5%}
 }


</style>

 
  </head>
  <body>
    <div class="container-prescription">

  <div class="header-border px-0"></div>
  <div class="container-fluid my-1">
  	
    <div class="row">
    
    	<div class="col-3">
          <h3 class="font-dr">डॉ. प्रणव कुमार</h3>
          <p class="clr">B.P.T. MAHE-Manipal</p>
          <p class="clr">M.P.T. - Orthopedics</p>
          <p class="clr">M.B.A. (Human Resource)</p>
          <p class="clr"></p>
          <p class="clr">MMTFL MIAP FAGE</p>         
        </div>
        <div class="col-5  px-0" align="center">
    
          <h3 class="font-dr mb-0">मणिपाल</h3>
        <p class="md">फिजियोथेरेपी एवं फिटनेस सेंटर </p>        
        <p class="clr">Reg No- L/16446</p>
        <p class="md">Cert. Health Promotion NIHFW, New Delhi </p>
        <p class="clr">स्वामी विवेकानंद रोड आदमपुर</p>
        
        </div>
   		 <div class="col-4 text-right pre-image">        
            <p class="clr"><i class="fas fa-mobile-alt"></i> 0641-2408677 <br> 9304414144</p>
            <p class="clr"><i class="fas fa-globe-asia"></i> wwww,manipalphysio.com </p>
              <p class="clr"><i class="far fa-envelope"></i> manipalphysio.bgp@gmail.com</p>
            <p class="clr">Time- 8 A.M 8 P.M. ( MON - SAT )<br>
                10 A.M. - 1 P.M. ( SUNDAY )</p>
        </div>
    
    </div>
    </div>
    <div class="heart"><img src="<?php echo base_url('assets/backend/images/heart.png');?>" class="img-fluid"></div>
   
   <div class="row form-prescription">
    <div class="col-3 prescription-image br">
      
      <img src="<?php echo base_url('assets/backend/images/skeleton.png');?>" class="img-fluid skeleton mb-3">
       <img src="<?php echo base_url('assets/backend/images/skelton2.png');?>" class="img-fluid mb-3">
       <img src="<?php echo base_url('assets/backend/images/rotation.png');?>" class="img-fluid mb-3">
       <img src="<?php echo base_url('assets/backend/images/ten.png');?>" class="img-fluid">
    </div>

    <div class="col-9 pl-0">
    	<div class="print"><a href="javascript:void(0);" onClick="return print_page(this)"><img src="<?php echo base_url('assets/backend/images/Print.png');?>"></a></div>
    <div class="back" ><a href="javascript:void(0);" onclick="window.history.go(-1); return false;" ><img src="<?php echo base_url('assets/backend/images/back.png');?>"></a></div>
    <div class=" p-1">

          <div class="border-bottom">
         <div class="row">

           <div class="col-4 border-right">
             <form class="form-inline fs-14">
               <div class="form-row">
                <div class="form-group col-12 mt-2 mb-3">
                  <label >Patient Id :</label>
                  <input class="form-border pi" value="PID-<?php echo $patient_data["id"];?>">
                </div>
                <div class="form-group col-12 mb-3">
                 <label >Ref BY :</label>
                 <input class="form-border rb"  value="<?php echo ucwords($patient_data["refered_by"]);?>" readonly>
                </div>
               </div>
             </form>
           </div>

          <div class="col-4  border-right">
            <h6 class="text-center text-danger">Name & Address </h6>
             <p class="mb-2 fs-12"><?php echo ucwords($patient_data["full_name"]);?><br> <?php echo $patient_data["address"];?></p>
          </div>

          <div class="col-4">
            <form class="form-inline fs-14">
               <div class="form-row">    
                 <div class="form-group col-7 mt-2 mb-3">
                   <label >Date :</label>
                   <input class="form-border dt" value="<?php echo date("d-m-Y",strtotime($patient_data["created"]))?>" / readonly>
                 </div>
                 <div class="form-group col-5 mt-2 mb-3">
                   <label> SEX:</label>
                   <input class="form-border" value="<?php echo $patient_data["gender"];?>" readonly>
                 </div>    
                 <div class="form-group col-7 mb-2">
                   <label>Weight :</label>
                   <input class="form-border wt" value="<?php echo $patient_data["weight"];?> Kg" readonly>
                 </div>     
                 <div class="form-group col-5 mb-2">
                   <label>Age :</label>
                   <input class="form-border ag" value="<?php echo $patient_data["age"].' '.$patient_data["yrs_mon"];?>" readonly>
                 </div>
               </div>
            </form>
          </div>

      </div>
    </div>




    
    </div>
  </div>
</div>

  <footer class="footer-prescription">
    
        <div class="row copy">
          <div class="col-12">Opinion expressed in my personal skill and capacity and no indemnity lies with me on account of brkoen therapy/ altered exercise regimen (whatever though sd s home programmes), irregular follow up or interfernce by another clinician/physiotherapist n forms of any act or opinion .
Risk about the exercise, electro therapy modalities and manipulation therapy techniques has been explanide in his/her languges.
  </div>
         <div class="col-12"align="center"><span>NOT VALID FOR MEDICO LEGAL PURPOSES</span></div> 
        </div>       
      
</footer>


  </div>
    

 
    <script>
	function print_page(ele){		
		ele.style='display:none';
		window.print();
	}
</script>
  </body>
</html>