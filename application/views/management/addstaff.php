<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<div class="container-fluid">
  <div class="row">
     <?php $this->load->view('management/navigation');?>
      <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
        <div class="bg-white p-3">
        <h3 class="mb-4"><?php echo (isset($staffinfo))?'Edit':'Add';?> Staff <a href="<?php echo base_url('management/staff/');?>" class="float-right" title="Back"><i class="fas fa-arrow-left"></i></a></h3>

        <form class="needs-validation" novalidate method="post" action="" autocomplete="off">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label >Full Name <span>*</span></label>
      <input type="text" class="form-control" id="staff_name" name="staff_name" value="<?php echo $staffinfo['staff_name'];?>" autocomplete="off" required>
      <span id="full_name_error" class="error"></span>
    </div>
    <div class="form-group col-md-6">
      <label>Mobile<span>*</span></label>
      <input type="number" class="form-control" id="staff_phone" name="staff_phone"  value="<?php echo $staffinfo['staff_phone'];?>" required>
      <span id="age_error" class="error"></span>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Email</label>
      <input type="email" class="form-control" id="staff_email" name="staff_email" value="<?php echo $staffinfo['staff_email'];?>">
      <span id="weight_error" class="error"></span>
    </div>
    <div class="form-group col-md-6">
      <label>Qualification <span>*</span></label>
      <input type="text" class="form-control" id="staff_degree" name="staff_degree" value="<?php echo $staffinfo['staff_degree'];?>" required>
      <span id="address_error" class="error"></span>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Address <span>*</span></label>
      <input type="text" class="form-control" id="staff_addr" name="staff_addr" value="<?php echo $staffinfo['staff_addr'];?>" required>
      <span id="mobile_error" class="error"></span>
    </div>
    <div class="form-group col-md-6">
      <label>Salary</label>
      <input type="number" class="form-control" id="salary" name="salary" value="<?php echo $staffinfo['salary'];?>">
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Profile Photo</label>
      <input type="file" class="form-control" id="staff_photo" name="staff_photo">
    </div>
	<div class="form-group col-md-6">
      <label>Role <span>*</span></label>
      <select id="role" name="role" class="form-control">
        <option value="">Select</option>
        <option value="Nurse">Nurse</option>
        <option value="Accountant">Accountant</option>
        <option value="Billing Manager">Billing Manager</option>
        <option value="CT Scan">CT Scan</option>
        <option value="X Ray">X Ray</option>
        <option value="House Keeping">House Keeping</option>
      </select>
      <span id="sex_error" class="error"></span>
    </div>    
  </div>    
  <div class="offset-md-5 col-md-3">  
  <button type="submit" class="btn btn-primary"><?php echo (isset($staffinfo))?'Update':'Add';?> Staff</button>
  <button type="reset" class="btn btn-warning">Reset</button>
</div>
</form>

</div>
      </div>
  </div>
</div>

<script>
       // valid fields Validation Form
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

    </script>
 <?php $this->load->view('management/footer');?>