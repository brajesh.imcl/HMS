<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
    <div class="container-prescription">

  <div class="header-border px-0"></div>
  <div class="container-fluid my-1">
  	
    <div class="row">
    
    	<div class="col-3">
          <h3 class="font-dr">डॉ. राजीव सिन्हा</h3>
        <p class="clr">पेट  ए विशेषज्ञ</p>
        </div>
        <div class="col-6  px-0" align="center">
    
          <h3 class="font-dr mb-0">DR. Rajiv Sinha</h3>
        <p class="md">MD. Medicine (Patna)</p>
        <p class="clr">Medical- Gastroenterologist, Interventional Endoscopist & General Physician</p>
        <p class="md">Medical Officer, ICU </p>
        <p class="clr">Jawhar Lal Nehru Medical College & Hospital, Bhagalpur</p>
        
        </div>
   		 <div class="col-3 text-right pre-image">
        	<img src="<?php echo base_url('assets/common/images/medico.png');?>">
            <img src="<?php echo base_url('assets/common/images/gastro.png');?>">
        </div>
    
    </div>
    </div>
    <div class="heart"><img src="<?php echo base_url('assets/common/images/heart.png');?>" class="img-fluid"></div>
   	<div class="print" style="position: fixed;left: 153px;top: 165px;"><a href="javascript:void(0);" onClick="return print_page(this)">Print</a></div>
    <div class="back" style="position: fixed;left: 153px;top: 220px;"><a href="javascript:void(0);" onclick="window.history.go(-1); return false;" >Back</a></div>
    <div class="form-prescription p-1">


  <form class="form-inline">
  <div class="form-row">    
   <div class="form-group col-md-5 mb-2">
      <label >Name</label>
      <input class="form-border" value="<?php echo ucwords($full_name);?>">
    </div>
    <div class="form-group col-md-2 mb-2">
      <label> SEX</label>
     <input class="form-border" value="<?php echo $sex;?>">
    </div>
    <div class="form-group col-md-2 mb-2">
      <label>Age</label>
      <input class="form-border" value="<?php echo $age;?> yrs">
    </div>
    <div class="form-group col-md-2 mb-2">
      <label>Weight</label>
      <input class="form-border" value="<?php echo $weight;?> kg">
    </div>    
    <div class="form-group col-md-5 mb-2">
      <label>Address</label>
      <input class="form-border" value="<?php echo $address;?>">
    </div>    
  </div>
<div class="form-row col-md-12 mb-12">
	<div class="form-group col-md-5 mb-2">
	************************************************History*********************************************************************************
    </div>
</div>
<?php foreach((array)$patient_transaction_data as $key => $transaction_row){?>
<ul>
	<li>Date- <?php echo date("d-m-Y",strtotime($transaction_row["created"]));?></li>
    <li>Clinic- <?php echo $clinic_info[$transaction_row["clinic_id"]];?></li>
    <li>Treatment- <?php echo $treatment[$transaction_row["treatment_id"]];?></li>
    <li>Fees- <?php echo number_format($transaction_row["fees"],2);?></li>
    <li>Discount- <?php echo number_format($transaction_row["discount"],2);?></li>
    <li>Remarks- <?php echo $transaction_row["remarks"];?></li>
    <li>Refered By- <?php echo ucwords($transaction_row["refered_by"]);?></li>
    <li>Paid Amount- <?php echo number_format($transaction_row["paid_amt"],2);?></li>
</ul>
<?php }?>

</form>

    
    </div>

 
<script>
	function print_page(ele){		
		ele.style='display:none';
		window.print();
	}
</script>
 <?php $this->load->view('management/footer');?>