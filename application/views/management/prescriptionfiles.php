<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('assets/backend/css/daterangepicker.css');?>">
<script src="<?php echo base_url('assets/backend/js/moment.min.js');?>"></script>
<script src="<?php echo base_url('assets/backend/js/daterangepicker.js');?>" ></script>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('assets/common/css/jquery-ui.css');?>">
<script src="<?php echo base_url('assets/common/js/jquery-ui.js');?>"></script>
<div class="container-fluid">
  <div class="row">
    <?php $this->load->view('management/navigation');?>
    <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
      <div class="bg-white p-3">
        <h3 class="mb-4">Patient's Prescription Files <a href="<?php echo base_url('management/patient/patients/');?>" class="float-right" title="Back"><i class="fas fa-arrow-left"></i></a></h3>
        <div class="filter">		
          <div class="table-responsive">
            <table class="table table-fluid table-bordered table-hover">
              <thead  class="thead-light">
                <tr>
                  <th scope="col">Name</th>
                  <th scope="col">Sex</th>
                  <th scope="col">Age</th>
                  <th scope="col">Weight</th>
                  <th scope="col">Address</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><?php echo ucwords($patientdata['full_name']);?></th>
                  <td><?php echo $patientdata['gender'];?></th>
                  <td><?php echo $patientdata['age'];?> Yrs</td>
                  <td><?php echo $patientdata['weight'];?> Kg</td>
                  <td><?php echo $patientdata['address'];?></td>
                </tr>
                <tr>
                  <th scope="row" colspan="5" class="text-center"><a href="javascript:void(0);" class="btn btn-outline-warning mb-2" onclick="openUploadModal();">Upload File</a></th>
                </tr>
              </tbody>
            </table>
			<div class="container mt-4 presc-gallery">
			 <div class="row">
			<?php foreach((array)$allPrescriptions as $key=>$row){?>
			   <div class="col-12 col-sm-3 pb-4">
			   <a title="Image 112" href="javasript:void(0);">
				<img class="thumbnail img-fluid" src="<?php echo base_url('/assets/uploads/prescriptions/'.$row['file_name']);?>">
			   </a>
			   <h6 class="mt-1"><?php echo date('d M Y',strtotime($row['created_date']));?> <span  class="float-right mr-2"> <a href="<?php echo base_url('/management/patient/downloadPrfile/'.$row['file_name']);?>" title="Download" class="mr-2"><i class="fas fa-download"></i></a> <a href="javascript:void(0);" onclick="printresult('<?php echo base_url('/assets/uploads/prescriptions/'.$row['file_name']);?>');" title="Print"><i class="fas fa-print"></i></a></span></h6></div>
			<?php }?>
			  </div>
			</div>
			
          </div>          
        </div>        
        
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('management/upload_prescription');?>
<?php $this->load->view('management/footer');?>
