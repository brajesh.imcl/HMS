<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Welcome To Clinic Adda</title>
	<?php $this->load->view('management/all_js_css');?>
  </head>
<body>
    <div class="container-prescription">

  <div class="header-border px-0"></div>
  <div class="container-fluid my-1">
  	
    <div class="row">
    
    	<div class="col-3">
          <h3 class="font-dr">डॉ. राजीव सिन्हा</h3>
        <p class="clr">पेट एवं लीवर रोग विशेषज्ञ</p>
        </div>
        <div class="col-6  px-0" align="center">
    
          <h3 class="font-dr mb-0">DR. Rajiv Sinha</h3>
        <p class="md">MD. Medicine (Patna)</p>
        <p class="clr">Medical- Gastroenterologist, Interventional Endoscopist & General Physician</p>
        <p class="md">Medical Officer, ICU </p>
        <p class="clr">Jawhar Lal Nehru Medical College & Hospital, Bhagalpur</p>
        
        </div>
   		 <div class="col-3 text-right pre-image">
        	<img src="<?php echo base_url('assets/common/images/medico.png');?>">
            <img src="<?php echo base_url('assets/common/images/gastro.png');?>">
        </div>
    
    </div>
    </div>
    <div class="heart"><img src="<?php echo base_url('assets/common/images/heart.png');?>" class="img-fluid"></div>
   	<div class="print" ><a href="javascript:void(0);" onClick="return print_page(this)"><img src="<?php echo base_url('assets/backend/images/Print.png');?>"></a></div>
    <div class="back"><a href="javascript:void(0);" onclick="window.history.go(-1); return false;" ><img src="<?php echo base_url('assets/backend/images/back.png');?>"></a></div>
    <div class="form-prescription p-1">

       <div class="border-bottom">
         <div class="row">

           <div class="col-4 border-right">
             <form class="form-inline fs-14">
               <div class="form-row">
                <div class="form-group col-12 mt-2 mb-3">
                  <label >Patient Id :</label>
                  <input class="form-border pi" value="PID-<?php echo $patient_data["id"];?>" readonly>
                </div>
                <div class="form-group col-12 mb-3">
                 <label >Ref BY :</label>
                 <input class="form-border rb"  value="<?php echo ucwords($patient_data["refered_by"]);?>" readonly>
                </div>
               </div>
             </form>
           </div>

          <div class="col-4  border-right">
            <h6 class="text-center text-danger">Name & Address </h6>
             <p class="mb-2 fs-12"><?php echo $patient_data["full_name"];?><br> <?php echo $patient_data["address"];?></p>
          </div>

          <div class="col-4">
            <form class="form-inline fs-14">
               <div class="form-row">    
                 <div class="form-group col-7 mt-2 mb-3">
                   <label >Date :</label>
                   <input class="form-border dt" value="<?php echo date("d-m-Y",strtotime($patient_data["created"]))?>" / readonly>
                 </div>
                 <div class="form-group col-5 mt-2 mb-3">
                   <label> SEX:</label>
                   <input class="form-border" value="<?php echo $patient_data["gender"];?>" readonly>
                 </div>    
                 <div class="form-group col-7 mb-2">
                   <label>Weight :</label>
                   <input class="form-border" value="<?php echo $patient_data["weight"];?> Kg" readonly>
                 </div>     
                 <div class="form-group col-5 mb-2">
                   <label>Age :</label>
                   <input class="form-border" value="<?php echo $patient_data["age"].' '.$patient_data["yrs_mon"];?>" readonly>
                 </div>
               </div>
            </form>
          </div>

      </div>
    </div>
    </div>
<footer class="footer-prescription">
<div class="row copy">
	<div class="col-6"><span>गैस्ट्रो क्लिनिक :</span> घंटाघर राजा एस. एन. रोड, मसाकचक, भागलपुर  </div>
	<div class="col-3 text-center"><img src="<?php echo base_url('assets/backend/images/telefon.png');?>"> 0641-2301818</div>
	<div class="col-3 text-right">15 दिनों बाद फीस पुनः लगेगा</div>
</div>
</footer>
<script>
function print_page(ele){		
	//ele.style='display:none';
	window.print();
}
</script>
 </div>
 </body>
</html>