<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<div class="container-fluid">
  <div class="row">
     <?php $this->load->view('management/navigation');?>
      <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
      <div class="bg-white p-3">
<h3 class="mb-4">Treatment's List <a href="<?php echo base_url('management/treatment/add');?>" class="float-right" title="Treatment's List"><i class="fas fa-plus-square"></i></a></h3>
<div class="table-responsive">
  <table class="table table-fluid table-bordered table-hover">
   <thead  class="thead-light">
    <tr>
      <th scope="col">SN. No</th>
      <th scope="col">Treatment's Name</th>
      <th scope="col">Fees</th>       
      <th scope="col">Status</th>
      <th scope="col">Action</th>
    </tr>
   </thead>
  <tbody>
  <?php 
  	if(count($allTreatment)>0){ 
  	foreach($allTreatment as $key => $row){?>
    <tr>
      <th scope="row"><?php echo ++$key;?></th>
      <td><?php echo $row["name"];?></td>     
      <td>&#8377; <?php echo number_format($row["fees"],2);?></td>
      <td><?php echo ($row["status"])?'Active':'Inactive';?></td>
      <td>
	  <a href="<?php echo base_url('management/treatment/edit/'.$row['id']);?>"><i class="fa fa-edit"></i></a> 
	  <a href="<?php echo base_url('management/treatment/delete/'.$row['id']);?>" onclick="return confirm('Are you sure want to delete this?');" class="float-right"><i class="fa fa-trash"></i></a></td>
    </tr>
    <?php }}else{?>
    <tr>
      <th scope="row" colspan="8">No Records Found !!</th>
     </tr>
    <?php }?>
    
  </tbody>
</table>
</div>
</div>

      </div>
  </div>
</div>
<?php $this->load->view('management/footer');?>