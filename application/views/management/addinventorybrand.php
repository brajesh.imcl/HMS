<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<div class="container-fluid">
  <div class="row">
    <?php $this->load->view('management/navigation');?>
    <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
      <div class="bg-white p-3">
        <h3 class="mb-4"><?php if($brand_info["brand_name"] !=""){?>Update<?php }else{?>Add<?php }?> Brand <a href="<?php echo base_url('management/inventory/brandrlist');?>" class="float-right" title="Back"><i class="fas fa-arrow-left"></i></a></h3>
        <form class="needs-validation" novalidate autocomplete="off" method="post" action="">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Category <span class="star">*</span></label>
              <select id="cat_id" name="cat_id" class="form-control">
                <option value="">Select</option>
                <?php foreach($all_cat as $key =>$row){?>
                <option value="<?php echo $row["id"];?>" <?php if($row["id"]==$brand_info["cat_id"]){?>selected<?php }?>><?php echo $row["category_name"];?></option>
                <?php }?>
                
              </select>
              <span id="sex_error" class="error"></span> </div>
            <div class="form-group col-md-6">
              <label>Brand Name <span class="star">*</span></label>
              <input type="text" class="form-control" name="brand_name" id="brand_name"  placeholder="Brand Name" value="<?php echo $brand_info["brand_name"];?>" required>
            </div>
          </div>
          <div class="offset-md-5 col-md-3">
            <button type="submit" class="btn btn-primary">
            <?php if($brand_info["brand_name"] !=""){?>
            Update
            <?php }else{?>
            Add
            <?php }?>
            </button>
            <a href="<?php echo base_url('management/inventory/brandrlist');?>" class="btn btn-warning">Cancel</a> </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
       // valid fields Validation Form
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();


    </script>
<?php $this->load->view('management/footer');?>