<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Welcome To Clinic Adda</title>
	<?php $this->load->view('management/all_js_css');?>
  </head>
  <body class="bglogin-n">

 <div class="container-login">
    <div class="login-form">   
      <h3 class="text-center mb-4" >Clinic Panel</h3>
	  <?php echo ($this->session->flashdata('flash_msg'))?'<span style="color:yellow;">'.$this->session->flashdata('flash_msg').'</span>':'';?>
        <?php echo form_open('management/login',$formAttr);?>
          <div class="login-jt-wrapper form-group">
            <label for="Email1">User Name </label>
            <input type="text" class="form-control" name="username" id="username" placeholder="Email" required>
            <span class="login-jt-icon"><i class="far fa-envelope"></i></span>
			<div class="danger"><?php echo form_error('mobile'); ?></div>
          </div>
          <div class="login-jt-wrapper form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password" required>
            <span class="login-jt-icon"><i class="fas fa-lock"></i></span>
			<div class="danger"><?php echo form_error('password'); ?></div>
          </div>
          <div class="form-row mt-3">
          <div class="checkbox col-7 pl-0">
            <label>
              <input type="checkbox">Remember Password
            </label>
          </div>
          <div class="text-right col-5 pl-0">            
            <a class="txt2" href="#">
              Forgot Password?
            </a>
          </div>
          <div class="col-md-3 offset-md-4 mt-3"><button type="submit" class="btn btn-success">SIGN IN</button></div>
          </div>
       </form>
     </div>
    </div>    
  </body>
</html>