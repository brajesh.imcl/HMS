<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<div class="container-fluid">
  <div class="row">
     <?php $this->load->view('management/navigation');?>
      <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
      <div class="bg-white p-3">
<h3 class="mb-4">Today's Appointments <a href="<?php echo base_url('management/dashboard/bookappointment');?>" class="float-right" title="Book Appointment"><i class="fas fa-plus-square"></i></a></h3>
<div class="filter">  
	 <?php if(count($allappointments) > 0){?>
	  <a href="<?php echo base_url('management/dashboard/export_appointments');?>" class="btn btn-outline-warning mb-2 float-right">Export Excel</a> 
     <?php }?>    
    
    </div>
    <div class="table-responsive">
  <table class="table table-fluid table-bordered table-hover">
   <thead  class="thead-light">
    <tr>
      <th scope="col">Appointment No</th>
      <th scope="col">Patient's Full Name</th>
      <th scope="col">Mobile</th>
      <th scope="col">Address</th>
      <th scope="col">Status</th>
    </tr>
   </thead>
  <tbody>
  <?php 
  	if(count($allappointments) > 0){
  	foreach((array)$allappointments as $key=>$row){
  ?>
    <tr>
      <th scope="row"><?php echo ++$key;?></th>
      <td><?php echo $row['name'];?></td>
      <td><?php echo $row['mobile'];?></td>
      <td><?php echo $row['address'];?></td>
      <td>In Queue <a href="<?php echo base_url('management/patient/addpatient/?'.http_build_query($row));?>" class="float-right" title="Add Patient"><i class="fas fa-plus-square"></i></a></td>
    </tr>
  <?php }}else{?>   
  <tr>
      <th scope="row" colspan="6">Sorry !! No appointments for today.</th>      
    </tr> 
  <?php }?>
  </tbody>
</table>
</div>
</div>

      </div>
  </div>
</div>
<?php $this->load->view('management/footer');?>