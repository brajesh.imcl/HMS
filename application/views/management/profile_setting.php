<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<div class="container-fluid">
  <div class="row">
     <?php $this->load->view('management/navigation');?>
      <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
        <div class="bg-white p-3">
        <h3 class="mb-4">Profile Settings <a href="<?php echo base_url('management/consultant/');?>" class="float-right" title="Back"><i class="fas fa-arrow-left"></i></a></h3>

<form class="needs-validation" novalidate method="post" action="" autocomplete="off" enctype="multipart/form-data">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label >Full Name <span class="star">*</span></label>
      <input type="text" class="form-control" id="clinic_name" name="clinic_name" value="<?php echo $clinicInfo['clinic_name'];?>" autocomplete="off" required>
      <span id="full_name_error" class="error"></span>
    </div>
    <div class="form-group col-md-6">
      <label>Mobile <span class="star">*</span></label>
      <input type="number" class="form-control" id="clinic_phone" name="clinic_phone"  value="<?php echo $clinicInfo['clinic_phone'];?>" required>
      <span id="age_error" class="error"></span>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Email</label>
      <input type="email" class="form-control" id="clinic_email" name="clinic_email" value="<?php echo $clinicInfo['clinic_email'];?>">
      <span id="weight_error" class="error"></span>
    </div>
    <div class="form-group col-md-6">
      <label>Address <span class="star">*</span></label>
      <input type="text" class="form-control" id="clinic_addr" name="clinic_addr" value="<?php echo $clinicInfo['clinic_addr'];?>" required>
      <span id="address_error" class="error"></span>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>UserName <span class="star">*</span></label>
      <input type="text" class="form-control" <?php if($clinicInfo){?> disabled <?php }?> value="<?php echo $clinicInfo['user_id'];?>" required>
      
    </div>
    <div class="form-group col-md-6">
      <label>Password <span class="star">*</span></label>
      <input type="password" class="form-control" id="password" name="password" value="<?php echo $clinicInfo['password'];?>" required>
	  <?php if($clinicInfo){?>
		<input type="hidden" class="form-control" id="cpassword" name="cpassword" value="<?php echo $clinicInfo['password'];?>">
	  <?php }?>
    </div>
  </div>
  <div class="form-row">    
	<div class="form-group col-md-6">
      <label>Clinic Logo</label>
      <input type="file" class="form-control" id="clinic_photo" name="clinic_photo">
	  <?php if(isset($uploadData['error']))echo '<font color="red">'.$uploadData['error'].'</font>';?>
    </div>	
  </div>    
  <div class="offset-md-5 col-md-3">  
  <button type="submit" class="btn btn-primary">Update</button>
  <button type="reset" class="btn btn-warning">Reset</button>
</div>
</form>

</div>
      </div>
  </div>
</div>

<script>
       // valid fields Validation Form
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

    </script>
<?php $this->load->view('management/footer');?>>