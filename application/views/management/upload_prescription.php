<div id="uploadModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">			
			<h4 class="modal-title">Upload Prescription File</h4>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		</div>
		<div class="modal-body">
			<!-- Form -->
			<form method='post' id="uploadprescription" action='' enctype="multipart/form-data">
				Select file : <input type='file' name='prescription_file' id='prescription_file' class='form-control' required><br>
				<input type="hidden" name="patient_id" id="patient_id" value="<?php echo $patientdata['id'];?>">
				<input type='submit' class='btn btn-info' value='Upload' id='upload'>
			</form>
			<!-- Preview-->
			<div id='preview'></div>
		</div>		
	</div>
  </div>
</div>

<div tabindex="-1" class="modal fade" id="fileModal" role="dialog">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
  <div class="modal-body">
    
  </div>
  <div class="modal-footer">
    <button class="btn btn-warning" data-dismiss="modal">Close</button>
  </div>
   </div>
  </div>
</div>
<script type='text/javascript'>
function printresult(contents){
var printWindow = window.open('', '', 'height=600,width=800');
printWindow.document.write('<html><head><title></title>');
printWindow.document.write('</head><body >');
printWindow.document.write('<div style="width:100%; text-align:center"><img src="'+contents+'" /></div>');
printWindow.document.write('</body></html>');
printWindow.document.close();
setTimeout(function () {printWindow.print();}, 500);				
}

function openUploadModal(patientId){		
		$('#uploadModal').modal('show');
		$('#preview').html('');
}

$(document).ready(function(){
	$('#uploadprescription').submit(function(e){
        e.preventDefault();
		var fd = new FormData();
		var files = $('#prescription_file')[0].files[0];
		fd.append('prescription_file',files);
		fd.append('request',1);
		fd.append('patient_id',$('#patient_id').val());

		// AJAX request
		$.ajax({
			url: '<?php echo base_url('management/patient/uploadprescription');?>',
			type: 'post',
			data: fd,
			contentType: false,
			processData: false,
			dataType:'json',
			beforeSend : function(){
				$('#preview').html('Uploading...');	
			},
			success: function(response){
				if(response.status ==1){
					// Show image preview
					window.location.reload(true);
					//$('#preview').html("<img src='"+response.filepath+"' width='100' height='100' style='display: inline-block;'>");
					//$('#prescription_file').val('');	
				}else{
					$('#preview').html(response.message);
				}
			}
		});
	});
	
$(document).ready(function() {
	$('.thumbnail').click(function(){
		$('#fileModal .modal-body').empty();
		var title = $(this).parent('a').attr("title");
		$('#fileModal .modal-title').html(title);
		$($(this).parents('div').html()).appendTo('#fileModal .modal-body');
		$('#fileModal').modal({show:true});
});
});
	
});
</script>