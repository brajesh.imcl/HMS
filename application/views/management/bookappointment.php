<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<div class="container-fluid">
  <div class="row">
     <?php $this->load->view('management/navigation');?>
      <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
        <div class="bg-white p-3">
	  <h3 class="mb-4">Today's Appointment <a href="<?php echo base_url('management/dashboard/appointment');?>" class="float-right" title="Back"><i class="fas fa-arrow-left"></i></a></h3>
<form class="needs-validation" novalidate autocomplete="off" method="post">

<div class="form-row">
    <div class="form-group col-12">
      <label>Doctor</label>
      <select class="form-control" name="consultant_id" id="consultant_id" required>
        	<option value="">Select</option>
			<?php foreach($allDocters as $key=>$row){?>
				<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
			<?php }?>
      </select>	  
    </div>
  </div>
  
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Full Name</label>
      <input type="text" class="form-control" name="name" id="name"  placeholder="Full Name" required>
    </div>
	<div class="form-group col-md-6">
      <label>Mobile</label>
      <input type="text" class="form-control" name="mobile" id="Mobile" required min="10" max="10">
    </div>
  </div>
  
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Address</label>
      <input type="text" class="form-control" name="address" id="address" required>
    </div>
    <div class="form-group col-md-6">
      <label>Sex</label>
      <select id="" class="form-control" name="gender" id="gender">
        	<option value="">Select</option>
			<option value="F">Female</option>
			<option value="M">Male</option>
      </select>	  
    </div>
  </div>  
  <div class="offset-md-5 col-md-3">
  <button type="submit" class="btn btn-primary">Book Now</button>
  <button type="reset" class="btn btn-warning">Reset</button>
</div>
</form>
</div>

      </div>
  </div>
</div>

<script>
       // valid fields Validation Form
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

</script>
<?php $this->load->view('management/footer');?>