<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<div class="container-fluid">
  <div class="row">
  <?php $this->load->view('management/navigation');?>
  <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
   <div class="bg-white p-3">
<h3 class="mb-4">Reports</h3>


<!--<form class="form-inline  needs-validation" novalidate>
  <div class="form-group mb-2">
    <label>Date</label>
    <input type="date" class="form-control" id="" required>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <label>Name</label>
    <input type="text" class="form-control" id="" placeholder="" required>
  </div>
    <div class="form-group mb-2">
    <label class="col-form-label">Ref By</label>
    <input type="text" class="form-control" id="" required>
  </div>
  <button type="submit" class="btn btn-primary mx-sm-3  mb-2">Filter</button>
  <button type="submit" class="btn btn-outline-warning mb-2">Reset</button>
</form>-->


<div class="table-responsive">
  <table class="table table-fluid table-bordered table-hover mt-4">
   <thead  class="thead-light">
    <tr>
      <th scope="col">S. No.</th>
      <th scope="col">Patient's Full Name</th>
      <th scope="col">Refered By</th>
      <th scope="col">Address</th>
    </tr>
   </thead>
  <tbody>
    <tr>
      <th scope="row" colspan="4">This page is under developement.</th>      
    </tr>
  </tbody>
</table>
</div>
<!--<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-end">
    <li class="page-item disabled">
      <a class="page-link" href="#" tabindex="-1">Previous</a>
    </li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item">
      <a class="page-link" href="#">Next</a>
    </li>
  </ul>
</nav>-->
</div>

      </div>
  </div>
</div>
  
    <script>
       // valid fields Validation Form
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();


    </script>
 <?php $this->load->view('management/footer');?>