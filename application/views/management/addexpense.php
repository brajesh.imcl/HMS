<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<div class="container-fluid">
  <div class="row">
    <?php $this->load->view('management/navigation');?>
    <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
      <div class="bg-white p-3">
        <h3 class="mb-4">Add Expense <a href="<?php echo base_url('management/expense');?>" class="float-right" title="Back"><i class="fas fa-arrow-left"></i></a></h3>
        <form class="needs-validation" novalidate autocomplete="off" method="post" action="">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Goods Name <span class="star">*</span></label>
              <input type="text" class="form-control" name="goods_name" id="goods_name"  placeholder="Goods Name" value="<?php echo $expense_info["goods_name"];?>" required>
            </div>
            <div class="form-group col-md-6">
              <label>Amount <span class="star">*</span></label>
              <input type="text" class="form-control" name="debit_amt" id="debit_amt"  min="1"  placeholder="0.00" value="<?php echo number_format($expense_info["debit_amt"],2);?>" required>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Remark <span class="star">*</span></label>
              <input type="text" class="form-control" name="remark" id="remark"  placeholder="Write the description here." value="<?php echo $expense_info["remark"];?>" required>
            </div>
            <div class="form-group col-md-6">
              <label>Employee <span class="star">*</span></label>
              <select id="staff_id" name="staff_id" class="form-control" required>
                <option selected value="">Select</option>
                <?php foreach((array)$all_employee as $key => $row){?>
                <option value="<?php echo $row["id"];?>" <?php if( $row["id"] == $expense_info["staff_id"]){?>selected<?php }?>><?php echo $row["staff_name"];?></option>
                <?php }?>
              </select>
            </div>
          </div>
          <div class="offset-md-5 col-md-3">
            <button type="submit" class="btn btn-primary">Add</button>
            <a href="<?php echo base_url('management/expense');?>" class="btn btn-warning">Cancel</a> </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
       // valid fields Validation Form
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();


    </script>
<?php $this->load->view('management/footer');?>