<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<div class="container-fluid">
  <div class="row">
    <?php $this->load->view('management/navigation');?>
    <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
      <div class="bg-white p-3">
        <h3 class="mb-4"> Add Order <a href="<?php echo base_url('management/inventory/productlist');?>" class="float-right" title="Back"><i class="fas fa-arrow-left"></i></a></h3>
        <form class="needs-validation" novalidate autocomplete="off" method="post" action="">
        <div class="form-row">
            <div class="form-group col-md-6">
              <label>Receiver Name <span class="star">*</span></label>
              <input type="text" class="form-control" name="inventory_order_name" id="inventory_order_name"  placeholder="Receiver Name" value="" required >
            </div>
            <div class="form-group col-md-6">
              <label>Receiver Address <span class="star">*</span></label>
              <input type="text" class="form-control" name="inventory_order_address" id="inventory_order_address"  placeholder="Receiver Address" value="" required >
            </div>
          </div>
        
        <div class="form-row">
            <div class="form-group col-md-6">
            <label>Seller Name<span class="star">*</span></label>             
              <select id="staff_id" name="staff_id" class="form-control" required> 
              <option value="">Select</option>
              <?php foreach($all_employee as $key => $row){?>             
                <option value="<?php echo $row["id"];?>"><?php echo $row["staff_name"];?></option>
              <?php  }?>
              </select>              
            </div>
            <div class="form-group col-md-6">
             <label>Payment Status <span class="star">*</span></label>              
              <select id="payment_status" name="payment_status" class="form-control" >              
                <option value="cash">Cash</option>
                 <option value="credit">Credit</option>
                
              </select>
              
            </div>
          </div>
        <div class="form-row">
        	<div class="form-group col-md-6">
             <label>Purchase Quantity <span class="star">*</span></label>
              <input type="text" class="form-control" name="inventory_order_quantity" id="inventory_order_quantity"  placeholder="0" value="" required >
            </div>
            <div class="form-group col-md-6">
             <label>Amount to be paid <span class="star">*</span></label>
              <input type="text" class="form-control" name="inventory_order_total" id="inventory_order_totals"  placeholder="0.00" value="" required readonly />
            </div>
            
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Category <span class="star">*</span></label>
              <select id="cat_id" name="cat_id" class="form-control" readonly>
                <option value="">Select</option>
                <?php foreach($all_cat as $key =>$row){?>
                <option value="<?php echo $row["id"];?>" <?php if($row["id"]==$product_info["cat_id"]){?>selected<?php }?>><?php echo $row["category_name"];?></option>
                <?php }?>
              </select>
            </div>
            <div class="form-group col-md-6">
              <label>Brand <span class="star">*</span></label>
              <select id="brand_id" name="brand_id" class="form-control" readonly>
                <option value="">Select</option>
                <?php foreach($all_brand as $key =>$row){?>
                <option value="<?php echo $row["id"];?>" <?php if($row["id"]==$product_info["brand_id"]){?>selected<?php }?>><?php echo $row["brand_name"];?></option>
                <?php }?>
              </select>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Product Name <span class="star">*</span></label>
              <input type="text" class="form-control" name="product_name" id="product_name"  placeholder="Product Name" value="<?php echo $product_info["product_name"];?>" required readonly>
            </div>
            <div class="form-group col-md-6">
              <label>Product Unit <span class="star">*</span></label>
              <select id="product_unit" name="product_unit" class="form-control" readonly>
                <option value="">Select</option>
                <?php foreach($product_unit as $key =>$value){?>
                <option value="<?php echo $key;?>" <?php if($key==$product_info["product_unit"]){?>selected<?php }?>><?php echo $value;?></option>
                <?php }?>
              </select>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Product Quantity <span class="star">*</span></label>
              <input type="text" class="form-control" name="product_quantity" id="product_quantity"  placeholder="Product Quantity" value="<?php echo $product_info["product_quantity"];?>" required readonly />
            </div>
            <div class="form-group col-md-6">
              <label>Product Price <span class="star">*</span></label>
              <input type="text" class="form-control" name="product_base_price" id="product_base_price"  placeholder="Product Price"  value="<?php echo $product_info["product_base_price"];?>" required readonly />
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Product Description <span class="star">*</span></label>
              <textarea class="form-control" name="product_description" id="product_description"  placeholder="Product Description" required readonly><?php echo $product_info["product_description"];?></textarea>
            </div>
            <div class="form-group col-md-6">
              <label>Product Tax(%)</label>
              <input type="text" class="form-control" name="product_tax" id="product_tax"  placeholder="Product Tax" value="<?php echo $product_info["product_tax"];?>" readonly />
            </div>
          </div>
          <div class="offset-md-5 col-md-3">
            <button type="submit" class="btn btn-primary">
            <?php if($brand_info["brand_name"] !=""){?>
            Update
            <?php }else{?>
            Add
            <?php }?>
            </button>
            <a href="<?php echo base_url('management/inventory/productlist');?>" class="btn btn-warning">Cancel</a> </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  function rupees_format(x){
	return x.toFixed(2);	
   }
  // valid fields Validation Form
(function() {
  'use strict';
  
  $("#inventory_order_quantity").on("change",function(){ 
 
	var p_price = $("#product_base_price").val();
	var o_quantity = $("#inventory_order_quantity").val();
	if(parseInt(o_quantity) > 0){
		//alert(rupees_format(o_quantity*p_price));
		$('#inventory_order_totals').prop('readonly',false);
		$('#inventory_order_totals').val(rupees_format(o_quantity*p_price));
		$('#inventory_order_totals').prop('readonly',true);
	}
  });
  
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
		
	var o_quantity = $("#inventory_order_quantity").val();
    var p_quantity = $("#product_quantity").val();
  if(parseInt(o_quantity) > parseInt(p_quantity)){
	  alert("Purchase Quantity should not be greater than Product Quantity");
	  return false;
  }
  
		
      }, false);
    });
  }, false);
  
  
})();


    </script>
<?php $this->load->view('management/footer');?>