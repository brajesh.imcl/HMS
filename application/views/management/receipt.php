<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <?php $this->load->view('management/all_js_css');?>

   <title>Receipt</title>
    
    <style>
      .fs-14{font-size: 14px} 
      .form-border.pi{width: 72%;}
      .form-border.rb{width: 80%;}
      .form-border.dt{width: 69%;}
      .form-prescription{height: 6% !important}
      .form-border.rw{width: 82%;}
      .container-receipt{height: 800px !important}

@media print {
.container-prescription{border: none;}
 
 }
    </style>
  </head>
  <body>
    <div class="container-prescription container-receipt">

  <div class="header-border px-0"></div>
  <div class="container-fluid my-1">
  	
     
        <div class=" px-0" align="center">
    
          <h3 class="font-dr mb-0"><?php echo $clinicInfo['clinic_name'];?></h3>
        <p class="md"><?php echo $clinicInfo['clinic_addr'];?></p>
        <p class="clr"><i class="fas fa-mobile-alt"></i> : &nbsp; <?php echo $clinicInfo['clinic_phone'];?></p>
        
        
        </div>
   		
    
    
    </div>
  <div class="print" ><a href="javascript:void(0);" onClick="return print_page(this)"><img src="<?php echo base_url('assets/backend/images/Print.png');?>"></a></div>
    <div class="back"><a href="javascript:void(0);" onclick="window.history.back(); return false;" ><img src="<?php echo base_url('assets/backend/images/back.png');?>"></a></div>
   
    <div class="p-o">
 
   <div class="border-bottom  border-top">
    <div class="row">
      <div class="col-6 border-right mt-2">

         <form class="form-inline fs-14 pl-2">
  <div class="form-row">
    <div class="form-group col-12 mb-2">
      <label >Bill NO :</label>
      <span class="form-border pi"><strong><?php echo rand(1,999);?></strong></span>
    </div>
    <div class="form-group col-12 mb-2">
      <label >Date :</label>
      <span class="form-border rb"><strong><?php echo date('j M Y');?></strong></span>
    </div>
    </div>
</form>

      </div>
       <div class="col-6">
         <h6 class="text-center text-danger">Name & Address </h6>
         <p class="mb-2 fs-14"><?php echo $patientInfo['full_name'].' <br> '.$patientInfo['address'];?></p>
       </div>
    </div>
   </div> 


    </div>

    <div class="table-responsive">
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">S.N.</th>
      <th scope="col">Test Name</th>
      <th scope="col">Amount</th>
    </tr>
  </thead>
  <tbody>
  <?php $toalAmout = 0; foreach(($treatments) as $key=>$row){?>
    <tr>
      <th scope="row"><?php echo ++$key;?></th>
      <td><?php echo $row['name'];?></td>
      <td>₹ <?php echo $row['paid_amt'];?></td>
    </tr>
  <?php $toalAmout+=$row['paid_amt'];}?>    
    <tr>      
      <td colspan="2" class="text-right">Total Amount</td>
      <td>₹ <?php echo number_format($toalAmout,2);?></td>
    </tr>
  </tbody>
</table>
     </div> 

   
      <div class="row px-3 pt-4">
        <div class="col-9">
         <form class="form-inline fs-14">
           <div class="form-row w-100 ">
            <div class="form-group col-12 mb-2">
              <label >Rupees In words :</label>
              <span class="form-border rw"><strong><?php echo convert_number($toalAmout);?></strong></span>
            </div>
           </div>
         </form>
        </div>
        <div class="col-3 pt-2">
          <p class="text-center">Auth. Sign</p>
        </div>
       </div>
  
      

  </div>
   <script>
function print_page(ele){		
	//ele.style='display:none';
	window.print();
}
</script>
  </body>
</html>

