<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<div class="container-fluid">
  <div class="row">
     <?php $this->load->view('management/navigation');?>
      <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
        <div class="bg-white p-3">
        <h3 class="mb-4"><?php echo (isset($staffinfo))?'Edit':'Add';?> Consultant <a href="<?php echo base_url('management/consultant/');?>" class="float-right" title="Back"><i class="fas fa-arrow-left"></i></a></h3>

<form class="needs-validation" novalidate method="post" action="" autocomplete="off">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label >Full Name <span>*</span></label>
      <input type="text" class="form-control" id="name" name="name" value="<?php echo $staffinfo['name'];?>" autocomplete="off" required>
      </div>
    <div class="form-group col-md-6">
      <label>Mobile<span>*</span></label>
      <input type="number" class="form-control" id="mobile_no" name="mobile_no"  value="<?php echo $staffinfo['mobile_no'];?>" required>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Email</label>
      <input type="email" class="form-control" id="email" name="email" value="<?php echo $staffinfo['email'];?>">
      <span id="weight_error" class="error"></span>
    </div>
    <div class="form-group col-md-6">
      <label>Qualification <span>*</span></label>
      <input type="text" class="form-control" id="degree" name="degree" value="<?php echo $staffinfo['degree'];?>" required>
     </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Specialist <span>*</span></label>
      <input type="text" class="form-control" id="specialist" name="specialist" value="<?php echo $staffinfo['specialist'];?>" required>
      <span id="mobile_error" class="error"></span>
    </div>
	
	<div class="form-group col-md-6">
      <label>Awards</label>
      <input type="text" class="form-control" id="awards" name="awards" value="<?php echo $staffinfo['awards'];?>">
     </div>	
  </div>
  
  <div class="form-row">  
  <div class="form-group col-md-6">
      <label>Registration No</label>
      <input type="number" class="form-control" id="licence_no" name="licence_no" value="<?php echo $staffinfo['licence_no'];?>">
    </div>	
    <div class="form-group col-md-6">
      <label>Address <span>*</span></label>
      <input type="text" class="form-control" id="address" name="address" value="<?php echo $staffinfo['address'];?>" required>
     </div>    
  </div> 
  
  <div class="form-row">  
  <div class="form-group col-md-6">
      <label>Seating Hrs (Morning)</label>
      <input type="time" class="form-control" id="seating_morning" name="seating_morning" value="<?php echo $staffinfo['seating_morning'];?>">
    </div>
	
    <div class="form-group col-md-6">
      <label>Seating Hrs (Evening)</label>
      <input type="time" class="form-control" id="seating_evening" name="seating_evening" value="<?php echo $staffinfo['seating_evening'];?>">
      </div>    
  </div>
  
  <div class="form-row">  
    <div class="form-group col-md-6">
      <label>Profile Photo</label>
      <input type="file" class="form-control" id="photo" name="photo">
    </div>
  </div> 
  
  <div class="offset-md-5 col-md-3">  
  <button type="submit" class="btn btn-primary"><?php echo (isset($staffinfo))?'Update':'Add';?> Consultant</button>
  <button type="reset" class="btn btn-warning">Reset</button>
</div>
</form>

</div>
      </div>
  </div>
</div>

<script>
       // valid fields Validation Form
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

    </script>
<?php $this->load->view('management/footer');?>>