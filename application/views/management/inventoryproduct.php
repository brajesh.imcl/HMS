<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('management/header');
?>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('assets/backend/css/daterangepicker.css');?>">
<script src="<?php echo base_url('assets/backend/js/moment.min.js');?>"></script>
<script src="<?php echo base_url('assets/backend/js/daterangepicker.js');?>" ></script>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('assets/common/css/jquery-ui.css');?>">
<script src="<?php echo base_url('assets/common/js/jquery-ui.js');?>"></script>
<div class="container-fluid">
  <div class="row">
    <?php $this->load->view('management/navigation');?>
    <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
      <div class="bg-white p-3">
        <h3 class="mb-4">Product List <a href="<?php echo base_url('management/inventory/addproduct');?>" class="float-right" title="Add Product"><i class="fas fa-plus-square"></i></a></h3>
        <div class="filter">
          <form class="form-inline  needs-validation" novalidate action="" method="get">
            <div class="form-group mx-sm-2 mb-2">
              <label>Search By Product Name</label>
              <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Search"  value="<?php echo $product_name;?>" autocomplete="off" required>
            </div>
            <button type="submit" name="_btnFilter" value="Filter" class="btn btn-primary mx-sm-2 mb-2">Filter</button>
            <a href="<?php echo base_url('management/productlist');?>" class="btn btn-outline-warning mb-2">Clear</a>
          </form>
        </div>
        <div class="table-responsive">
          <table class="table table-fluid table-bordered table-hover">
            <thead  class="thead-light">
              <tr>
                <th scope="col">Sr. No</th>
                <th scope="col">Date</th>
                <th scope="col">Cateogry Name</th>
                <th scope="col">Brand Name</th>
                <th scope="col">Product Name</th>
                <th scope="col">Product Quantity</th>
                <th scope="col">Product Base Price (Rs.)</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
  	if(count($all_product)>0){ 
  	foreach($all_product as $key => $row){?>
              <tr>
                <th scope="row"><?php echo ++$key;?></th>
                <td><?php echo date("d-m-Y",strtotime($row["created"]));?></td>
                <td><?php echo $all_cat[$row["cat_id"]]["category_name"];?></td>
                <td><?php echo $all_brand[$row["brand_id"]]["brand_name"];?></td>
                <td><?php echo $row["product_name"];?></td>
                <td><?php echo $row["product_quantity"];?>-<?php echo $row["product_unit"];?></td>
                <td><?php echo $row["product_base_price"];?></td>
                <td><?php echo ($row["status"])?'Active':'Inactive';?></td>
                <td><a href="<?php echo base_url('management/inventory/addorder/'.$row['id']);?>">Sell</a> <a href="<?php echo base_url('management/inventory/editproduct/'.$row['id']);?>"><i class="fa fa-edit"></i></a></td>
              </tr>
              <?php }}else{?>
              <tr>
                <th scope="row" colspan="6">No Records Found !!</th>
              </tr>
              <?php }?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
var options = {
	startDate: <?php if(isset($start_date) && $start_date !=""){ echo "'".$start_date."'";?><?php }else{?>moment().startOf('hour')<?php }?>,
    endDate: <?php if(isset($end_date) && $end_date !=""){ echo "'".$end_date."'";?><?php }else{?>moment().startOf('hour').add(0, 'hour')<?php }?>
};

     $('#date_range').daterangepicker(options, function(start, end, label) {
		 console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')'); 
		 $("#dt").val(start.format('YYYY-MM-DD')+','+end.format('YYYY-MM-DD'));
		})

$( function() {
    var availableTags = <?php echo $refered_list;?> 
    $( "#tags" ).autocomplete({
      source: availableTags
    });
  } );
  </script>
<?php $this->load->view('management/footer');?>
