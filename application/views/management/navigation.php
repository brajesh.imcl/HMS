<?php $crrentUri	=	($this->uri->segment(2))?$this->uri->segment(2):$this->uri->segment(1);?> 
<div class="navbar-collaps collapse  col-md-3 col-lg-2 p-0" id="sidebar">
	<div class="list-group border-0 card text-md-left">
	  <?php if(in_array('Dashboard',$clinicInfo['access'])){?>
		<a href="
			<?php echo base_url('management/dashboard/appointment');?>" class="list-group-item d-inline-block collapsed <?php echo ($crrentUri =='dashboard')?'active':'';?>" data-parent="#sidebar">
			<i class="fa fa-list menu-icon"></i>
			<span class=" d-md-inline">All Appointments</span>
		</a>
	  <?php } if(in_array('Consultant',$clinicInfo['access'])){ ?>
        
		<a href="
			<?php echo base_url('management/consultant/');?>" class="list-group-item d-inline-block collapsed <?php echo ($crrentUri =='consultant')?'active':'';?>" data-parent="#sidebar">
			<i class="fas fa-user-md menu-icon"></i>
			<span class=" d-md-inline">Consultants Management</span>
		</a>
     <?php } if(in_array('Patient',$clinicInfo['access'])){ ?>
		<a href="
			<?php echo base_url('management/patient/patients');?>" class="list-group-item d-inline-block collapsed <?php echo ($crrentUri =='patient')?'active':'';?>" data-parent="#sidebar">
			<i class="fas fa-users menu-icon"></i>
			<span class=" d-md-inline">Patients Management</span>
		</a>
	 <?php } if(in_array('Staff',$clinicInfo['access'])){ ?>	
		<a href="
			<?php echo base_url('management/staff/');?>" class="list-group-item d-inline-block collapsed <?php echo ($crrentUri =='staff')?'active':'';?>" data-parent="#sidebar">
			<i class="fas fa-users menu-icon"></i>
			<span class=" d-md-inline">Staff Management</span>
		</a>
	 <?php } if(in_array('Treatment',$clinicInfo['access'])){ ?>	
		<a href="
			<?php echo base_url('management/treatment/');?>" class="list-group-item d-inline-block collapsed <?php echo ($crrentUri =='treatment')?'active':'';?>" data-parent="#sidebar">
			<i class="fas fa-medkit menu-icon"></i>
			<span class=" d-md-inline">Equipments Management</span>
		</a>
	<?php } if(in_array('Expense',$clinicInfo['access'])){ ?>
		<a href="
			<?php echo base_url('management/expense/');?>" class="list-group-item d-inline-block collapsed <?php echo ($crrentUri =='expense')?'active':'';?>" data-parent="#sidebar">
			<i class="fas fa-rupee-sign menu-icon"></i>
			<span class=" d-md-inline">Daily Expenses</span>
		</a>
      <?php } if(in_array('Inventory',$clinicInfo['access'])){ ?>   
		<!--<a href="
			<?php echo base_url('management/inventory/');?>" class="list-group-item d-inline-block collapsed" data-parent="#sidebar">
			<i class="fas fa-inr menu-icon" aria-hidden="true"></i>
			<span class=" d-md-inline">Inventory Management</span>
		</a>-->
             <a href="#menu1" class="list-group-item d-inline-block collapsed <?php echo ($crrentUri =='expense')?'inventory':'';?>" data-toggle="collapse" aria-expanded="false">
             	<i class="fas fa-cogs menu-icon"></i> <span class=" d-md-inline">Inventory</span> 
            </a>
            <div class="collapse" id="menu1" data-parent="#sidebar">
            	<a href="<?php echo base_url('management/inventory/');?>" class="list-group-item" >Products List</a>                   
            	  <a href="<?php echo base_url('management/inventory/addcategory/');?>" class="list-group-item" >Add Category </a>                   
                  <a href="<?php echo base_url('management/inventory/addbrand/');?>" class="list-group-item" >Add Brand</a>                   
                  <a href="<?php echo base_url('management/inventory/addproduct/');?>" class="list-group-item" >Add Product </a>                   
            </div> 
	  <?php } ?>			
        		
		<!--<a href="
			<?php echo base_url('management/index/reports');?>" class="list-group-item d-inline-block collapsed" data-parent="#sidebar">
			<i class="fas fa-receipt menu-icon"></i>
			<span class=" d-md-inline">Reports Management</span>
		</a>-->       
               
        
	</div>
</div>

