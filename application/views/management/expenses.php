<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('assets/backend/css/daterangepicker.css');?>">
<script src="<?php echo base_url('assets/backend/js/moment.min.js');?>"></script>
<script src="<?php echo base_url('assets/backend/js/daterangepicker.js');?>" ></script>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('assets/common/css/jquery-ui.css');?>">
<script src="<?php echo base_url('assets/common/js/jquery-ui.js');?>"></script>
<div class="container-fluid">
  <div class="row">
    <?php $this->load->view('management/navigation');?>
    <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
      <div class="bg-white p-3">
        <h3 class="mb-4">Expense's List <a href="<?php echo base_url('management/expense/add');?>" class="float-right" title="Add Expense"><i class="fas fa-plus-square"></i></a></h3>
        <div class="filter">
          <form class="form-inline  needs-validation" novalidate action="" method="get">
            <div class="form-group mb-2">
              <label>Date</label>
              <input type="text" class="form-control" id="date_range" autocomplete="off" />
              <input type="hidden" name="dt" id="dt" value="" />
            </div>
            <div class="form-group mx-sm-2 mb-2">
              <label>Good's Name</label>
              <input type="text" class="form-control" id="goods_name" name="goods_name" placeholder="Search with Good's name"  value="<?php echo $goods_name;?>" autocomplete="off" required>
            </div>
            <button type="submit" name="_btnFilter" value="Filter" class="btn btn-primary mx-sm-2 mb-2">Filter</button>
            <a href="<?php echo base_url('management/expense');?>" class="btn btn-outline-warning mb-2">Clear</a>
          </form>
        </div>
        <div class="table-responsive">
          <table class="table table-fluid table-bordered table-hover">
            <thead  class="thead-light">
              <tr>
                <th scope="col">Sr. No</th>
                <th scope="col">Date</th>
                <th scope="col">Good's Name</th>
                <th scope="col">Remarks</th>
                <th scope="col">Staff</th>
                <th scope="col">Amount</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
  	if(count($allExpense)>0){ 
  	foreach($allExpense as $key => $row){?>
              <tr>
                <th scope="row"><?php echo ++$key;?></th>
                <td><?php echo date("d-m-Y",strtotime($row["created"]));?></td>
                <td><?php echo $row["goods_name"];?></td>
                <td><?php echo $row["remark"];?></td>
                <td><?php echo $all_employee[$row["staff_id"]]["staff_name"];?></td>
                <td>&#8377; <?php echo number_format($row["debit_amt"],2);?></td>
                <td><?php echo ($row["status"])?'Active':'Inactive';?></td>
                <td><a href="<?php echo base_url('management/expense/edit/'.$row['id']);?>"><i class="fa fa-edit"></i></a></td>
              </tr>
              <?php }}else{?>
              <tr>
                <th scope="row" colspan="8">No Records Found !!</th>
              </tr>
              <?php }?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
var options = {
	startDate: <?php if(isset($start_date) && $start_date !=""){ echo "'".$start_date."'";?><?php }else{?>moment().startOf('hour')<?php }?>,
    endDate: <?php if(isset($end_date) && $end_date !=""){ echo "'".$end_date."'";?><?php }else{?>moment().startOf('hour').add(0, 'hour')<?php }?>
};

     $('#date_range').daterangepicker(options, function(start, end, label) {
		 console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')'); 
		 $("#dt").val(start.format('YYYY-MM-DD')+','+end.format('YYYY-MM-DD'));
		})

$( function() {
    var availableTags = <?php echo $refered_list;?> 
    $( "#tags" ).autocomplete({
      source: availableTags
    });
  } );
  </script>
<?php $this->load->view('management/footer');?>
