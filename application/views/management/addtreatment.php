<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>

<div class="container-fluid">
  <div class="row">
    <?php $this->load->view('management/navigation');?>
    <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
      <div class="bg-white p-3">
        <h3 class="mb-4">Add Treatment <a href="<?php echo base_url('management/treatment');?>" class="float-right" title="Back"><i class="fas fa-arrow-left"></i></a></h3>
        <form class="needs-validation" novalidate autocomplete="off" method="post" action="">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Treatment Name</label>
              <input type="text" class="form-control" name="name" id="name"  placeholder="Treatment Name" value="<?php echo $treatmentinfo["name"];?>" required>
            </div>
            <div class="form-group col-md-6">
              <label>Fess</label>
              <input type="text" class="form-control" name="fees" id="fees"  min="1"  placeholder="0.00" value="<?php echo number_format($treatmentinfo["fees"],2);?>" required>
            </div>
          </div>
          <div class="offset-md-5 col-md-3">
            <button type="submit" class="btn btn-primary">Add Treatment</button>
            <a href="<?php echo base_url('management/treatment');?>" class="btn btn-warning">Cancel</a> </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
       // valid fields Validation Form
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();


    </script>
<?php $this->load->view('management/footer');?>
