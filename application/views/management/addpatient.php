<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<div class="container-fluid">
  <div class="row">
     <?php $this->load->view('management/navigation');?>
      <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
        <div class="bg-white p-3">
        <h3 class="mb-4"><?php if($update_patient){?>Update<?php }else{?>Add<?php }?> Patient <a href="<?php echo base_url('management/patient/patients');?>" class="float-right" title="Back"><i class="fas fa-arrow-left"></i></a></h3>

        <form class="needs-validation" novalidate method="post" action="" autocomplete="off" id="frmPatient">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label >Full Name <span class="star">*</span></label>
       <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Full Name" autocomplete="off" required value="<?php echo $full_name;?>">

      <span id="full_name_error" class="error"></span>
    </div>
    <div class="form-group col-md-3">
      <label>Age:<span class="star">*</span></label>
      <input type="number" class="form-control" id="age" name="age"  placeholder="Age" required value="<?php echo $age;?>">

      <span id="age_error" class="error"></span>
    </div>
	<div class="form-group col-md-3">
      <label>Years/Month<span class="star">*</span></label>
      <select id="yrs_mon" name="yrs_mon" class="form-control">        
        <option value="Years" <?php if($yrs_mon=='Years'){?>selected<?php }?>>Years</option>
         <option value="Months" <?php if($yrs_mon=='Months'){?>selected<?php }?>>Months</option>
      </select>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Weight (in kgs)<span class="star">*</span></label>
      <input type="number" class="form-control" id="weight" name="weight" placeholder="Weight" required value="<?php echo $weight;?>">

      <span id="weight_error" class="error"></span>
    </div>
    <div class="form-group col-md-6">
      <label>Address <span class="star">*</span></label>
      <input type="text" class="form-control" id="address" name="address" placeholder="Address" required value="<?php echo $address;?>">

      <span id="address_error" class="error"></span>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Mobile <span class="star">*</span></label>
      <input type="text" class="form-control" id="mobile" name="mobile" maxlength="12" required  value="<?php echo $mobile;?>">

      <span id="mobile_error" class="error"></span>
    </div>
    <div class="form-group col-md-6">
      <label>Sex <span class="star">*</span></label>
      <select id="sex" name="sex" class="form-control">
        <option <?php if(!isset($sex)){?>selected<?php }?> value="">Select</option>
        <option value="Female" <?php if($sex=='Female'){?>selected<?php }?>>Female</option>
         <option value="Male" <?php if($sex=='Male'){?>selected<?php }?>>Male</option>
      </select>
      <span id="sex_error" class="error"></span>
    </div>
  </div>
 
  
  <div class="form-row">
    <div class="form-group col-md-4">
      <label >Refer By</label>
      <input type="text" class="form-control" id="refer_by" name="refer_by" value="<?php echo $refered_by;?>">
    </div>
	<div class="form-group col-md-4">
      <label>Treatment Type</label>
      <select id="treatment_id" name="treatment_id" class="form-control">
	  <?php foreach((array)$treatment as $key=>$row){?>
        <option <?php if($fees == $row['fees']){?>selected<?php }?> value="<?php echo $row['id'];?>" data-fees="<?php echo $row['fees'];?>"><?php echo $row['name'];?></option>        
	  <?php }?>
      </select>
    </div>    
    <div class="form-group col-md-4">
      <label>Fees <span class="star">*</span></label>
      <input type="number" step="0.01" class="form-control" id="fees" name="fees" required value="<?php echo $fees;?>" />
      <span id="fees_error" class="error"></span>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label>Discount</label>
      <input type="text" class="form-control" id="discount" name="discount" value="<?php echo $discount;?>">
    </div>
    <div class="form-group col-md-4">
      <label>Remarks</label>
      <textarea id="remarks" name="remarks" class="form-control" rows="1" <?php if($update_patient){?>readonly<?php }?>><?php echo $remarks;?></textarea>
    </div>
    <div class="form-group col-md-4">
      <label>Due Amount</label>
      <input type="text" class="form-control" id="due_amount" name="due_amount" value="<?php echo $due_amount;?>">
    </div>
  </div>
  <input type="hidden" name="transId" value="<?php echo $transId;?>">  
  
  <div class="text-center">
 
  <input type="hidden" name="_temp_id" value="<?php echo $temp_id;?>">
  <input type="hidden" name="_addpatient" value="addpatient">
  <button type="submit" class="btn btn-primary"> <?php if($update_patient){?>Update Patient<?php }else{ ?>Add Patient<?php }?></button>
  <?php if($update_patient){?>  	
    <a href="<?php echo base_url('management/patient/reportcard/'.$p_id);?>"class="btn btn-info">Print</a>
    <a href="<?php echo base_url('management/patient/patients');?>" class="btn btn-danger">Cancel</a>  	
  <?php }else{?>
  <a href="<?php echo base_url('management/patient/patients');?>" class="btn btn-danger">Cancel</a>
  <a href="javascript:void(0);" onClick="return clear_form();" class="btn btn-warning">Reset</a>
  <?php }?>
</div>
</form>

</div>
      </div>
  </div>
</div>

<script>
function clear_form(){
	document.getElementById("frmPatient").reset();
}
       // valid fields Validation Form
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
	
	
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
		var discount = parseFloat($('#discount').val());
		var fees = parseFloat($('#fees').val());
		var due_amount = parseFloat($('#due_amount').val());
			
		var full_name = $("#full_name").val();
		var age = $("#age").val();
		var weight = $("#weight").val();
		var address = $("#address").val();
		var mobile = $("#mobile").val();
		var sex = $("#sex").val();
		
		
			
        var boolValidate = true;
		if (form.checkValidity() === false) {			
          event.preventDefault();
          event.stopPropagation();
		  boolValidate = false;
        }
		form.classList.add('was-validated');
		//alert(boolValidate+'*****');
		if(full_name==""){
			$("#full_name_error").html("Please enter Full Name.");		
			boolValidate = false;
		}else{ 			
			$("#full_name_error").html("");		
		}
		if(age==""){			
			$("#age_error").html("Please enter Age.");		
			boolValidate = false;
		}else{ 			
			$("#age_error").html("");		
		}
		if(weight==""){ 
			$("#weight_error").html("Please enter Weight.");		
			boolValidate = false;
		}else{ 			
			$("#weight_error").html("");		
		}
		if(address==""){
			$("#address_error").html("Please enter Address.");		
			boolValidate = false;
		}else{ 			
			$("#address_error").html("");		
		}
		if(mobile==""){
			$("#mobile_error").html("Please enter Mobile.");		
			boolValidate = false;
		}else{ 			
			$("#mobile_error").html("");		
		}
		if(sex==""){
			$("#sex_error").html("Please select Sex.");		
			boolValidate = false;
		}else{ 			
			$("#sex_error").html("");		
		}		
		
		if(isNaN(fees) || fees==""){
			$("#fees_error").html("Please enter Fees.")	;		
			boolValidate = false;
		}else if((fees < discount)){
			$("#fees_error").html("Fees should not be less than Discount.")	;		
			boolValidate = false;
		}else if((fees < due_amount)){ 	
			$("#fees_error").html("Fees should not be less than Due Amount.")			
			  boolValidate = false;
		}else{
			$("#fees_error").html("");	
			 
		}  
		
		
		if(boolValidate){
			return true;
		}else{
			event.preventDefault();
          	event.stopPropagation();
			return false;
		}
        
      }, false);
    });
  }, false);

  $('#treatment_id').change(function(){
		var dataFees = $("option:selected", this).attr('data-fees');
		$('#fees').val(dataFees);
	
  });  
  
})();

</script>
<?php $this->load->view('management/footer');?>