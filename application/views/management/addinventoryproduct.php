<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<div class="container-fluid">
  <div class="row">
    <?php $this->load->view('management/navigation');?>
    <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
      <div class="bg-white p-3">
        <h3 class="mb-4">
          <?php if($product_info["product_name"] !=""){?>
          Update
          <?php }else{?>
          Add
          <?php }?>
          Product <a href="<?php echo base_url('management/inventory/productlist');?>" class="float-right" title="Back"><i class="fas fa-arrow-left"></i></a></h3>
        <form class="needs-validation" novalidate autocomplete="off" method="post" action="">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Category <span class="star">*</span></label>
              <select id="cat_id" name="cat_id" class="form-control">
                <option value="">Select</option>
                <?php foreach($all_cat as $key =>$row){?>
                <option value="<?php echo $row["id"];?>" <?php if($row["id"]==$product_info["cat_id"]){?>selected<?php }?>><?php echo $row["category_name"];?></option>
                <?php }?>
              </select>
            </div>
            <div class="form-group col-md-6">
              <label>Brand <span class="star">*</span></label>
              <select id="brand_id" name="brand_id" class="form-control">
                <option value="">Select</option>
                <?php foreach($all_brand as $key =>$row){?>
                <option value="<?php echo $row["id"];?>" <?php if($row["id"]==$product_info["brand_id"]){?>selected<?php }?>><?php echo $row["brand_name"];?></option>
                <?php }?>
              </select>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Product Name <span class="star">*</span></label>
              <input type="text" class="form-control" name="product_name" id="product_name"  placeholder="Product Name" value="<?php echo $product_info["product_name"];?>" required>
            </div>
            <div class="form-group col-md-6">
              <label>Product Unit <span class="star">*</span></label>
              <select id="product_unit" name="product_unit" class="form-control">
                <option value="">Select</option>
                <?php foreach($product_unit as $key =>$value){?>
                <option value="<?php echo $key;?>" <?php if($key==$product_info["product_unit"]){?>selected<?php }?>><?php echo $value;?></option>
                <?php }?>
              </select>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Product Quantity <span class="star">*</span></label>
              <input type="text" class="form-control" name="product_quantity" id="product_quantity"  placeholder="Product Quantity" value="<?php echo $product_info["product_quantity"];?>" required>
            </div>
            <div class="form-group col-md-6">
              <label>Product Price <span class="star">*</span></label>
              <input type="text" class="form-control" name="product_base_price" id="product_base_price"  placeholder="Product Price"  value="<?php echo $product_info["product_base_price"];?>" required />
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Product Description <span class="star">*</span></label>
              <textarea class="form-control" name="product_description" id="product_description"  placeholder="Product Description" required><?php echo $product_info["product_description"];?></textarea>
            </div>
            <div class="form-group col-md-6">
              <label>Product Tax(%)</label>
              <input type="text" class="form-control" name="product_tax" id="product_tax"  placeholder="Product Tax" value="<?php echo $product_info["product_tax"];?>" >
            </div>
          </div>
          <div class="offset-md-5 col-md-3">
            <button type="submit" class="btn btn-primary">
            <?php if($brand_info["brand_name"] !=""){?>
            Update
            <?php }else{?>
            Add
            <?php }?>
            </button>
            <a href="<?php echo base_url('management/inventory/productlist');?>" class="btn btn-warning">Cancel</a> </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
       // valid fields Validation Form
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();


    </script>
<?php $this->load->view('management/footer');?>