<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('assets/backend/css/daterangepicker.css');?>">
<script src="<?php echo base_url('assets/backend/js/moment.min.js');?>"></script>
<script src="<?php echo base_url('assets/backend/js/daterangepicker.js');?>" ></script>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('assets/common/css/jquery-ui.css');?>">
<script src="<?php echo base_url('assets/common/js/jquery-ui.js');?>"></script>
<div class="container-fluid">
  <div class="row">
    <?php $this->load->view('management/navigation');?>
    <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
      <div class="bg-white p-3">
        <h3 class="mb-4">Order List <a href="<?php echo base_url('management/inventory/productlist');?>" class="float-right" title="Product List"><i class="fas fa-plus-square"></i></a></h3>
        <div class="filter">
          <form class="form-inline  needs-validation" novalidate action="" method="get">
            <div class="form-group mb-2">
              <label>Date</label>
              <input type="text" class="form-control" id="date_range" autocomplete="off" />
              <input type="hidden" name="dt" id="dt" value="" />
            </div>
            <div class="form-group mx-sm-2 mb-2">
              <label>Search</label>
              <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Product's name"  value="<?php echo $product_name;?>" autocomplete="off" required>
            </div>
            <button type="submit" name="_btnFilter" value="Filter" class="btn btn-primary mx-sm-2 mb-2">Filter</button>
            <a href="<?php echo base_url('management/inventory/orderlist');?>" class="btn btn-outline-warning mb-2">Clear</a>
          </form>
        </div>
        <div class="table-responsive">
          <table class="table table-fluid table-bordered table-hover">
            <thead  class="thead-light">
              <tr>
                <th scope="col">Sr. No</th>
                <th scope="col">Date</th>
                <th scope="col">Product Name</th>
                <th scope="col">Product Quantity</th>
                <th scope="col">Product Base Price</th>
                <th scope="col">Total Price</th>
                <th scope="col">Tax(%)</th>
                <th scope="col">Buyer's Name</th>
                <th scope="col">Buyer's Address</th>
                <th scope="col">Payment Mode</th>
                <th scope="col">Sold By</th>
              </tr>
            </thead>
            <tbody>
              <?php 
  	if(count($all_order)>0){ 
  	foreach($all_order as $key => $row){?>
              <tr>
                <th scope="row"><?php echo ++$key;?></th>
                <td><?php echo date("d-m-Y h:i A",strtotime($row["created"]));?></td>
                <td><?php echo $row["product_name"];?></td>
                <td><?php echo $row["quantity"];?></td>
                <td><?php echo $row["price"];?></td>
                <td><?php echo number_format($row["quantity"]*$row["price"],2);?></td>
                <td><?php echo $row["tax"];?></td>
                <td><?php echo $row["inventory_order_name"];?></td>
                <td><?php echo $row["inventory_order_address"];?></td>
                <td><?php echo $row["payment_status"];?></td>
                <td>&nbsp;<?php echo $all_employee[$row["staff_id"]]["staff_name"];?></td>
              </tr>
              <?php }}else{?>
              <tr>
                <th scope="row" colspan="11">No Records Found !!</th>
              </tr>
              <?php }?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
var options = {
	startDate: <?php if(isset($start_date) && $start_date !=""){ echo "'".$start_date."'";?><?php }else{?>moment().startOf('hour')<?php }?>,
    endDate: <?php if(isset($end_date) && $end_date !=""){ echo "'".$end_date."'";?><?php }else{?>moment().startOf('hour').add(0, 'hour')<?php }?>
};

     $('#date_range').daterangepicker(options, function(start, end, label) {
		 console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')'); 
		 $("#dt").val(start.format('YYYY-MM-DD')+','+end.format('YYYY-MM-DD'));
		})

$( function() {
    var availableTags = <?php echo $refered_list;?> 
    $( "#tags" ).autocomplete({
      source: availableTags
    });
  } );
  </script>
<?php $this->load->view('management/footer');?>
