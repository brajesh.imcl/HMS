<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Sneh New Born & Child Care Centre</title>
<?php $this->load->view('management/all_js_css');?>

<style>

.form-prescription {	height: 82.4%}

.footer-prescription .copy span {	color: red}

 @media print {

.form-prescription {height: 82.4%}

}

</style>
<title>Prescription</title>
</head>
<body>
<div class="container-prescription">
  <div class="header-border px-0"></div>
  <div class="container-fluid my-1">
    <div class="row">
      <div class="col-4">
          <h3 class="font-dr">डॉ. अजय कुमार </h3>
          <p class="clr">स्नेह न्यू  बॉर्न एवं  चाइल्ड  केयर  सेंटर </p>
          <p class="clr">Pediatrics Degree</p>       
        </div>
     
      <div class="col-4  px-0" align="center">
        <h3 class="font-dr mb-0">DR. Ajay Kumar</h3>
        <p class="md">Sneh New Born & Child Care Centre</p>
        <p class="clr">Reg No- L/16446</p>
        <p class="md">Red Cross Road Adampur</p>
        <p class="clr">कचहरी चौक भागलपुर</p>
      </div>
       
      <div class="col-4 text-right pre-image">
        <p class="clr"><i class="fas fa-mobile-alt"></i> 0641-2408677 <br>
          9304414144</p>
        <p class="clr"><i class="fas fa-globe-asia"></i> www.xyz.com </p>
        <img src="<?php echo base_url('assets/common/images/doctor-child.png');?>" class="img-fluid"> <img src="<?php echo base_url('assets/common/images/baby.png');?>" class="img-fluid"> </div>
    </div>
  </div>
  <div class="heart"><img src="<?php echo base_url('assets/common/images/heart.png');?>" class="img-fluid"></div>
  <div class="print" style="position: fixed;left: 145px;top: 145px;"><a href="javascript:void(0);" onClick="return print_page(this)"><img src="<?php echo base_url('assets/backend/images/Print.png');?>"></a></div>
  <div class="back" style="position: fixed;left: 153px;top: 220px;"><a href="javascript:void(0);" onclick="window.history.go(-1); return false;" ><img src="<?php echo base_url('assets/backend/images/back.png');?>"></a></div>
  <div class="form-prescription ajay-prescription">
    <div class=" p-1">
      <form class="form-inline">
        <div class="form-row">
          <div class="form-group col-md-5 col-5 mb-2">
            <label >Ref BY :</label>
            <input class="form-border" value="<?php echo ucwords($patient_data["refered_by"]);?>" readonly>
          </div>
          <div class="form-group col-md-2 col-2 mb-2">
            <label >Date :</label>
            <input class="form-border" value="<?php echo date("d-m-Y",strtotime($patient_data["created"]))?>" / readonly>
          </div>
          <div class="form-group col-md-2 col-2 mb-2">
            <label>Age:</label>
            <input class="form-border" value="<?php echo $patient_data["age"];?> Yrs" readonly>
          </div>
          <div class="form-group col-md-3 col-3 mb-2">
            <label>Weight :</label>
            <input class="form-border" value="<?php echo $patient_data["weight"];?> Kg" readonly>
          </div>
          <div class="form-group col-md-5 col-5 mb-2">
            <label >Name :</label>
            <input class="form-border" value="<?php echo ucwords($patient_data["full_name"]);?>" readonly>
          </div>
          <div class="form-group col-md-5 col-5 mb-2">
            <label>Address :</label>
            <input class="form-border" value="<?php echo $patient_data["address"];?>" readonly>
          </div>
          <div class="form-group col-md-2 col-2 mb-2">
            <label> SEX :</label>
            <input class="form-border" value="<?php echo $patient_data["gender"];?>" readonly>
          </div>
        </div>
      </form>
    </div>
  </div>
  <footer class="footer-prescription">
    <div class="row copy">
      <div class="col-12">Opinion expressed in my personal skill and capacity and no indemnity lies with me on account of brkoen therapy/ altered exercise regimen (whatever though sd s home programmes), irregular follow up or interfernce by another clinician/physiotherapist n forms of any act or opinion .
        Risk about the exercise, electro therapy modalities and manipulation therapy techniques has been explanide in his/her languges. </div>
      <div class="col-12" align="center"><span>NOT VALID FOR MEDICO LEGAL PURPOSES</span></div>
    </div>
  </footer>
</div>

<!-- Optional JavaScript --> 
<script>
	function print_page(ele){		
		ele.style='display:none';
		window.print();
	}
</script>
</body>
</html>