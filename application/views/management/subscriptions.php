<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <?php $this->load->view('management/all_js_css');?>
     
    <title>Subscription</title>
    <style>
    .container-subscription{width:1170px; margin: 0 auto;}
    .bg-gradient-primary {    background-color: #007bff!important;
}
    </style>
  </head>
  <body>
   
<section>
  <h3 class="text-center text-white bg-gradient-primary py-4">Hosting Plans To Meet Every Need</h3>
  <div class="container-subscription">    
    <h4 class="py-2"> Admin Panel / Manegment Panel</h4>
      <div class="card-deck mb-3 text-center">
         <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Free</h4>
          </div>
          <div class="card-body">
            <h3 class="card-title pricing-card-title">₹ 0</small></h3>
            <ul class="list-unstyled mt-3 mb-4">
               <li><strong>Responsive :</strong>  Mobile Design</li>
              <li><strong>Website :</strong> Hosting, Domain</li>
              <li><strong>support :</strong> 24/7 </li>
              <li><strong>Security  :</strong> (SSL)</li>
              <li><strong>Manage  :</strong> orders and inventory</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">15 Days Trail</button>
          </div>
        </div>
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">3 Month</h4>
          </div>
          <div class="card-body">
            <h3 class="card-title pricing-card-title">₹ 4000<small class="text-muted">/ 3 Month</small></h3>
            <ul class="list-unstyled mt-3 mb-4">
              <li><strong>Responsive :</strong>  Mobile Design</li>
              <li><strong>Website :</strong> Hosting, Domain</li>
              <li><strong>support :</strong> 24/7 </li>
              <li><strong>Security  :</strong> (SSL)</li>
              <li><strong>Manage  :</strong> orders and inventory</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Buy Now</button>
          </div>
        </div>
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">6 Month</h4>
          </div>
          <div class="card-body">
            <h3 class="card-title pricing-card-title">₹ 7500 <small class="text-muted">/ 6 Month</small></h3>
            <ul class="list-unstyled mt-3 mb-4">
              <li><strong>Responsive :</strong>  Mobile Design</li>
              <li><strong>Website :</strong> Hosting, Domain</li>
              <li><strong>support :</strong> 24/7 </li>
              <li><strong>Security  :</strong> (SSL)</li>
              <li><strong>Manage  :</strong> orders and inventory</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Buy Now</button>
          </div>
        </div>
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">1 Years</h4>
          </div>
          <div class="card-body">
            <h3 class="card-title pricing-card-title">₹ 14500 <small class="text-muted">/ 1 Years</small></h3>
            <ul class="list-unstyled mt-3 mb-4">
              <li><strong>Responsive :</strong>  Mobile Design</li>
              <li><strong>Website :</strong> Hosting, Domain</li>
              <li><strong>support :</strong> 24/7 </li>
              <li><strong>Security  :</strong> (SSL)</li>
              <li><strong>Manage  :</strong> orders and inventory</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Buy Now</button>
          </div>
        </div>
      </div>
      <h4 class="py-2"> Frontend Design</h4>
      
         <div class="card mb-4 box-shadow text-center" style="max-width: 22rem;"">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Fronted Design</h4>
          </div>
          <div class="card-body">
            <h3 class="card-title pricing-card-title">₹ 6000 <small class="text-muted">/ 1 Year</small></h3>
            <ul class="list-unstyled mt-3 mb-4">
              <li>4-5 Pages</li>
              <li><strong>Minor Changes :</strong> Free 3 Months</li>
              <li><strong>Major Changes :</strong> Cost As Perchanges </li>
              <li><strong>Responsive :</strong>  Mobile Design</li>
              <li><strong>Website :</strong> Hosting, Domain</li>
              <li><strong>support :</strong> 24/7 </li>
              <li><strong>Security  :</strong> (SSL)</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Buy Now</button>
          </div>
        </div>
    
  </div>
</section>

  
  </body>
</html>