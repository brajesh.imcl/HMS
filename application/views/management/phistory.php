<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('management/header');?>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('assets/backend/css/daterangepicker.css');?>">
<script src="<?php echo base_url('assets/backend/js/moment.min.js');?>"></script>
<script src="<?php echo base_url('assets/backend/js/daterangepicker.js');?>" ></script>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('assets/common/css/jquery-ui.css');?>">
<script src="<?php echo base_url('assets/common/js/jquery-ui.js');?>"></script>
<div class="container-fluid">
  <div class="row">
    <?php $this->load->view('management/navigation');?>
    <div class="col-12 col-sm-12 col-md-9 col-lg-10  mt-3">
      <div class="bg-white p-3">
        <h3 class="mb-4">Patient's History <a href="<?php echo base_url('management/patient/patients/');?>" class="float-right" title="Back"><i class="fas fa-arrow-left"></i></a></h3>
        <div class="filter"> <a href="<?php echo base_url('management/patient/download/'.$p_id);?>" class="btn btn-outline-success mb-2 float-right">Export Excel</a>
          <div class="table-responsive">
            <table class="table table-fluid table-bordered table-hover">
              <thead  class="thead-light">
                <tr >
                  <th scope="col">Name</th>
                  <th scope="col">Sex</th>
                  <th scope="col">Age</th>
                  <th scope="col">Weight</th>
                  <th scope="col" class="text-center">Address</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td ><?php echo ucwords($full_name);?></td>
                  <td ><?php echo $sex;?></td>
                  <td><?php echo $age.' '.$yrs_mon;?></td>
                  <td><?php echo $weight;?> Kg</td>
                  <td><?php echo $address;?></td>
                </tr>
              </tbody>
            </table>
          </div>       
        </div>
        <div class="table-responsive">
          <table class="table table-fluid table-bordered table-hover">
            <thead  class="thead-light">
              <tr>
                <th scope="col">S.No</th>
                <th scope="col">Date</th>
                <th scope="col">Treatment</th>
                <th scope="col">Fees</th>
                <th scope="col">Discount</th>
                <th scope="col">Remarks</th>
                <th scope="col">Refered By</th>
                <th scope="col">&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              <?php 
  	if($patient_transaction_data && (count((array)$patient_transaction_data)>0)){
			
  	foreach((array)$patient_transaction_data as $patient_key => $transaction_row){?>
              <tr>
                <td><?php echo $patient_key+1;?></th>
                <td><?php echo date("d-m-Y",strtotime($transaction_row["created"]));?></th>
                <td><?php echo $treatment[$transaction_row["treatment_id"]];?></td>
                <td><?php echo number_format($transaction_row["fees"],2);?></td>
                <td><?php echo number_format($transaction_row["discount"],2);?></td>
                <td><?php echo ($transaction_row["remarks"] !="")?$transaction_row["remarks"]:" ---";?></td>
                <td><?php echo ($transaction_row["refered_by"] !="")?ucwords($transaction_row["refered_by"]):"---";?></td>
				<td><input type="checkbox" class="trmtId" value="<?php echo $transaction_row['treatment_id'];?>" /></td>
              </tr>
              <?php }?>
			  <tr><td colspan="8"><a href="javascript:void(0);" class="float-right print_receipt" title="Back"><i class="fas fa-print"></i> Print Receipt</a></td></tr>
			  <?php }else{?>
              <tr>
                <th scope="row" colspan="7">Sorry !! No Records Found.</th>
              </tr>
              <?php }?>
            </tbody>
          </table>
        </div>
        <p><?php echo $links; ?></p>
      </div>
    </div>
  </div>
</div>
<script>
$(function(){
	$('.print_receipt').click(function(e){
		e.stopPropagation();
		if($('input[class="trmtId"]:checked').length){
			let trmtIds = $('input[class="trmtId"]:checked').map(function() {return this.value;}).get().join(',');
			window.location.replace('<?php echo base_url('management/patient/receipt/?p_id='.$p_id);?>&ids='+trmtIds);
		}else{
			alert('Please select atleast one treatment.');
			return false;	
		}
		
		
	});
	
});


var options = {
	startDate: <?php if(isset($start_date) && $start_date !=""){ echo "'".$start_date."'";?><?php }else{?>moment().startOf('hour')<?php }?>,
    endDate: <?php if(isset($end_date) && $end_date !=""){ echo "'".$end_date."'";?><?php }else{?>moment().startOf('hour').add(0, 'hour')<?php }?>
};

     $('#date_range').daterangepicker(options, function(start, end, label) {
		 console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')'); 
		 $("#dt").val(start.format('YYYY-MM-DD')+','+end.format('YYYY-MM-DD'));
		})

$( function() {
    var availableTags = <?php echo $refered_list;?> 
    $( "#tags" ).autocomplete({
      source: availableTags
    });
  } );
  </script>
<?php $this->load->view('management/footer');?>
