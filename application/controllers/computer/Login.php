<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent:: __construct();
		
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->load->model('computer_model');
	}
	
	public function index(){
		$this->alreadyLogin();	
		if($this->form_validation->run('user_login') == FALSE){
			$this->load->view('computer/login');
		}else{
			$postData = $this->input->post();
            $args = array('username'=>$postData['username'],'password'=>md5($postData['password']));	
			$responseData = $this->computer_model->AuthenticateUser($args);
			#print_r($responseData);
			if(count($responseData) > 0){			
				$this->session->set_userdata('computer_login', $responseData);
				redirect(base_url('computer/dashboard/'));
			}else{
				$this->session->set_flashdata('flash_msg', 'Your username OR password is Wrong!');
				$this->load->view('computer/login');
			}				
			
		}
	}
	
	
	public function allControllers(){
			$this->load->helper('file');
			$controllers = get_filenames( APPPATH . 'controllers/' ); 

			foreach( $controllers as $k => $v ){
				if( strpos( $v, '.php' ) === FALSE)
				{
					unset( $controllers[$k] );
				}
			}

			echo '<ul>';

			foreach( $controllers as $controller ){
				echo '<li>' . $controller . '</li>';				
			}

			echo '</ul>'; 
		
	}
	
	public function alreadyLogin(){
		if($this->session->has_userdata('computer_login')){
			$this->adminInfo = 	$this->session->userdata('computer_login');
			redirect(base_url('computer/dashboard/'));			
		}
	}	
	
}