<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	private $cmpInfo, $data = array();
	
	function __construct(){
		parent:: __construct();		
		if($this->session->has_userdata('computer_login')){
			$this->data['cmpInfo'] = $this->session->userdata('clinic_login');
		}else{
			redirect(base_url('computer/login/'));
		}
		$this->load->model('computer_model');
		$this->load->library('form_validation');
	}
	
	public function index(){	
		
		$varWhere	=	array('status <>'=>2);
		$this->data['allClinics'] = $this->computer_model->getClinics($varWhere);
		$this->load->view('computer/clinics', $this->data);
	}
	
	public function addclinic(){		
		
		$this->data['controls'] = $this->allControllers();
		
		$this->form_validation->set_rules('user_id', 'user_id', 'callback_check_clinic', array('check_clinic'=>'This username already exist!'));
		if($this->form_validation->run() == FALSE){
			$this->load->view('computer/addclinic',$this->data);
		}else{			
			$postData = $this->input->post();
			$postData['password'] = md5($postData['password']);
			$postData['access'] = json_encode($postData['access']);
			if(!empty($_FILES['clinic_photo']['name'])){
				$uploadData = array('config'=>array(
												'upload_path'=>'./assets/uploads/clinic/',
												'allowed_types'=>'gif|jpg|jpeg|png',
												'min_width'	=> '200',
												'min_height' => '200'),
									 			'filename'=>'clinic_photo'			
									);
				$uploadData = uploadfiles($uploadData);							
				if(isset($uploadData['upload_data'])){
					$postData['clinic_photo'] = $uploadData['upload_data']['file_name'];
					$this->computer_model->saveClinic($postData);
					$this->session->set_flashdata('message','You have successfully added a record.');
					redirect(base_url('computer/dashboard/'));
				}
				$this->data['uploadData'] = $uploadData;
				$this->load->view('computer/addclinic',$this->data);
			}else{	
				$response = $this->computer_model->saveClinic($postData);
				if($response){
					$this->session->set_flashdata('message','You have successfully added a record.');	
					redirect(base_url('computer/dashboard/'));
				}
			}
		}		
		
	}
	
	public function edit($editId){		
		
		if($this->input->post() && $editId){			
			$postData = $this->input->post();
			$postData['access'] = json_encode($postData['access']);
			if($postData['password'] != $postData['oldpassword']){
				$postData['password'] = md5($postData['password']);
			}else{
				unset($postData['password']);
			}
			unset($postData['oldpassword']);
			#echo "<pre>";print_r($postData);die;	
			$varWhere	=	array('id'=>$editId);
			if(!empty($_FILES['clinic_photo']['name'])){
				$uploadData = array('config'=>array(
												'upload_path'=>'./assets/uploads/clinic/',
												'allowed_types'=>'gif|jpg|jpeg|png',
												'min_width'	=> '200',
												'min_height' => '200'),
									 			'filename'=>'clinic_photo'			
									);
				$uploadData = uploadfiles($uploadData);							
				if(isset($uploadData['upload_data'])){
					$postData['clinic_photo'] = $uploadData['upload_data']['file_name'];
					$this->computer_model->updateClinic($postData, $varWhere);
					$this->session->set_flashdata('message','You have successfully updated a record.');
					redirect(base_url('computer/dashboard/'));
				}
				$this->data['uploadData'] = $uploadData;				
			}else{
				$response = $this->computer_model->updateClinic($postData, $varWhere);
				if($response){
					$this->session->set_flashdata('message','You have successfully updated a record.');	
					redirect(base_url('computer/dashboard/'));
				}
			}
		}
		$varWhere  =	array('id'=>$editId);
		$this->data['clinicinfo']	=	$this->computer_model->getClinicRowBy($varWhere);	
		$this->data['controls'] = $this->allControllers();
		$this->load->view('computer/addclinic', $this->data);
	}
	
	public function check_clinic($field_value){		
		$clinicRow	=	$this->computer_model->getClinicRowBy(array('user_id'=>$field_value));
        if($clinicRow > 0){            
            return FALSE;
        }else{
            return TRUE;
        }
    }
	
	public function delete($delId){
		if($delId){
			$varWhere	=	array('id'=>$delId);
			$this->computer_model->updateClinic(array('status'=>2), $varWhere);
			redirect(base_url('computer/dashboard/'));
		}
		
	}

	public function allControllers(){
			$this->load->helper('file');
			$controllers = get_filenames( APPPATH . 'controllers/' ); 

			foreach( $controllers as $k => $v ){
				if( strpos( $v, '.php' ) === FALSE)
				{
					unset( $controllers[$k] );
				}
			}

			return $controllers;
		
	}	

	
	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url('computer/login/'));		
	}	
	
}
