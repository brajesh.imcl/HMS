<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment extends CI_Controller {

	function __construct(){
		parent:: __construct();			
		$this->load->model('appointment_model');
	}
	
	public function book(){		
		
		$postData = $this->input->post();
		if($postData){			
			$response = $this->appointment_model->bookAppointment($postData);
			if($response){
				echo json_encode(array('status'=>1,'message'=>'Your appointment has been booked successfully.'));
			}else{
				echo json_encode(array('status'=>0,'message'=>'Something went wrong, Try again!'));
			}
			
		}		
			
		die;
	}
	
	
	
}