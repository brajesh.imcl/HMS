<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	private $doctor_arr = array('rajiv'=>'Dr Rajiv Sinha','ajay'=>'Dr Ajay Kumar');
	function __construct(){
		parent:: __construct();	
		$this->load->helper('hms_helper');
	}
	public function index(){		
		$this->load->view('home/welcome');
	}
	private function get_personalize_data($dr_slug){
		$this->data['doctor'] = 'Dr. '.ucwords($dr_slug);
		$this->data['top_nav'] = top_nav($dr_slug);
		$this->data['home_logo'] = home_logo($dr_slug);
		$this->data['clinic_details'] = clinic_details($dr_slug);
		$this->data['home_slider'] = home_slider($dr_slug);
		$this->data['doctor_details'] = doctor_details($dr_slug);
		$this->data['clinic_services'] = clinic_services($dr_slug);
		$this->data['clinic_location_map'] = clinic_location_map($dr_slug);
	}
	public function profile($dr_slug = 'rajiv'){
		#echo '<pre>';print_r(home_slider($dr_slug));
		if((!array_key_exists($dr_slug,$this->doctor_arr))){
			redirect('home/index'); 
		}
		$this->get_personalize_data($dr_slug);
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}
		$this->load->view('home/index',$this->data);
	}
	public function gallery($dr_slug = 'rajiv'){
		$this->get_personalize_data($dr_slug);
		$this->load->helper('gallery_helper');
		$this->data['gallery'] = home_gallery($dr_slug);
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}
		$this->load->view('home/gallery',$this->data);
	}
	public function contact_us($dr_slug = 'rajiv'){
		$this->get_personalize_data($dr_slug);
		$this->load->view('home/contact',$this->data);
	}
	public function appointment($dr_slug = 'rajiv'){
		$this->get_personalize_data($dr_slug);
		$this->load->view('home/appointment',$this->data);
	}
}
