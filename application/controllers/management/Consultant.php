<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consultant extends CI_Controller {
	
	private $clinicInfo, $data = array();
	
	function __construct(){
		parent:: __construct();		
		if($this->session->has_userdata('clinic_login')){
			$this->data['clinicInfo'] = $this->clinicInfo  = $this->session->userdata('clinic_login');
		}else{
			redirect('management/login/');
		}
		$this->load->model('consultant_model');
	}
	
	public function index(){
		$varWhere	=	array('clinic_id'=>$this->clinicInfo['id'], 'status <>'=>2);
		$this->data['allconsultant'] = $this->consultant_model->getConsultant($varWhere);					
		$this->load->view('management/consultant', $this->data);
	}
	
	public function add(){		
		
		if($this->input->post()){			
			$postData = $this->input->post();
			$postData['clinic_id']	=	$this->clinicInfo['id'];
			#print_r($postData);die;
			$response = $this->consultant_model->saveConsultant($postData);
			if($response){
				$this->session->set_flashdata('message','A Consultant has been added successfully.');				
				redirect(base_url('management/consultant/'));
			}
		}	
		
		$this->load->view('management/addconsultant', $this->data);
	}
	
	public function edit($editId){		
		
		if($this->input->post() && $editId){			
			$postData = $this->input->post();
			$varWhere	=	array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']);
			$response = $this->consultant_model->updateConsultant($postData, $varWhere);
            if($response){
				$this->session->set_flashdata('message','A Consultant has been updated successfully.');	
				redirect(base_url('management/consultant/'));
			}
		}
		$varWhere  =	array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']);
		$this->data['staffinfo']	=	$this->consultant_model->getConsultantRowBy($varWhere);	
		#echo "<pre>";print_r($this->data);die;
		$this->load->view('management/addconsultant', $this->data);
	}
	
	public function delete($delId){
		if($delId){
			$varWhere	=	array('id'=>$delId, 'clinic_id'=>$this->clinicInfo['id']);
			$this->consultant_model->deleteConsultant($varWhere);
			redirect(base_url('management/consultant/'));
		}
		
	}
	
	
	
}
