<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent:: __construct();
		
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->load->model('login_model');
	}
	
	public function index(){
		$this->alreadyLogin();	
		if($this->form_validation->run('user_login') == FALSE){
			$this->load->view('management/login');
		}else{
			$postData = $this->input->post();
            $args = array('user_id'=>$postData['username'],'password'=>md5($postData['password']));	
			$responseData = $this->login_model->AuthenticateUser($args);
			#print_r($responseData);
			if(count($responseData)){				
				if($responseData['status']==2){
					$this->session->set_flashdata('flash_msg', 'Your account has been deleted!');
					$this->load->view('management/login');
				}else if($responseData['status']==0){
					$this->session->set_flashdata('flash_msg', 'Your account has not activated!');
					$this->load->view('management/login');
				}else{
					$responseData['access']	= json_decode($responseData['access']);					
					$this->session->set_userdata('clinic_login', $responseData);
					redirect(base_url('management/dashboard/'));
				}

			}else{
				$this->session->set_flashdata('flash_msg', 'Your username OR password is Wrong!');
				$this->load->view('management/login');
			}				
			
		}
	}
	
	public function allControllers(){
			$this->load->helper('file');
			$controllers = get_filenames( APPPATH . 'controllers/' ); 

			foreach( $controllers as $k => $v ){
				if( strpos( $v, '.php' ) === FALSE)
				{
					unset( $controllers[$k] );
				}
			}

			echo '<ul>';

			foreach( $controllers as $controller ){
				echo '<li>' . $controller . '</li>';				
			}

			echo '</ul>'; 
		
	}
	
	public function alreadyLogin(){
		if($this->session->has_userdata('clinic_login')){
			$this->adminInfo = 	$this->session->userdata('clinic_login');
			redirect(base_url('management/dashboard/'));			
		}
	}
   public function subscriptions(){

	$this->load->view('management/subscriptions');
   }   
	
}