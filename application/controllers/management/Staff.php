<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {
	
	private $clinicInfo, $data = array();
	
	function __construct(){
		parent:: __construct();		
		if($this->session->has_userdata('clinic_login')){
			$this->data['clinicInfo'] = $this->clinicInfo  = $this->session->userdata('clinic_login');
		}else{
			redirect('management/login/');
		}
		$this->load->model('staff_model');
	}
	
	public function index(){
		$varWhere	=	array('status <>'=>2,'clinic_id'=>$this->clinicInfo['id']);
		$this->data['allStaff'] = $this->staff_model->getStaffs($varWhere);					
		$this->load->view('management/staffs', $this->data);
	}
	
	public function add(){		
		
		if($this->input->post()){			
			$postData = $this->input->post();
			$postData['clinic_id']	=	$this->clinicInfo['id'];
			$response = $this->staff_model->saveStaff($postData);
			if($response){		
				redirect(base_url('management/staff/'));
			}
		}	
		
		$this->load->view('management/addstaff', $this->data);
	}
	
	public function edit($editId){		
		
		if($this->input->post() && $editId){			
			$postData = $this->input->post();
			$varWhere	=	array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']);
			$response = $this->staff_model->updateStaff($postData, $varWhere);
            if($response){			
				redirect(base_url('management/staff/'));
			}
		}
		$varWhere  =	array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']);
		$this->data['staffinfo']	=	$this->staff_model->getStaffRowBy($varWhere);	
		#echo "<pre>";print_r($this->data);die;
		$this->load->view('management/addstaff', $this->data);
	}
	
	public function delete($delId){
		if($delId){
			$varWhere	=	array('id'=>$delId, 'clinic_id'=>$this->clinicInfo['id']);
			$this->staff_model->deletestaff($varWhere);
			redirect(base_url('management/staff/'));
		}
		
	}
	
	
	
}
