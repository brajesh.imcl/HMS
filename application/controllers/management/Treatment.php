<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Treatment extends CI_Controller {
	
	private $clinicInfo, $data = array();
	
	function __construct(){
		parent:: __construct();	
		
		if($this->session->has_userdata('clinic_login')){
			$this->data['clinicInfo'] = $this->clinicInfo  = $this->session->userdata('clinic_login');
		}else{
			redirect('management/login/');
		}
		$this->load->model('treatment_model');		
	}
	public function index(){
		$varWhere	=	array('status <>'=>2,'clinic_id'=>$this->clinicInfo['id']);
		$this->data['allTreatment'] = $this->treatment_model->getTreatments($varWhere);	
		#secho '<pre>'; print_r($this->data);
		$this->load->view('management/treatments', $this->data);
	}
	
	public function add(){		
		if($this->input->post()){			
			$postData = $this->input->post();
			$postData['clinic_id']	=	$this->clinicInfo['id'];
			$postData['created'] = date("Y-m-d");
			$response = $this->treatment_model->saveTreatment($postData);
			if($response){		
				redirect(base_url('management/treatment'));
			}
		}	
		$this->load->view('management/addtreatment', $this->data);
	}
	public function edit($editId){		
		
		if($this->input->post() && $editId){			
			$postData = $this->input->post();
			$varWhere	=	array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']);
			$response = $this->treatment_model->updateTreatment($postData, $varWhere);
            if($response){			
				redirect(base_url('management/treatment/'));
			}
		}
		$varWhere  =	array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']);
		$this->data['treatmentinfo']	=	$this->treatment_model->getTreatmentRowBy($varWhere);	
		#echo "<pre>";print_r($this->data);die;
		$this->load->view('management/addtreatment', $this->data);
	}
		
	public function delete($delId){
		if($delId){
			$varWhere	=	array('id'=>$delId, 'clinic_id'=>$this->clinicInfo['id']);
			$this->treatment_model->deleteTreatment($varWhere);
			redirect(base_url('management/treatment/'));
		}
		
	}
	
}
