<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient extends CI_Controller {
	
	private $clinicInfo, $data = array();
	
	function __construct(){
		parent:: __construct();	
		
		if($this->session->has_userdata('clinic_login')){
			$this->data['clinicInfo'] = $this->clinicInfo  = $this->session->userdata('clinic_login');
		}else{
			redirect(base_url('management/login/'));
		}
		$this->load->model('patient_model');
		$this->load->library("pagination");
	}
	
	public function appointment(){
		$this->data['clinicInfo']  = $this->clinicInfo;
		$this->load->view('management/appointment', $this->data);
	}
	public function download($editId){			
		$this->patient_model->getExcelData(array('patients.id'=>(int)$editId,'patients.clinic_id'=>$this->clinicInfo['id']));
	}
	
	public function patients($args = array()){		
		if($this->input->get('dt', TRUE) !=""){
			$date_range = $this->input->get('dt', TRUE);
			$date_arr = explode(",",$date_range);
			$start_date = $date_arr[0];
			$end_date = $date_arr[1];
			$this->data['start_date'] =	date("m/d/Y",strtotime($start_date));	
			$this->data['end_date'] =	date("m/d/Y",strtotime($end_date));	
		}
		if($this->input->get('p_id', TRUE) !=""){
			$p_id = str_replace('PID-','',$this->input->get('p_id', TRUE));
			$this->data['p_id']  = $p_id; 
		}
		if($this->input->get('tags', TRUE) !=""){
			$refered_by = $this->input->get('tags', TRUE);
			$this->data['refered_by']  = $refered_by; 
		}
		if($this->input->get('patient_name', TRUE) !=""){
			$patient_name = $this->input->get('patient_name', TRUE);
			$this->data['patient_name']  = $patient_name; 
		}
		
		      
		$limit_per_page = 10;		
		$page =  ($this->input->get('per_page'))?($this->input->get('per_page') - 1) * $limit_per_page:0;	   
		
				
		$patients_data = $this->patient_model->getPatients(array('limit'=>$limit_per_page,'offset'=>$page,'date_range'=>$date_range,'refered_by'=>$refered_by,'patient_name'=>$patient_name,'p_id'=>$p_id,'clinic_id'=>$this->clinicInfo['id']));		
		
		//=== pagination goes here	----------------------------------------- 
				
			$args = paginationConfig(array('baseurl'=>base_url('management/patient/patients'), 'total_records'=>$patients_data["total"], 'limit_per_page'=>5)); 
			$this->pagination->initialize($args);
			$this->data["links"] = $this->pagination->create_links();
		//=== pagination ends here
		
		$refered_list = $this->patient_model->getReferedList(array('clinic_id'=>$this->clinicInfo['id']));
		$refered_list_str = "";
		foreach((array)$refered_list as $key => $row){
			$refered_list_str .= '"'.$row["refered_by"].'"'.",";
		}
		$refered_list_str = substr($refered_list_str,0,-1);
		
		$this->data['refered_list'] =	"[".$refered_list_str."]";	
		$this->data['patients_data'] =	$patients_data["data"];	
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}				
		$this->load->view('management/patients',$this->data);
	}
	
	public function addpatient(){
		
		$treatment_data	=	$this->patient_model->getTreatments(array('clinic_id'=>$this->clinicInfo['id']));
		$this->data["fees"] = $treatment_data[0]["fees"];
		
		if($this->input->post()){
			
			$postData = $this->input->post();
			#echo '<pre>'; print_r($postData);die;
						
			$patient_data = array(
				'full_name'=>$postData['full_name'],
				'age'=> $postData['age'],
				'yrs_mon'=> $postData['yrs_mon'],
				'weight'=> $postData['weight'],
				'address'=> $postData['address'],
				'mobile'=> $postData['mobile'],
				'gender'=> $postData['sex'],
				'refered_by'=> $postData['refer_by'],
				'fees'=> $postData['fees'],
				'discount'=> $postData['discount'],
				'remarks'=>$postData['remarks'],
				'paid_amt'=>$postData['fees']-$postData['due_amount'],						
				'password'=> $postData['due_amount'],
				'clinic_id' => $this->clinicInfo['id'],	
				'treatment_id' => $postData['treatment_id'],
				'_temp_id' => ($postData["_temp_id"] !="")?(int)$postData["_temp_id"]:"0"
				);	
				//print_r($patient_data); die;
			
			$patient_id = $this->patient_model->savePatient($patient_data);		
			
			redirect(base_url('management/patient/reportcard/'.$patient_id));
		}
		
		if($this->input->get('name', TRUE) !=""){			
			$this->data['full_name'] =	$this->input->get('name', TRUE);
		}
		if($this->input->get('mobile', TRUE) !=""){			
			$this->data['mobile'] =	$this->input->get('mobile', TRUE);
		}
		if($this->input->get('address', TRUE) !=""){			
			$this->data['address'] =	$this->input->get('address', TRUE);
		}
		if($this->input->get('gender', TRUE) !=""){			
			$gender =	$this->input->get('gender', TRUE);
			$this->data['sex'] = ($gender=='M')?'Male':'Female';
		}
		if($this->input->get('id', TRUE) !=""){			
			$this->data['temp_id'] =	$this->input->get('id', TRUE);
		}
		
		
		
		$this->data["treatment"] = $treatment_data;
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}
		$this->load->view('management/addpatient', $this->data);
	}
	public function edit($editId){		
		
		if($this->input->post() && $editId){			
			$postData = $this->input->post();
			$varWhere	=	array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']);
			$response = $this->patient_model->updatePatient($postData, $varWhere);
            if($response){
				$this->session->set_flashdata('message','You have successfully updated a patient.');	
				redirect(base_url('management/patient/patients'));
			}
		}
		
		$this->data['treatment']	=	$this->patient_model->getTreatments(array('clinic_id'=>$this->clinicInfo['id']));		
		$patient_data	=	$this->patient_model->getPatientDataById(array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']));
		
		$this->data['sex'] = $patient_data["gender"];
		$this->data['full_name'] = $patient_data["full_name"];
		$this->data['address'] = $patient_data["address"];
		$this->data['age'] = $patient_data["age"];
		$this->data['yrs_mon'] = $patient_data["yrs_mon"];
		$this->data['yrs_mon'] = $patient_data["yrs_mon"];
		$this->data['weight'] = $patient_data["weight"];
		$this->data['refered_by'] = $patient_data["refered_by"];
		$this->data['mobile'] = $patient_data["mobile"];
		$this->data['fees'] = $patient_data["fees"];
		$this->data['discount'] = $patient_data["discount"];
		$this->data['remarks'] = $patient_data["remarks"];
		$this->data['due_amount'] = $patient_data["fees"]-$patient_data["paid_amt"]; 
		$this->data['update_patient'] = 1; 
		$this->data['p_id'] = $editId; 
		$this->data['transId'] = $patient_data['transId']; 
	
		#pr($patient_data);
		$this->load->view('management/addpatient', $this->data);
	}
	public function treatments($editId){
		if($this->input->post() && $editId){			
			$postData = $this->input->post();
			$data	=	array('patient_id'=>$editId,'clinic_id'=>$this->clinicInfo['id'],'treatment_id'=>(int)$postData["treatment"],'fees'=>$postData["fees"],'discount'=>$postData["discount"],'remarks'=>$postData["remarks"],'paid_amt'=>(float)$postData["fees"]-((float)$postData["due_amount"]+(float)$postData["discount"]),'refered_by'=>$postData["refer_by"],'payment_date'=>date("Y-m-d"),'created'=>date("Y-m-d"));
			#print_r($data);
			$response = $this->patient_model->savePatientTransaction($data);
            if($response){
				$this->session->set_flashdata('message','A transaction has been saved.');	
				redirect(base_url('management/patient/patients'));
			}
		}
		

		$treatment_data	=	$this->patient_model->getTreatments(array('clinic_id'=>$this->clinicInfo['id']));
		$patient_data	=	$this->patient_model->getPatientDataById(array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id'],'treatment_id'=>$treatment_data[0]["id"]));
		
		$this->data["treatment"] = $treatment_data;
		$this->data['sex'] = $patient_data["gender"];
		$this->data['yrs_mon'] = $patient_data["yrs_mon"];
		$this->data['full_name'] = $patient_data["full_name"];
		$this->data['address'] = $patient_data["address"];
		$this->data['age'] = $patient_data["age"];
		$this->data['weight'] = $patient_data["weight"];
		$this->data['refered_by'] = $patient_data["refered_by"];
		$this->data['mobile'] = $patient_data["mobile"];
		$this->data['fees'] = $patient_data["fees"];
		$this->data['discount'] = $patient_data["discount"];
		$this->data['remarks'] = $patient_data["remarks"];
		$this->data['due_amount'] = $patient_data["fees"]-$patient_data["paid_amt"]; 
		
		$this->data['p_id'] = $editId; 
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}
		$this->load->view('management/ptreatments', $this->data);
	}
	private function arrangeTreatment($treatments = array()){
		$return_data = array();
		foreach((array)$treatments as $key => $row){
			$return_data[$row["id"]] =$row["name"];
		}
		return $return_data;
	}
	private function arrangeClinicInfo($clinicInfo = array()){
		$return_data = array();		
		foreach((array)$clinicInfo as $key => $row){			
			$return_data[$row["id"]] = $row["clinic_name"];
		}
		return $return_data;
	}
	public function history($editId){
		
		
		$patient_transaction_data	=	$this->patient_model->getPatientTransaction(array('patient_id'=>$editId,'clinic_id'=>$this->clinicInfo['id']));
		$treatment_data	=	$this->patient_model->getTreatments(array('clinic_id'=>$this->clinicInfo['id']));
		$patient_data	=	$this->patient_model->getPatientDataById(array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id'],'treatment_id'=>$treatment_data[0]["id"]));
		#echo '<pre>';print_r($treatment_data);
		$this->data["patient_transaction_data"] = $patient_transaction_data;
		$this->data["treatment"] = $this->arrangeTreatment($treatment_data);
		$this->data['clinic_info'] = $this->arrangeClinicInfo($this->patient_model->getClinicInfo(array('status'=>1)));
		$this->data['sex'] = $patient_data["gender"];
		$this->data['yrs_mon'] = $patient_data["yrs_mon"];
		$this->data['full_name'] = $patient_data["full_name"];
		$this->data['address'] = $patient_data["address"];
		$this->data['age'] = $patient_data["age"];
		$this->data['weight'] = $patient_data["weight"];
		$this->data['refered_by'] = $patient_data["refered_by"];
		$this->data['mobile'] = $patient_data["mobile"];
		$this->data['fees'] = $patient_data["fees"];
		$this->data['discount'] = $patient_data["discount"];
		$this->data['remarks'] = $patient_data["remarks"];
		$this->data['due_amount'] = $patient_data["fees"]-$patient_data["paid_amt"]; 
		
		$this->data['p_id'] = $editId; 
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}
		$this->load->view('management/phistory', $this->data);
	}
	
	public function receipt(){
		if($this->input->get('ids') && $this->input->get('p_id')){
			$p_id	=	$this->input->get('p_id');
			$ids 	= 	$this->input->get('ids');
			$this->data['patientInfo']	=	$this->patient_model->getPatientBy(array('clinic_id'=>$this->clinicInfo['id'],'id'=>$p_id));
			$args	=	array('ts.clinic_id'=>$this->clinicInfo['id'],'ts.patient_id'=>$p_id, 'ts.treatment_id IN('.$ids.')'=>null);
			$this->data['treatments']	=	$this->patient_model->getTreatmentsByIds($args);
			//pr($this->data);
			$this->load->view('management/receipt',$this->data);
		}
	}
	
	public function reports(){
		
		$this->load->view('management/reports', $this->data);
	}
	
	public function reportcard($patient_id){
		$treatment_data	=	$this->patient_model->getTreatments(array('clinic_id'=>$this->clinicInfo['id'])); 
		$this->data["patient_data"] = $this->patient_model->getPatientDataById(array('id'=>$patient_id,'clinic_id'=>$this->clinicInfo['id']));
		
		#pr($this->data["patient_data"]);  die; 
		if($this->clinicInfo['id']==1){
			$this->load->view('management/reportcard',$this->data);
		}else if($this->clinicInfo['id']==2){
			$this->load->view('management/reportcardmanipal',$this->data);
		}else if($this->clinicInfo['id']==3){
			$this->load->view('management/reportcardajay',$this->data);
		}else{
			$this->load->view('management/reportcardmanipal',$this->data);
		}
		
	}
	
	public function prescriptionfiles($patientId){
		
		$this->data['patientdata']	=	$this->patient_model->getPatientBy(array('id'=>$patientId,'clinic_id'=>$this->clinicInfo['id']));
		$this->data['allPrescriptions']	=	$this->patient_model->getAllPrescriptionFiles(array('patient_id'=>$patientId,'clinic_id'=>$this->clinicInfo['id']));
		#pr($this->data);
		$this->load->view('management/prescriptionfiles',$this->data);
	}
	
	
	
	public function uploadprescription(){
		
		if(!empty($_FILES['prescription_file']['name'])){
				$uploadData = array('config'=>array(
												'upload_path'=>'./assets/uploads/prescriptions/',
												'allowed_types'=>'gif|jpg|jpeg|png',
												'min_width'	=> '200',
												'min_height' => '200'),
									 			'filename'=>'prescription_file'			
									);
				$uploadData = uploadfiles($uploadData);
				
				if(isset($uploadData['upload_data'])){
					$saveData = array('clinic_id'=>$this->clinicInfo['id'],'patient_id'=>$this->input->post('patient_id'),'file_name'=>$uploadData['upload_data']['file_name']);	
					if($this->patient_model->savePrescriptionFile($saveData)){
						echo json_encode(array('status'=>1,'filepath'=>base_url('/assets/uploads/prescriptions/'.$uploadData['upload_data']['file_name']),'message'=>'File has been uploaded successfully.'));
					}else{
						echo json_encode(array('status'=>0,'message'=>'Something went wrong, Try again.'));
					}
									
				}else{
					echo json_encode(array('status'=>0,'message'=>$uploadData['error']));
				}
				
			}
		die;
	}
	
	public function downloadPrfile($filename){		
		if($filename){
			$this->load->helper('download');
			$data = file_get_contents(base_url('/assets/uploads/prescriptions/'.$filename));
			force_download($filename, $data);
		}
	}

	public function exportpatient(){
		if($this->input->get('dt', TRUE) !=""){
			$date_range = $this->input->get('dt', TRUE);
			$date_arr = explode(",",$date_range);
			$start_date = $date_arr[0];
			$end_date = $date_arr[1];
			$this->data['start_date'] =	date("m/d/Y",strtotime($start_date));	
			$this->data['end_date'] =	date("m/d/Y",strtotime($end_date));	
		}
		if($this->input->get('p_id', TRUE) !=""){
			$p_id = str_replace('PID-','',$this->input->get('p_id', TRUE));
			$this->data['p_id']  = $p_id; 
		}
		if($this->input->get('tags', TRUE) !=""){
			$refered_by = $this->input->get('tags', TRUE);
			$this->data['refered_by']  = $refered_by; 
		}
		
		$patients_data = $this->patient_model->exportPatients(array('date_range'=>$date_range,'refered_by'=>$refered_by,'p_id'=>$p_id,'clinic_id'=>$this->clinicInfo['id']));

	}
	
}
