<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {
	
	private $clinicInfo, $data = array();
	
	function __construct(){
		parent:: __construct();	
		
		if($this->session->has_userdata('clinic_login')){
			$this->data['clinicInfo'] = $this->clinicInfo  = $this->session->userdata('clinic_login');
			if($this->data['clinicInfo']["id"] !="2"){
				redirect('management/patient/patients/');
			}
		}else{
			redirect('management/login/');
		}
		$this->load->model('inventory_model');
		$this->load->model('staff_model');		
	}
	private function product_unit(){
		return array("Bags"=>"Bags","Bottles"=>"Bottles","Box"=>"Box","Dozens"=>"Dozens","Feet"=>"Feet","Gallon"=>"Gallon","Grams"=>"Grams","Inch"=>"Inch","Kg"=>"Kg","Liters"=>"Liters","Meter"=>"Meter","Nos"=>"Nos","Packet"=>"Packet","Rolls"=>"Rolls");
	}
	private function filter_arr($emp_arr){
		
		$return_arr = array();
		foreach($emp_arr as $key => $row){
			$return_arr[$row["id"]] = $row;//array("staff_name"=>$row["staff_name"]);
		}
		return $return_arr;
	}
	public function index(){
		
		$this->productlist();
	}
	public function add(){		
		if($this->input->post()){			
			$postData = $this->input->post();
			$postData['clinic_id']	=	 $this->clinicInfo['id'];
			$postData['created'] = date("Y-m-d");
			$postData['credit_amt'] = "0.00";
			$response = $this->expense_model->saveExpense($postData);
			if($response){		
				redirect(base_url('management/expense'));
			}
		}
		
		$varWhere	=	array('status <>'=>2,'clinic_id'=> $this->clinicInfo['id']);
		$this->data['all_employee'] = $this->staff_model->getStaffs($varWhere);		
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}
		$this->load->view('management/addexpense', $this->data);
	}
	public function catlist(){		
		$varWhere	=	array('status <>'=>2,'clinic_id'=>$this->clinicInfo['id']);
		
		$this->data['all_category'] = $this->inventory_model->getCategories($varWhere);	
		$this->data['all_employee'] = $this->filter_arr($this->staff_model->getStaffs($varWhere));		
		
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}
		$this->load->view('management/inventorycat', $this->data);
	}
	
	public function addcategory(){		
		if($this->input->post()){			
			$postData = $this->input->post();
			$postData['clinic_id']	=	 $this->clinicInfo['id'];
			$postData['created'] = date("Y-m-d");			
			$response = $this->inventory_model->saveCategory($postData);
			if($response){		
				redirect(base_url('management/inventory/catlist'));
			}
		}
		
		$varWhere	=	array('status <>'=>2,'clinic_id'=> $this->clinicInfo['id']);
		$this->data['all_employee'] = $this->staff_model->getStaffs($varWhere);		
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}
		$this->load->view('management/addinventorycat', $this->data);
	}
	public function editcat($editId){		
		
		if($this->input->post() && $editId){			
			$postData = $this->input->post();
			$varWhere	=	array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']);
			$response = $this->inventory_model->updateCategory($postData, $varWhere);
            if($response){			
				redirect(base_url('management/inventory/catlist'));
			}
		}
		$varWhere  =	array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']);
		$this->data['expense_info']	=	$this->inventory_model->getCategoryBy($varWhere);	
		
		$this->data['all_employee'] = $this->staff_model->getStaffs(array('status <>'=>2,'clinic_id'=>$this->clinicInfo['id']));
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}		
		$this->load->view('management/addinventorycat', $this->data);
	}
	public function brandrlist(){
		
		$clinic_id	=	$this->clinicInfo['id'];
		$varWhere	=	array('status <>'=>2,'clinic_id'=>$clinic_id);
		$this->data['all_cat'] = $this->filter_arr($this->inventory_model->getCategories($varWhere));		
		$this->data['all_brand'] = $this->inventory_model->getBrands($varWhere);	
		//$this->data['all_employee'] = $this->filter_arr($this->staff_model->getStaffs($varWhere));		
		
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}
		$this->load->view('management/inventorybrand', $this->data);
	}
	public function addbrand(){		
		if($this->input->post()){			
			$postData = $this->input->post();
			$postData['clinic_id']	=	 $this->clinicInfo['id'];
			$postData['created'] = date("Y-m-d");			
			$response = $this->inventory_model->saveBrand($postData);
			if($response){		
				redirect(base_url('management/inventory/brandrlist'));
			}
		}
		
		$varWhere	=	array('status <>'=>2,'clinic_id'=> $this->clinicInfo['id']);		
		
		$this->data['all_cat'] = $this->filter_arr($this->inventory_model->getCategories($varWhere));		
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}
		$this->load->view('management/addinventorybrand', $this->data);
	}
	public function editbrand($editId){		
		
		if($this->input->post() && $editId){			
			$postData = $this->input->post();
			$varWhere	=	array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']);
			$response = $this->inventory_model->updateBrand($postData, $varWhere);
            if($response){			
				redirect(base_url('management/inventory/brandrlist'));
			}
		}
		$varWhere  =	array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']);
		$this->data['brand_info']	=	$this->inventory_model->getBrandBy($varWhere);	
		
		$this->data['all_cat'] = $this->filter_arr($this->inventory_model->getCategories($varWhere));		
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}		
		$this->load->view('management/addinventorybrand', $this->data);
	}
	
	public function productlist(){
		
		$clinic_id	=	$this->clinicInfo['id'];
		$varWhere	=	array('status <>'=>2,'clinic_id'=>$clinic_id);
		$this->data['all_cat'] = $this->filter_arr($this->inventory_model->getCategories($varWhere));		
		$this->data['all_brand'] = $this->filter_arr($this->inventory_model->getBrands($varWhere));	
		$this->data['all_product'] = $this->inventory_model->getProducts($varWhere);		
		
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}
		$this->load->view('management/inventoryproduct', $this->data);
	}
	
	public function addproduct(){	
		
		if($this->input->post()){			
			$postData = $this->input->post();
			$postData['clinic_id']	=	 $this->clinicInfo['id'];
			$postData['created'] = date("Y-m-d");			
			$response = $this->inventory_model->saveProduct($postData);
			if($response){		
				redirect(base_url('management/inventory/productlist'));
			}
		}
		
		$varWhere	=	array('status <>'=>2,'clinic_id'=> $this->clinicInfo['id']);		
		
		$this->data['all_cat'] = $this->filter_arr($this->inventory_model->getCategories($varWhere));	
		$this->data['all_brand'] = $this->filter_arr($this->inventory_model->getBrands($varWhere));	
		$this->data['product_unit'] = $this->product_unit(); 	
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}
		$this->load->view('management/addinventoryproduct', $this->data);
	}
	public function editproduct($editId){		
		
		if($this->input->post() && $editId){			
			$postData = $this->input->post();
			$varWhere	=	array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']);
			$response = $this->inventory_model->updateProduct($postData, $varWhere);
            if($response){			
				redirect(base_url('management/inventory/productlist'));
			}
		}
		$varWhere  =	array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']);
		$this->data['product_info']	=	$this->inventory_model->getProductBy($varWhere);	
		
		$this->data['all_cat'] = $this->filter_arr($this->inventory_model->getCategories($varWhere));
		$this->data['all_brand'] = $this->filter_arr($this->inventory_model->getBrands($varWhere));	
		$this->data['product_unit'] = $this->product_unit(); 				
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}		
		$this->load->view('management/addinventoryproduct', $this->data);
	}
	public function orderlist(){
		if($this->input->get('dt', TRUE) !=""){
			$date_range = $this->input->get('dt', TRUE);
			$date_arr = explode(",",$date_range);
			$start_date = $date_arr[0];
			$end_date = $date_arr[1];
			$this->data['start_date'] =	date("m/d/Y",strtotime($start_date));	
			$this->data['end_date'] =	date("m/d/Y",strtotime($end_date));	
			$filterWhere["date_range"] = $date_range;
		}
		if($this->input->get('product_name', TRUE) !=""){			
			$this->data['product_name']  = $this->input->get('product_name', TRUE);
			$filterWhere["product_name"] = $this->data['product_name'];
		}
		
		
		$varWhere	=	array('product.clinic_id'=>$this->clinicInfo['id']);
		$this->data['all_employee'] = $this->filter_arr($this->staff_model->getStaffs(array('status <>'=>2,'clinic_id'=> $this->clinicInfo['id'])));		
		$this->data["all_order"] = $this->inventory_model->getOrders($varWhere,$filterWhere);
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}
		$this->load->view('management/inventoryorder', $this->data);
	}
	
	
	public function addorder($editId){		
		$varWhere  =	array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']);
		$this->data['product_info']	=	$this->inventory_model->getProductBy($varWhere);	
		
		if($this->input->post() && $editId){
			if($this->data['product_info'] > $args["inventory_order_quantity"]){			
				$postData = $this->input->post();
				$postData["clinic_id"] = $this->clinicInfo['id'];
				$postData["product_id"] = (int)$editId;
				
				$response = $this->inventory_model->saveOrder($postData, $varWhere);
				if($response){			
					redirect(base_url('management/inventory/orderlist'));
				}
			}else{
				$this->data["msg"] = "Order Quantity is greater than Product Quantity.";
			}
		}
		
		
		
		$this->data['all_cat'] = $this->filter_arr($this->inventory_model->getCategories($varWhere));
		$this->data['all_brand'] = $this->filter_arr($this->inventory_model->getBrands($varWhere));	
		$this->data['product_unit'] = $this->product_unit(); 				
		$this->data['all_employee'] = $this->filter_arr($this->staff_model->getStaffs(array('status <>'=>2,'clinic_id'=> $this->clinicInfo['id'])));		
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}		
		$this->load->view('management/addinventoryorder', $this->data);
	}
		
	
}
