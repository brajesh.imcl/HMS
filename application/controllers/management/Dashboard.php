<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	private $clinicInfo, $data = array();
	
	function __construct(){
		parent:: __construct();		
		if($this->session->has_userdata('clinic_login')){
			$this->data['clinicInfo'] = $this->clinicInfo  = $this->session->userdata('clinic_login');
		}else{
			redirect('management/login/');
		}
		
	}
	
	public function appointment(){
		
		$this->load->model('appointment_model');
		$varWhere	=	array('clinic_id'=>$this->clinicInfo['id'], 'status <>'=>2, 'date(`created`)'=>date('Y-m-d'));
		$this->data['allappointments'] = $this->appointment_model->getAppointments($varWhere);
		$this->load->view('management/appointment', $this->data);
	}
	
	public function bookappointment(){
		$this->load->model('appointment_model');
		$this->data['allDocters']	=	$this->appointment_model->getDoctersBy(array('clinic_id'=>$this->clinicInfo['id'],'status'=>1));
		if($this->input->post()){
			$postData = $this->input->post();
			$postData['clinic_id']	= 	$this->clinicInfo['id'];			
			$response = $this->appointment_model->bookAppointment($postData);
			if($response){
				$this->session->set_flashdata('message','An appointment has been successfully booked.');
				redirect(base_url('management/dashboard/appointment'));
			}
		}		
		$this->load->view('management/bookappointment', $this->data);
		
	}
	
	public function profile(){
		
		if($this->input->post()){
			$postData	=	$this->input->post();
			
			if($postData['password'] == $postData['cpassword']){
				unset($postData['password']);
			}else{
				$postData['password']	=	md5($postData['password']);
				$this->data['clinicInfo']['password'] = $postData['password']; 
			}
			unset($postData['cpassword']);
			#pr($postData);die;
			$varWhere	=	array('id'=>$this->clinicInfo['id']);
			$rosponse = $this->db->update('clinic_user_login', $postData, $varWhere);
			if($rosponse){
				
				$this->data['clinicInfo']['clinic_name'] 	= $postData['clinic_name'];
				$this->data['clinicInfo']['clinic_addr'] 	= $postData['clinic_addr'];
				$this->data['clinicInfo']['clinic_email'] = $postData['clinic_email'];
				$this->data['clinicInfo']['clinic_phone'] = $postData['clinic_phone'];
				
				$this->session->set_userdata('clinic_login', $this->data['clinicInfo']);
				$this->session->set_flashdata('message', 'You have successfully updated profile.');				
			}else{
				$this->session->set_flashdata('message', 'Something went wrong. Please try again.');
			}
			redirect(base_url('management/dashboard/profile'));
		}		
		$this->load->view('management/profile_setting', $this->data);
	}
	
	public function reports(){		
		$this->load->view('management/reports');		
	}	
	
	public function export_appointments(){		
		$this->load->library('excel');
		$varWhere	=	array('clinic_id'=>$this->clinicInfo['id'], 'status <>'=>3, 'date(`created`)'=>date('Y-m-d'));
		$sql = $this->db->select(array('name','mobile','address','gender','created'))->where($varWhere)->get('temp_appointment');		
		$this->excel->filename = 'todays_appointments';
		$this->excel->make_from_db($sql);
	}

	
	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url());		
	}	
	
}
