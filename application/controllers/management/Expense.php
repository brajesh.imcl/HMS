<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expense extends CI_Controller {
	
	private $clinicInfo, $data = array();
	
	function __construct(){
		parent:: __construct();	
		
		if($this->session->has_userdata('clinic_login')){
			$this->data['clinicInfo'] = $this->clinicInfo  = $this->session->userdata('clinic_login');
		}else{
			redirect('management/login/');
		}
		$this->load->model('expense_model');
		$this->load->model('staff_model');		
	}
	public function index(){
		
		$clinic_id	=	$this->clinicInfo['id'];
		$varWhere	=	array('status <>'=>2,'clinic_id'=>$clinic_id);
		
		if($this->input->get('dt', TRUE) !=""){
			$date_range = $this->input->get('dt', TRUE);
			$date_arr = explode(",",$date_range);
			$start_date = $date_arr[0];
			$end_date = $date_arr[1];
			$this->data['start_date'] =	date("m/d/Y",strtotime($start_date));	
			$this->data['end_date'] =	date("m/d/Y",strtotime($end_date));	
			$filterWhere["date_range"] = $date_range;
		}
		if($this->input->get('goods_name', TRUE) !=""){			
			$this->data['goods_name']  = $this->input->get('goods_name', TRUE);
			$filterWhere["goods_name"] = $this->data['goods_name'];
		}
		
		
		$this->data['allExpense'] = $this->expense_model->getExpenses($varWhere,$filterWhere);	
		$this->data['all_employee'] = $this->filter_employee($this->staff_model->getStaffs(array('status <>'=>2,'clinic_id'=>$clinic_id)));		
		
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}
		$this->load->view('management/expenses', $this->data);
	}
	private function filter_employee($emp_arr){
		$return_arr = array();
		foreach($emp_arr as $key => $row){
			$return_arr[$row["id"]] = array("staff_name"=>$row["staff_name"]);
		}
		return $return_arr;
	}
	
	public function add(){		
		if($this->input->post()){			
			$postData = $this->input->post();
			$postData['clinic_id']	=	 $this->clinicInfo['id'];
			$postData['created'] = date("Y-m-d");
			$postData['credit_amt'] = "0.00";
			$response = $this->expense_model->saveExpense($postData);
			if($response){		
				redirect(base_url('management/expense'));
			}
		}
		
		$varWhere	=	array('status <>'=>2,'clinic_id'=> $this->clinicInfo['id']);
		$this->data['all_employee'] = $this->staff_model->getStaffs($varWhere);		
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}
		$this->load->view('management/addexpense', $this->data);
	}
	public function edit($editId){		
		
		if($this->input->post() && $editId){			
			$postData = $this->input->post();
			$varWhere	=	array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']);
			$response = $this->expense_model->updateExpense($postData, $varWhere);
            if($response){			
				redirect(base_url('management/expense'));
			}
		}
		$varWhere  =	array('id'=>$editId,'clinic_id'=>$this->clinicInfo['id']);
		$this->data['expense_info']	=	$this->expense_model->getExpenseRowBy($varWhere);	
		
		$this->data['all_employee'] = $this->staff_model->getStaffs(array('status <>'=>2,'clinic_id'=>$this->clinicInfo['id']));
		if($this->input->get('pr', TRUE) =="pr"){
			echo '<pre>'; print_r($this->data);
		}		
		$this->load->view('management/addexpense', $this->data);
	}
		
	public function delete($delId){
		if($delId){
			/*$varWhere	=	array('id'=>$delId, 'clinic_id'=>$this->clinicInfo['id']);
			$this->treatment_model->deleteTreatment($varWhere);*/
			redirect(base_url('management/expense'));
		}
		
	}
	
}
